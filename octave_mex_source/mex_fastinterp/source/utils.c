/*
   DESCRIPTION: Contains general utility functions.
  
   Copyright(C) 2002 Antti Happonen
   email: happonea@cs.tut.fi
   You may use, copy, modify this code for any purpose and 
   without fee. You may distribute this ORIGINAL package.

*/ 

/* Gets column from the data */
void getcolumn(double *data, double *column, unsigned long rows, 
	       unsigned long jth)  {

  unsigned long j;
  for (j=0; j < rows; j++) {

    column[j] = data[j + jth*rows];
  }
  return;
}
/* Gets row from the data */
void getrow(double *data, double *row, unsigned long rows, 
	    unsigned long cols, unsigned long ith)  {

  unsigned long j;
  for (j=0; j < cols; j++) {

    row[j] = data[j*rows + ith];
  }
  return;
}
/* Puts column into data */
void putcolumn(double *data, double *column, unsigned long rows, 
	       unsigned long jth) {

  unsigned long j;
  for (j=0; j < rows; j++) {

    data[j + jth*rows] = column[j];
  }
  return;
}
/* Puts row into data */
void putrow(double *data, double *row, unsigned long rows, 
	    unsigned long cols, unsigned long ith) {

  unsigned long j;
  for (j=0; j < cols; j++) {

    data[j*rows + ith] = row[j];
  }
  return;
}
/* Gets window signal from data */ 
void getwin(double *signal, double *window, unsigned long point, 
	    int Nmid, unsigned long length) {

  long k, n;
  n = 0;
  
  for ( k=-Nmid; k <= Nmid; k++ ) {

      window[n] = signal[labs( (long)length - 1 -
		  labs( (long)length - 1 - (long)point - k))];
      n++;
  }
  return;
}     


