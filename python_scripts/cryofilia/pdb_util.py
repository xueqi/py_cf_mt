'''
    PDB file manipulation
    usage:
    
    p = PDB()
    p.readFile(pdbfilename)
    p1 = p.symmetrize([[1,0,0], [0, 1, 0], [1, 0, 1]])
    cx, cy, cz = p1.getCenter()
    p1.toCenter()
    vol = p1.toVolume(pixelSize = 1.3, boxSize = 80) # EMImage
    vol.write_image('vol.mrc')
    p1.write("out.pdb")  # write to file
'''
class PDB(object):
    def __init__(self):
        '''
            one model is dictionary of chains
            one chain is list of atoms. atom.chainID should be same as chain 
        '''
        self.models = [{}]
        
    def readFile(self, filename):
        self.init()
        with open(filename) as f:
            line = f.readline()
            while line:
                if line.startswith("ATOM"):
                    atom = Atom.parse(line)
                    if atom.chainID not in self.models[-1]:
                        self.models[-1][atom.chainID] = []
                    self.models[-1][atom.chainID].append(atom)
                elif line.startswith("ENDMDL"):
                    self.models.append({})
                
                line = f.readline()
        if len(self.models[-1]) == 0: self.models.pop()
        
    def write(self, filename):
        with open(filename, 'w') as f:
            for atom in self.nextAtom():
                f.write("%s\n" % atom)
        
    def init(self):
        self.models = [{}]
        
    def copy(self):
        '''
            return a new copy of pdb
        '''
        p = PDB()
        p.models = []
        for model in self.models:
            newModel = {}
            for chainID in model:
                newModel[chainID] = []
                for atom in model[chainID]:
                    newModel[chainID].append(atom.copy())
            p.models.append(newModel)
        return p
    
    def symmetrize(self, A, inplace = False):
        '''
            helical symmetry is:
            cosa, sina, 0, 0
            -sina, cosa, 0, 0
            0, 0, 1, dz
        '''
        import numpy as np
        if inplace:
            rpdb = self
        else:
            rpdb = self.copy()
        for model_ in rpdb.models:
            coords = []
            for atom in rpdb.nextAtomInModel(model_):
                coords.append([atom.x, atom.y, atom.z, 1])
                x = atom.x * A[0][0] + atom.y * A[0][1] + atom.z * A[0][2] + A[0][3]
                y = atom.x * A[1][0] + atom.y * A[1][1] + atom.z * A[1][2] + A[1][3]
                z = atom.x * A[2][0] + atom.y * A[2][1] + atom.z * A[2][2] + A[2][3]
                atom.x, atom.y, atom.z = x, y, z
        return rpdb

    def toVolume1(self, pixelSize = 1, resolution = None, boxSize = 64):
        '''
            convert pdb to volume.
            If want the volume be centered, center the pdb first.
        '''
        import numpy as np
        if resolution is None or resolution < pixelSize * 2: 
            resolution = pixelSize * 2
        if isinstance(boxSize, int): boxSize = [boxSize] * 3
        if len(boxSize) != 3: raise Exception("Wrong format of boxSize")
        from cryofilia.EMImage import EMImage, TestImage
        outmap = EMImage(*boxSize)
        xt, yt, zt = map(lambda x : x / 2, boxSize)
        gaus = TestImage.sphere(64, 12)
#        gaus.process_inplace("mask.gaussian",{"outer_radius":12.0})
        
        for atom in self.nextAtom():
            x,y,z = atom.x, atom.y, atom.z
            x, y, z = x / pixelSize, y / pixelSize, z / pixelSize
            ix, iy, iz = int(x), int(y), int(z)
            ax, ay, az = x - ix, y - iy, z - iz
            idxx, idxy, idxz = np.mgrid[-1:2, -1:2, -1:2]
            at = (idxx-(2*idxx-1)*ax) * (idxy-2*(idxy-1)*ay) * (idxz-(2*idxz-1)*az)
            at *= atom.weight
            
            outmap.insert_sum(gaus, (ix + xt, iy+yt, iz+zt))
        outmap.voxelSize=pixelSize
        return outmap
    
    def toVolume(self, pixelSize = 1, boxSize = 64):
        ''' Convert pdb to volume. using linear interpolation
            (0,0,0) in pdb is (boxSize/2, boxSize/2, boxSize/2) in volume
        '''
        import numpy as np
        from cryofilia.EMImage import EMImage
        cx, cy, cz = boxSize / 2, boxSize / 2, boxSize / 2
        x,y,z = [],[],[]
        weights = []
        vol = np.zeros([boxSize, boxSize, boxSize])
        for atom in self.nextAtom():
            x.append(atom.x/pixelSize)
            y.append(atom.y/pixelSize)
            z.append(atom.z/pixelSize)
            weights.append(atom.weight)
        x,y,z = np.array(x), np.array(y), np.array(z)
        weights = np.array(weights)
        x0 = np.floor(x).astype(np.int32)
        y0 = np.floor(y).astype(np.int32)
        z0 = np.floor(z).astype(np.int32)
        idxes = np.logical_and.reduce([z0 >= -cx, 
                                      y0 >= -cx, 
                                      x0 >= -cx,
                                      z0 < cx - 1,
                                      y0 < cx  - 1,
                                      x0 < cx - 1])
        x,y,z= x[idxes], y[idxes], z[idxes]
        x0,y0,z0 = x0[idxes], y0[idxes], z0[idxes]
        weights = weights[idxes]
        fx,fy,fz = x - x0, y-y0, z-z0
        mfx, mfy, mfz = 1. - fx, 1. - fy, 1. - fz

        d000 = mfz * mfy
        d001 = d000 * fx
        d000 *= mfx
        d010 = mfz * fy
        d011 = d010 * fx
        d010 *= mfx
        d100 = fz * mfy
        d101 = d100 * fx
        d100 *= mfx
        d110 = fz * fy
        d111 = d110 * fx
        d110 *= mfx
        x0 += cx
        y0 += cy
        z0 += cz
        x1, y1, z1 = x0+1, y0+1, z0+1
        
        vol[z0, y0, x0] += d000 * weights
        vol[z0, y0, x1] += d001 * weights
        vol[z0, y1, x0] += d010 * weights
        vol[z0, y1, x1] += d011 * weights
        vol[z1, y0, x0] += d100 * weights
        vol[z1, y0, x1] += d101 * weights
        vol[z1, y1, x0] += d110 * weights
        vol[z1, y1, x1] += d111 * weights
        
        img = EMImage(vol)
        img.voxelSize=pixelSize
        return img
        
    def transform(self, shx, shy, shz, inplace = True):
        if inplace: p = self
        else: p = self.copy()
        for atom in p.nextAtom():
            atom.x = atom.x + shx
            atom.y = atom.y + shy
            atom.z = atom.z + shz   
        return p
    
    def rotate(self, phi, theta, psi, inplace = True):
        '''
            phi, theta, psi in degree
        '''
        if inplace: p = self
        else: p = self.copy()
        from math import cos, sin, pi
        phi = phi * pi / 180
        theta = theta * pi / 180
        psi = psi * pi / 180
        
        cx = cos(psi)
        sx = sin(psi)
        ct = cos(theta)
        st = sin(theta)
        cp = cos(phi)
        sp = sin(phi)
        
        sym = [[cx*ct*cp - sx*sp, cx*ct*sp+sx*cp, -cx*st, 0],
               [-sx*ct*cp - cx*sp, -sx*ct*sp + cx*cp, sx*st, 0],
               [-st*cp, st*sp, ct, 0],
               [0, 0, 0, 0]]
        p.symmetrize(sym, inplace = inplace)
        return p
    
    def renameChain(self, oldChain, newChain):
        if oldChain == newChain: return
        for _model in self.models:
            newMol = []
            if newChain in _model:
                newMol = _model[newChain]
            if oldChain in _model:
                for atom in _model[oldChain]:
                    atom.chainID = newChain
                    newMol.append(atom)
                del _model[oldChain]
            if newMol:
                _model[newChain] = newMol
    
    def extractChains(self, chains):
        p = PDB()
        for _model in self.models:
            for chn in chains:
                if chn in _model:
                    p.models[0][chn] = _model[chn][:]

        return p


    def toCenter(self):
        cx,cy,cz = self.getCenter()
        self.transform(-cx, -cy, -cz)
    
    def nextAtomInModel(self, model):
        for chainID in model:
            model_ = model[chainID]
            for atom in model_:
                yield atom
     
    def nextAtom(self):
        for model_ in self.models:
            for chainID in model_:
                model = model_[chainID]
                for atom in model:
                    yield atom
                    
    def getCenter(self, gravity=False):
        xs, ys, zs = 0, 0, 0
        N = 0
        for atom in self.nextAtom():
            x,y,z = atom.x, atom.y, atom.z
            if gravity:
                w = atom.weight
                N += atom.weight
            else:
                w = 1
                N += 1
            xs, ys, zs = xs + x*w, ys + y*w, zs + z*w
        return xs / N, ys / N, zs / N

    @staticmethod
    def get_center(pdb, gravity=False):
        ''' Static method to get the center of pdb
        '''
        if isinstance(pdb, PDB):
            return pdb.getCenter(gravity)
        if isinstance(pdb, str):
            p = PDB()
            p.readFile(pdb)
            return p.getCenter(gravity)

    
    @property
    def center(self):
        return self.getCenter()
    @property
    def cog(self):
        return self.getCenter(gravity=True)
    def __add__(self, other):
        result = self.copy()
        result.models.extend(other.models)
        return result
    def __radd__(self, other):
        if other == 0:
            return self.copy()
        return self.__add__(other)
    def __iadd__(self, other):
        self.models.extend(other.models)
        return self
    def __str__(self):
        return "PDB with %d models" % (len(self.models))


atomdefs={'H':(1.0,1.00794),'C':(6.0,12.0107),'N':(7.0,14.00674),'O':(8.0,15.9994),'P':(15.0,30.973761),
              'S':(16.0,32.066),'W':(18.0,1.00794*2.0+15.9994),'AU':(79.0,196.96655), 
              'U':(0,0)}

class Element(object):
    def __init__(self, atom_name, atom_index = None, atom_weight = None):
        self._name = atom_name
        self.atom_index = atom_index
        self.weight = atom_weight
        if self.atom_index is None: self.atom_index = atomdefs[atom_name[0]][0]
        if self.weight is None: self.weight = atomdefs[atom_name[0]][1]
    @property
    def name(self):
        return self._name
    @name.setter
    def name(self, atom_name):
        self._name = atom_name
        self.atom_index = atomdefs[atom_name[0]][0]
        self.weight = atomdefs[atom_name[0]][1]
        
class Atom(Element):
    '''http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html#ATOM
    '''
    fieldwidth = (6, 5, -1, 4, 1, 3, -1, 1, 4, 1, -3, 8, 8, 8, 6, 6, -10, 2, 2)
    fieldname = ('sig', 'serial', 'name', 'altLoc', 'resName', 'chainID', 
                 'resSeq', 'iCode', 'x', 'y', 'z', 'occupancy', 'tempFactor', 
                 'element', 'charge')
    fieldType = (str, int, str, str, str, str, 
                 int, str, float, float, float, float, float, 
                 str, str) 
    code = {str:'s', int:'d', float:'f'}
    input_format = ['%s%s' % (abs(fw), 'x' if fw < 0 else 's') for fw in fieldwidth]
    output_format = ""
    j = 0
    for i in range(len(fieldwidth)):
        fw = fieldwidth[i]
        if fw > 0:
            output_format += "%" 
            if fieldname[j] in ['sig']: output_format += "-"    
            output_format += str(fw) 
            if fieldname[j] in ['x', 'y', 'z']: output_format += '.3'
            elif fieldname[j] in ['tempFactor', 'occupancy']:
                output_format += '.2'

            output_format += code[fieldType[j]]
            j+=1
        else:
            output_format += " " * (-fw)
    def __init__(self, atom_name = "U", x = 0, y = 0, z = 0, atom_index = None, atom_weight = None, index=-1):
        super(Atom, self).__init__(atom_name)
        self.x = x
        self.y = y
        self.z = z
        self.index = index
    
    def copy(self):
        p = Atom()
        for field in Atom.fieldname:
            setattr(p, field, getattr(self, field))
        return p
    @staticmethod
    def parse(line):
        import struct
        atom = Atom()
        p = ['%s%s' % (abs(fw), 'x' if fw < 0 else 's') for fw in Atom.fieldwidth]
        p = "".join(p)
        line = line.strip("\n")
        if len(line) < struct.calcsize(p):
            line += " " * (struct.calcsize(p) - len(line))
        result = struct.unpack(p, line[:struct.calcsize(p)])
        for fname, ftype, v in zip(Atom.fieldname, Atom.fieldType, result):
            setattr(atom, fname, ftype(v.strip()))
        return atom
    
    def __str__(self):
        result = Atom.output_format % tuple(map(lambda x: getattr(self, x), Atom.fieldname))
        return result


    @property
    def index(self):
        return self.serial
    @index.setter
    def index(self, value):
        self.serial = value

