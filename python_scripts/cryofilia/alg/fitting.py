'''
Created on Jan 5, 2018
@author: xueqi
'''
import sys
import numpy as np
<<<<<<< HEAD

=======
import logging

logger = logging.getLogger(__name__)
>>>>>>> findkin
def nd_linefit(points):
    """ N dimentaion linear fit.
    :param points:  All points should in same R sapce.
    :type points: ndarray
    :return: unit_vec, origin, fitness_tot, fitness
    :rtype: (ndarray, ndarray, float, ndarray)
    """
    n, _ = points.shape
    if n < 2:
        raise RuntimeError("At least 2 points are needed for nd_linefit")
    origin = np.mean(points, axis=0)
    coords_ctr = points - origin
    # SVD is a * b * v, not the same as in octave, which is a * b * v'
    a, b, v = np.linalg.svd(coords_ctr, False)
    unit_vec = v[0, :]

    fitness = np.zeros(n)
    # print n, coords_ctr
    for ind in xrange(n):
        a = coords_ctr[ind, :]
        b = coords_ctr[ind, :] - unit_vec
        if a.size == 2: a = np.append(a, 0)
        if b.size == 2: b = np.append(b, 0)
        v = np.cross(a,b)
        fitness[ind] = sum([i * i for i in v])
    fitness_tot = sum(fitness)
    return unit_vec, origin, fitness_tot, fitness

def trimmed_nd_lsq_fit(points, lsq=False):
    """ n dimentional trimmed least square fit.
<<<<<<< HEAD
=======
    
>>>>>>> findkin
    :param points: The input points. All points should in same R sapce.
    :type points: ndarray
    :return: unit_vec, origin, trimmed, fitness_tot, fitness
    :rtype: (ndarray, ndarray, ndarray, float, ndarray)
    :raise: RuntimeError, if less than 4 points provided.
    """
    n, ndim = points.shape
        
    if (not lsq and n < 4) or (lsq and n < 2):
<<<<<<< HEAD
        raise RuntimeError("At least 4 points are needed for trimmed_lsq_fit")
    if n >= 4: lsq = False
=======
        raise FitError("At least 4 points are needed for trimmed_lsq_fit")
    if n < 4: lsq = True
>>>>>>> findkin
    best_resid_error = -1
    best_trim = None
    best_unit_vec = None
    best_origin = None
    indexes = [True] * n
    for trim1 in xrange(n-1):
        for trim2 in xrange(trim1 + 1, n):
            indexes[trim1] = lsq
            indexes[trim2] = lsq
            tempc = points[indexes, :]
            unit_vec, origin, resid_error_tot, fitness = nd_linefit(tempc)
            if best_resid_error == -1 or resid_error_tot < best_resid_error:
                best_resid_error = resid_error_tot
                best_unit_vec = unit_vec
                best_origin = origin
                best_trim = [trim1, trim2]
            indexes[trim1] = True
            indexes[trim2] = True
    trimmed = np.array([False] * n)
<<<<<<< HEAD
    trimmed[best_trim[0]] = True
    trimmed[best_trim[1]] = True
=======
    if not lsq:
        trimmed[best_trim[0]] = True
        trimmed[best_trim[1]] = True
>>>>>>> findkin
    d = np.matmul(points - points[0, :], best_unit_vec)
    npositive = d[d>=0].size
    if npositive < n - npositive:
        best_unit_vec = -best_unit_vec
    resid_error = np.zeros(n)
    coords_ctr = points - best_origin
    for ind in range(n):
        a = coords_ctr[ind, :]
        b = coords_ctr[ind, :] - best_unit_vec
        if a.size == 2: a = np.append(a, 0)
        if b.size == 2: b = np.append(b, 0)
        v = np.cross(a, b)
        resid_error[ind] = sum([i * i for i in v])
    return best_unit_vec, best_origin, trimmed, best_resid_error, resid_error

def trimmed_lsq_fit(x, y, fit_order=1):
    x = np.array(x)
    y = np.array(y)
    param, trimmed, best_resid_err = trimmed_lsq_fit_arr(x, y, fit_order)
    return param.tolist(), trimmed.tolist(), best_resid_err

def trimmed_lsq_fit_arr(x, y, fit_order=1, lsq=False):
    """
        fit: y = a*x^2 + b*x + c
        :param x: The input x for lsq fit.
        :param y_values: The input ys for lsq fit.
        :param lsq: If lsq is True, go back to lsq fit if points too less
        return [a,b,c], outliers
    """
    # check if input y are more than one dataset.
    if (lsq == False and len(x) < 2 + fit_order + 1) or (lsq and len(x) < fit_order+1):
        raise Exception("At least %d points needed for fit_order %d" % 
                        (2 + fit_order + 1, fit_order))
    n = len(x)
    if len(x) >= 3 + fit_order:
        lsq = False
    # set the index for selecting the trimmed point
    indexes = [True] * n
    # bring the points near x = 0
    delta = -np.average(x)
    x = x + delta
    best_resid_err = 1.e100
    best_trim1 = 0
    best_trim2 = 0
    
    if y.ndim == 1:
        y = np.reshape(y, [n, 1])
        return_dim = 1
    else:
        return_dim = y.ndim
    n, ndim = y.shape
    best_params = None
    for trim1 in range(len(x) - 1):
        indexes[trim1] = lsq
        for trim2 in range(trim1+1, len(x)):
            indexes[trim2] = lsq
            # lsq fit on selected points
            tempx = x[indexes]
            tempy = y[indexes, :]
            resid_error = 0
            cur_param = [None] * ndim
            for dim in xrange(ndim):
                param = np.polyfit(tempx, tempy[:, dim], fit_order)
                # calcuate the error to find best parameter.
                poly = np.poly1d(param)
                cur_param[dim] = param
                y_est = poly(tempx)
                diff = y_est - tempy[:, dim]
                diff *= diff
                resid_error += np.sum(diff)
            if resid_error <= best_resid_err:
                best_params = cur_param
                best_resid_err = resid_error
                best_trim1 = trim1
                best_trim2 = trim2
            indexes[trim2] = True
        indexes[trim1] = True
    # recalculate the fit parameter
    params = np.zeros([ndim, fit_order+1])
    for dim in xrange(ndim):
        param = best_params[dim]
        a,b,c = 0, 0, 0
        if fit_order == 0: c = param[0]
        elif fit_order == 1: b, c = param
        elif fit_order == 2: a,b,c = param  
        params[dim, -3:] = np.array(
            [a, b + 2 * a * delta, c + b * delta + a * delta**2])[-fit_order-1:]
    # set outliers
    trimmed = np.zeros(n, dtype=np.bool)
    trimmed[best_trim1] = trimmed[best_trim2] = True
    if return_dim == 1:
        params = params[0,:]
    return params, trimmed, best_resid_err

def trimmed_lsq_fit_md(x, y, fit_order=1):
    """
        fit: y = a*x^2 + b*x + c

        :param x: The input x for lsq fit.
        :param y_values: The input ys for lsq fit.
        return [a,b,c], outliers
    """
    if len(x) < 4:
        raise Exception("At least 4 points needed")
    n = len(x)
    # set the index for selecting the trimmed point
    indexes = [True] * n
    # bring the points near x = 0
    delta = -np.average(x)
    x = np.array(x)
    y = np.array(y)
    x = x + delta
    best_resid_err = 1.e100
    best_trim1 = 0
    best_trim2 = 0
    for trim1 in range(len(x) - 1):
        indexes[trim1] = False
        for trim2 in range(trim1+1, len(x)):
            indexes[trim2] = True
            # lsq fit on selected points
            tempx = x[indexes]
            tempy = y[indexes]
            param = np.polyfit(tempx, tempy, fit_order)
            # calcuate the error to find best parameter.
            poly = polynd(param)
            y_est = poly(tempx)
            diff = y_est - tempy
            diff *= diff
            resid_error = np.sum(diff)
            if resid_error <= best_resid_err:
                best_resid_err = resid_error
                best_trim1 = trim1
                best_trim2 = trim2
            indexes[trim2] = True
        indexes[trim1] = True
    # recalculate the fit parameter
    a,b,c = 0, 0, 0
    if fit_order == 0: c = param[0]
    elif fit_order == 1: b, c = param
    elif fit_order == 2: a,b,c = param
    param = [a, b + 2 * a * delta, c + b * delta + a * delta**2]
    # set outliers
    trimmed = np.zeros(n, dtype=np.bool)
    trimmed[best_trim1] = trimmed[best_trim2] = True
    return param, trimmed, best_resid_err

class Ransac(object):
    """ Ransac fitting algorithm
    """
    def __init__(self, params={}):
        self.params = params
    def fit(self, x, y, max_iter=100, max_diff=1024., order=None):
        """ Fit order 1: y = ax + b.
        a = (y2 - y1) / (x2 - x1)
        """
        import random
        n = len(x)
        if len(y) != n:
            raise RuntimeError("Need same length")
        if len(x) < 2:
            raise RuntimeError("At least 2 points needed for ransac")
        if len(x) == 2:
            a = (y[1] - y[0]) / (x[1] - x[0])
            b = y[1] - a * x[1]
            return a, b
        min_std = sys.maxint
        best_model=None
        for _ in xrange(max_iter):
            alsoinliner = []
            i1 = random.randint(0, n-1)
            i2 = random.randint(0, n-1)
            while i2 == i1:
                i2 = random.randint(0, n-1)
            # get the a, b
            x1, y1, x2, y2 = x[i1], y[i1], x[i2], y[i2]
            # verticle line
            if x1 == x2:
                continue
            a = (y2 - y1) / (x2 - x1)
            b = y2 - a * x1
            # calculate each sample
            for i in range(n):
                if i == i1 or i == i2: continue
                y_calc = a * x[i] + b
                diff = y_calc - y[i]
                if diff * diff < max_diff:
                    alsoinliner.append([x[i], y[i]])
            if len(alsoinliner) > n * 0.25 - 2: # 50% fits
                # fit the
                alsoinliner.append([x1, y1])
                alsoinliner.append([x2, y2])
                x1, y1 = zip(*alsoinliner)
                a, b = np.polyfit(x1, y1, deg=1)
                p = np.poly1d([a,b])
                v = p(x1)
                this_std = np.std(v- y1)
                if this_std < min_std:
                    best_model = [a,b]
                    min_std = this_std
            if len(alsoinliner) >= len(x) - 2:
                max_diff /= 2
                if max_diff < 1: break
        if best_model is None:
            print x, y
            print a, b, alsoinliner
        return best_model, min_std
    
class polynd(object):
    def __init__(self, params):
        self.params = params
    def __call__(self, xval):
        result = np.zeros([len(xval), len(self.params)])
        for index, param in enumerate(self.params):
            poly = np.poly1d(param)
            result[:, index] = poly(xval)
        return result
            
<<<<<<< HEAD
=======
class FitError(RuntimeError):
    pass
>>>>>>> findkin

if __name__ == "__main__":
    ab = [-172.435299, -172.425061, -172.552584, -172.428676,
        -172.5461, -172.552827, -172.573353, -172.573314,
        -172.573289, -172.690936, -172.684857, 7.193972,
        -172.787579, -172.904796, -172.801915, -172.801871,
        -172.801849, 7.429504]
    rs = Ransac()
    print(len(ab))
    print rs.fit(range(len(ab)), ab, max_iter = 100)
<<<<<<< HEAD
            
=======
            
>>>>>>> findkin
