'''
Created on Feb 26, 2018

@author: xueqi
'''

def interp(x1, y1, x2, y2, x):
    """ interpolate value at x using point (x1, y1), (x2, y2)
        make sure x1 != x2
    """
    return y1 + (y2-y1) / (x2-x1) * (x-x1)

def interp_angle(idx1, ang1, idx2, ang2, idx, delta):
    """ This is similar to fix_rollover, but the index diff is known
    """
    ang2 = ang2 + 360. * round((ang1 + (idx2 - idx1) * delta) / 360.)
    return interp(idx1, ang1, idx2, ang2, idx)