''' Find Kinesin. 
    TODO: Use octave currently
'''
import os
import sys
from relion import Metadata, MPIParameter
from chuff.util.project import get_next_workdir
import logging

logger = logging.getLogger(__name__)

FINDKIN_LABEL = "FindKin"
results = []
class FindKinesinParameter(MPIParameter):
    def __init__(self, *args, **kwargs):
        super(FindKinesinParameter, self).__init__(FINDKIN_LABEL)
    
    def add_arguments(self):
        ''' override
        '''
        self.add_argument('num_pfs', type=int, default = 13, 
                help = "Number of Protofilament")
        self.add_argument("bin_factor", type=float, default = 0,
                help = "bin factor for find kinesin")
        self.add_argument('input_star', required = True,
                help = "Input aligned star file")
        self.add_argument('output_star',
                help="output aligned star file")
        self.add_argument("do_find_kinesin", action = "store_true",
                help="Do find kinesin")
        self.add_argument("focal_pair",
                help="aligned focal pair star file")
        self.add_argument("microtubule_pdb",
                help="Fitted microtubule pdb")
        self.add_argument("kinesin_pdb",
                help="Fitted kinesin pdb file")
        super(FindKinesinParameter, self).add_arguments()

class FindKinesin(FindKinesinParameter):
    ''' Find Kinesin class.
    '''
    def __init__(self, workdir = None):
        ''' 
        :param workdir: The working directory of FindKinesin. This is usually created by caller, in FindKin/jobxxx. Should not be None and should exist.
        :param ncpus: The number of process to run. Each process run a subset of micrographs. Default is 1, which means only run on one process.
        :type ncpus: int
        :param myid: The index of process of current FindKinesin instance.
        :type myid: int
        '''
        FindKinesinParameter.__init__(self)
        self.current_mg_id = None  # micrograph counter. This is set with first micrograph to run in the lsit.
        self.pattern = []
    
    def run(self):
        ''' 
        findKin = FindKinesin()
        findKin.run()
        '''
        if self._rank == 0:
            self.workdir = self.workdir or get_next_workdir(FINDKIN_LABEL)
            if not self.output_star:
                self.output_star = os.path.join(self.workdir, "output_dimer.star")
            self.write(os.path.join(self.workdir, 'findkin.star'))
            self.create_dirs()
            # prepare find kinesin references
            self.prepare_refs()
            # read and sort the micrographs by name. Each process will do subset of micrographs
            m = Metadata.readFile(self.input_star)
            mgs = m.group_by("MicrographName")
            if self.focal_pair and os.path.exists(self.focal_pair):
                from cryofilia.align_focal_pair import AlignmentData as FocalPairAlignmentData
                self.m_focal_pair = FocalPairAlignmentData.readFile(self.focal_pair)
                focal_pairs = self.m_focal_pair.group_by("MicrographName")
            else:
                self.m_focal_pair = None
                focal_pairs = {}
                for mg in mgs:
                    focal_pairs[mg] = None
            result = Metadata.new_like(m)
        else:
            mgs = {}
            focal_pairs = {}
        self.workdir = self.bcast(self.workdir)
        n_mgs = len(mgs)
        keys = mgs.keys()
        def g():
            for mg_id, mg in enumerate(mgs):
                yield [mgs[mg], focal_pairs[mg]], {"mg_id" : mg_id+1}
        try:
            results = self.parallel_job(self.find_mg, g(), progress_bar=True)
        except:
            print sys.exc_info()
            pass
        if self._rank == 0:
            for r in results:
                result += r
            if result and len(result) > 0:
                result.write(self.output_star)
            if not self.do_find_kinesin:
                logger.info("Goto %s and run chumps_find_kinesin" % self.workdir)
        self.barrier()
    def create_dirs(self):
        ''' Create directory structure in workdir
        '''
        # to be compatible with old find kin
        if not os.path.exists(self.scan_dir):
            os.makedirs(self.scan_dir)
        if not os.path.exists(self.smooth_dir):
            os.makedirs(self.smooth_dir)
        if not os.path.exists(self.cf_parameter_file):
            os.symlink(os.path.realpath('cf-parameters.m'), self.cf_parameter_file)
    
    def prepare_refs(self):
        ''' Prepare findkin references. To be compatible with old find kin
        '''
        if self.bin_factor == 0:
            raise Exception("bin factor not set yet")
        if self.num_pfs == 0:
            raise Exception("number of protofilaments not set yet")
        pwd = os.getcwd()
        if not os.path.exists(self.ref_tub_dir):
            if self.microtubule_pdb and os.path.exists(self.microtubule_pdb):
                os.symlink(os.path.realpath(self.microtubule_pdb), self.path('micrutubule_pf%d.pdb' % self.num_pfs))
            os.chdir(self.workdir)
            cmd = 'chumps_prepare_findkin bin=%d' % self.bin_factor
            if os.path.exists('microtubule_pf%d.pdb' % self.num_pfs):
                cmd += " pdb=microtubule_pf%d.pdb" % self.num_pfs
            os.system(cmd)
            os.chdir(pwd)
        if not os.path.exists(self.ref_kin_dir):
            if self.kinesin_pdb and os.path.exists(self.kinesin_pdb):
                os.symlink(os.path.realpath(self.kinesin_pdb), self.path('kinesin.pdb'))
            os.chdir(self.workdir)
            cmd = 'chumps_prepare_findkin kinesin=1 bin=%d' % self.bin_factor
            if os.path.exists('kinesin_pf%d.pdb' % self.num_pfs):
                cmd += " pdb=kinesin_pf%d.pdb" % self.num_pfs
            os.system(cmd)
            os.chdir(pwd)

    def find_mg(self, mg, hd_mg=None, mg_id = 0):
        ''' Find kinesin main function.

        :param mg: the micrograph to be processed. Should have following fields:
            DefocusU, DefocusV, DefocusAngle, HelicalTubeID, CoordinateX, CoordinateY, AngleRot, AngleTilt, AnglePsi. 
            optional fields: OriginX, OriginY
            
            if OriginX, OriginY is set, the pixel size should be original micrograph pixel size
        :type mg: Metadata
        '''
        result = Metadata.new_like(mg)
        mg_ori_file = mg[0]['MicrographName']
        mg_name = "%04d" % mg_id
        mg_link = os.path.join(self.scan_dir,"%s.mrc" % mg_name)
        if not os.path.exists(mg_link):
            os.symlink(os.path.realpath(mg_ori_file), mg_link)
        def1 = mg[0]['DefocusU']
        def2 = mg[0]['DefocusV']
        defang = mg[0]['DefocusAngle']
        ctf_doc_file = os.path.join(self.scan_dir, '%s_ctf_doc.spi' % mg_name)
        open(ctf_doc_file,'w').write("1 3 %.4f %.4f %.4f\n" % (def1, def2, defang))
        hd_mg_link = os.path.join(self.scan_dir,"%s_focal_mate_align.mrc" % mg_name)
        hd_mg_ctf_file = os.path.join(self.scan_dir, '%s_focal_mate_ctf_doc.spi' % mg_name)
        focal_mate = False
        if hd_mg is not None:
            hd_mg_ori_file = hd_mg[0].hd_mg_align
            if os.path.exists(hd_mg_ori_file) and not os.path.exists(hd_mg_link):
                os.symlink(os.path.realpath(hd_mg_ori_file), hd_mg_link)
            def1 = hd_mg[0].hd_d1
            def2 = hd_mg[0].hd_d2
            defang = hd_mg[0].hd_dangle
            ctf_doc_file = os.path.join(self.scan_dir, '%s_focal_mate_ctf_doc.spi' % mg_name)
            open(ctf_doc_file,'w').write("1 3 %.4f %.4f %.4f\n" % (def1, def2, defang))
            focal_mate = True

        mts = mg.group_by("HelicalTubeID")
        for mt_id in mts:
            mt = mts[mt_id]
            box_name = "%s_MT%d_1" % (mg_name, mt_id)
            result1 = self.find_mt(mt, box_name, focal_mate=focal_mate)
            if result1 is not None:
                result += result1
        if self.do_find_kinesin:
            print "Find kinesin done on %s" % mg_name
        return result

    def find_mt(self, mt, box_name, focal_mate=False):
        ''' Find kinesin main function
        '''
        import scipy.io as sio
        import numpy as np
        if len(mt) < 3:
            return Metadata.new_like(mt)
        # ????_MT*_1.box should exists. No reason why
        box_path =  os.path.join(self.scan_dir, "%s.box" % box_name)
        open(box_path,'w')
        # chumps_round1 should exist before this
        box_cr_dir = os.path.join(self.workdir, 'chumps_round1', box_name)
        # make chumps_round1/????_MT?_1
        if not os.path.exists(box_cr_dir):
            os.mkdir(box_cr_dir)
        # write selected_mt_type.txt
        open(os.path.join(box_cr_dir, 'selected_mt_type.txt'), 'w').write("%d 3\n" % self.num_pfs)
        # scale doc ... Not sure how to do this one. Use 1 here.
        scale_doc = os.path.join(box_cr_dir, 'radon_scale_doc.spi')
        open(scale_doc,'w').write("1 2 %.5f %f\n" % (1., 0))
        # write the mat file in smoothed_coords directory
        mat_file = os.path.join(self.smooth_dir, '%s_fil_info.mat' % box_name)
        if not os.path.exists(mat_file):
            xs = mt.getValues("CoordinateX")
            ys = mt.getValues("CoordinateY")
            phis = mt.getValues("AngleRot")
            thetas = mt.getValues("AngleTilt")
            psis = mt.getValues("AnglePsi")
            # precised center is used to calculate the subunit position
            if mt[0].hasKey("OriginX"):
                shxs = mt.getValues("OriginX")
                xs = [_a[0] - _a[1] for _a in zip(xs, shxs)]
            if mt[0].hasKey("OriginY"):
                shys = mt.getValues("OriginY")
                ys = [_a[0] - _a[1] for _a in zip(ys, shys)]
            sio.savemat(mat_file,
                     {
                         "coords_est_smooth" : np.array(zip(xs, ys)), # coords_est_smooth
                         "phi_est_smooth"    : np.array(phis),
                         "theta_est_smooth"  : np.array(thetas),
                         "psi_est_smooth"    : np.array(psis),
                         })
        if not self.do_find_kinesin:
            return None
        if not os.path.exists(os.path.join(box_cr_dir, 'find_kinesin', 'tot_decorate_map.spi')):
            self.find_kinesin(box_name)
            if focal_mate:
                self.find_kinesin(box_name, focal_mate=True)
        if not os.path.exists(os.path.join(box_cr_dir, 'find_kinesin', 'sorted_map_full_dimer.spi')):
            self.postprocess(box_name)
        return self.export_dimers(mt, box_name)

    def find_kinesin(self, box_name, focal_mate = False):
        ''' find kinesin for box_name

        :param box_name: The box id used to find kinesin. Should be ????_MT*_1
        :type box_name: str
        '''
        pwd = os.getcwd()
        os.chdir(self.workdir)
        import subprocess
        cmd = ["chumps_find_kinesin", "%s.box" % box_name, "bin=%s" % self.bin_factor, 'invert_density=1']
        if focal_mate:
            cmd.append("focal_mate=1")
        p = subprocess.Popen(cmd, stdout=self.stdout, stderr=self.stderr)
        p.communicate()
        os.chdir(pwd)

    def postprocess(self, box_name):
        ''' Post process for find kinesin
        ''' 
        pwd = os.getcwd()
        os.chdir(self.workdir)
        import subprocess
        p =  subprocess.Popen(["chumps_findkin_postprocess", "%s.box" % box_name, "bin=%s" % self.bin_factor], stdout=self.stdout, stderr=self.stderr)
        p.communicate()
        os.chdir(pwd)
    def export_to_frealign(self, pattern):
        ''' Export to frealign with pattern
        '''
        pwd = os.getcwd()
        os.chdir(self.workdir)
        import subprocess
        cmd = ['chumps_exprot_dimers_to_frealign_new']
        cmd.append('find_bin_factor=%s' % self.bin_factor)
        p =  subprocess.Popen(cmd, stdout=self.stdout, stderr=self.stderr)
        p.communicate()
        os.chdir(pwd)
    def export_dimers(self, mt, box_name, target_mode="dimer"):
        from chuff.util.octave_wrap import get_octave
        logger.debug("exporting dimer")
        octave = get_octave()
        result = Metadata.new_like(mt)
        xs = mt.getValues("CoordinateX")
        ys = mt.getValues("CoordinateY")
        phis = mt.getValues("AngleRot")
        thetas = mt.getValues("AngleTilt")
        psis = mt.getValues("AnglePsi")
        pwd = os.getcwd()
        octave.cd(self.workdir)
        logger.debug("start octave")
        try:
            boxes, xs1, ys1, phis1, thetas1, psis1 = octave.chumps_export_dimers(
                box_name,   # job_dir
                0,          # focal_mate
                target_mode,# target_mode
                1,          # chumps_round
                self.bin_factor, # find_bin_factor
                "chumps_round1/smoothed_coords", # smooth_dir
                phis,       # phis
                thetas,     # thetas
                psis,       # psis
                xs,         # xs
                ys,         # ys
                0,          # prerotate # not used
                self.bin_factor, # frealign bin_factor # not used
                2,          # fit_order
                3,          # data_redundancy
                2,          # max_outliers_per_window
                3,          # twist_tolerance
                10,         # coord_error_tolerance
                5,          # phi_error_tolerance
                5,          # theta_error_tolerance
                5,          # psi_error_tolerance
                10,         # min_seg_length
                nout = 6
                )
        except:
            import sys
            print sys.exc_info()
            boxes = []
        logger.debug( "done octave")
        octave.cd(pwd)
        if len(boxes) > 0:
            boxes, xs1, ys1, phis1, thetas1, psis1 = boxes[0], xs1[0], ys1[0], phis1[0], thetas1[0], psis1[0]

            for i in range(boxes.size):
                ptcl = mt[int(boxes[i])-1].copy()
                ptcl["CoordinateX"] = xs1[i]
                ptcl["CoordinateY"] = ys1[i]
                ptcl["AngleRot"] = phis1[i]
                ptcl["AngleTilt"] = thetas1[i]
                ptcl["AnglePsi"] = psis1[i]
                result.append(ptcl)
        return result


    @property
    def scan_dir(self):
        return os.path.join(self.workdir, 'scans')
    @property
    def smooth_dir(self):
        return os.path.join(self.workdir, 'chumps_round1', 'smoothed_coords')

    @property
    def cf_parameter_file(self):
        return os.path.join(self.workdir, 'cf-parameters.m')
    
    @property
    def ref_tub_dir(self):
        return  os.path.join(self.workdir, 'chumps_round1', 'ref_tub_subunit_bin%d' % self.bin_factor)
    @property
    def ref_kin_dir(self):
        return  os.path.join(self.workdir, 'chumps_round1', 'ref_kin_subunit_bin%d' % self.bin_factor)

