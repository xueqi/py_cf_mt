"""
    Smooth Coordinate Module
    Smooth a filament according to its symmetry
    The input of the smooth_coord should be continuous filament without gap or overlap
    Gap or overlap should be fixed by fixup_align_params
@author: xueqi
"""
import logging
<<<<<<< HEAD
from collections import Counter
import os
import numpy as np
# check if interactive
import __main__ as main_module
if hasattr(main_module, '__file__'):
    import matplotlib
    matplotlib.use('Agg')
=======
from collections import Counter, OrderedDict
import os
import numpy as np
>>>>>>> findkin
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from relion import Metadata, MPIParameter
from cryofilia.alg.fitting import trimmed_nd_lsq_fit,\
    trimmed_lsq_fit_arr
from cryofilia.helix.smoothing.utils import get_values, fix_rollover, get_window
from cryofilia.helix.smoothing.plotting import plot_smooth_result_m

logger = logging.getLogger(__name__)

SMOOTH_XY = "smooth_xy"
SMOOTH_ALL = "smooth_all"
SMOOTH_ANGLE = "smooth_angle"
SMOOTH_NONE = "smooth_none"

class SmoothedCoordinates(Metadata):
    """ Smooth Coordinate Output format
    """
    _labels = {
        'mg_name'   : ['MicrographName'],
        'x'         : ['CoordinateX'],
        'y'         : ['CoordinateY'],
        'z'         : ['CoordinateZ'],
        'shx'       : ['OriginX'],
        'shy'       : ['OriginY'],
        'shz'       : ['OriginZ'],
        'phi'       : ['AngleRot'],
        'theta'     : ['AngleTilt'],
        'psi'       : ['AnglePsi'],
        'magnification' : ['Magnification'],
        'scanner_pixel_size'    : ['DetectorPixelSize'],
        'voltage'   : ['Voltage'],
        'd1'        : ['DefocusU'],
        'd2'        : ['DefocusV'],
        'dangle'    : ['DefocusAngle'],
        'amplitude_contrast'    : ['AmplitudeContrast'],
        'cs'        : ['SphericalAberration'],
        'fila_id' : ['HelicalTubeID'],
        'track_length'  :['HelicalTrackLength'],
        'twist' : ['twist', float, "helical twist"],
        'dtheta': ['dtheta', float, 'dtheta'],
        'dpsi'  : ['dpsi', float, 'dpsi'],
        'discontinuity' : ['discontinuity', float, 'discontinuity'],
        'interpolated'  : ['interpolated', bool, 'interpolated'],
        'fitness'   : ['fitness', float, 'fitness'],
        'index' : ['index', int, 'index'],
        'x_index_est' : ['xIndexEstimate', float, "estimated x after indexing"],
        'y_index_est' : ['yIndexEsitmate', float, 'estimated y after indexing'],
        'x_old' : ['oldXCoordinate', float, 'original x coord'],
        'y_old' : ['oldYCoordinate', float, 'original y coord'],
        'discont' : ['discont', int, "discontinuity mask"]
        }

    def get_name(self, num_pf=None):
        """ Get identifier name of the filament
            bname_filaid_pfxx
        """
        bname = os.path.splitext(os.path.basename(self.mg_name))[0]
        if num_pf is not None:
            return "%s_%d_%d" % (bname, self.fila_id, num_pf)
        else:
            return "%s_%d" % (bname, self.fila_id)
# load labels
SmoothedCoordinates()
class SmoothCoordsParameter(MPIParameter):
    """ SmoothCoords parameters. Inherit from MPIParameter
    """
    def __init__(self, *args, **kwargs):
        super(SmoothCoordsParameter, self).__init__()
    def add_arguments(self):
        self.add_argument('input_star', required=True,
                help="Input star file", group="Input/Output")
        self.add_argument("output_star_file",
                help="Output star file", group="Input/Output")
        # filament Parameter
        self.add_argument("helical_twist", type=float, default=-1000,
                help="approximately twist per repeat",
                group="Filament Parameter")
        self.add_argument("helical_rise", type=float,
                help="approximately twist per repeat",
                group="Filament Parameter")
        self.add_argument("num_pf", type=int, default=1,
                help="Number of protofilament",
                group="Filament Parameter")
        self.add_argument("subunits_per_repeat", type=float, default=1,
                help="number of subunit per repeat",
                group="Filament Parameter")
        self.add_argument("rise_per_subunit", type=float,
                help="rise per subunit. used for fixup seam",
                group="Filament Parameter")
        self.add_argument("twist_per_subunit", type=float, default=0.0,
                help="twist per subunit. needed for fix seam",
                group="Filament Parameter")
        self.add_argument("num_starts", type=float, default=1.0,
                help="num of starts. Used to calculate subunit infos",
                group="Filament Parameter")
        # experiment
        self.add_argument("micrograph_pixel_size", type=float,
                help="pixel size on micrograph, in which boxing is done")
        self.add_argument("phi_error_tolerance", type=float, default=5,
                help="phi error tolerance")
        self.add_argument("theta_error_tolerance", type=float, default=5,
                help="theta error tolerance")
        self.add_argument("psi_error_tolerance", type=float, default=5,
                help="psi error tolerance")
        self.add_argument("angle_tolerance", type=float, default=5,
                help="angle tolerance, global for all angles")
        self.add_argument("direction_error_tolerance", type=float, default=5,
                help="direction error tolerance")
        self.add_argument("dist_tolerance", type=float, default=5,
                help="distance difference between adjacent subunits tolerance")
        self.add_argument("twist_tolerance", type=float, default=7,
                help="twist tolerance")
        self.add_argument("win_size", type=int, default=7,
                help="Window size for smooth")
        self.add_argument("fit_order", type=int, default=2,
                help="fit order to smooth the data")
        self.add_argument("min_segs", type=int, default=1,
                help="mininum consecutive boxes to keep")
        self.add_argument("output_plot_dir",
                help="output plot directory")
        self.add_argument("plot_title", default=".",
                help="plot title")
<<<<<<< HEAD
=======
        self.add_argument("interp_discontinuity", action="store_true",
                help="Also interpolate discontinuity")
>>>>>>> findkin
        self.add_argument("keep_filament_id", action="store_true",
                help="do not reject the boxes and keep filament id")
        self.add_argument("output_pdf",
                help="output plot in pdf. Does not work yet")
        self.add_argument("dont_output_plot", action="store_true",
                help="Do not output plot")
        super(SmoothCoordsParameter, self).add_arguments()

SmoothCoordsParameter()
class SmoothCoords(SmoothCoordsParameter):
    """ Smooth Coords instance. For command line processing
    """
    def __init__(self, *args, **kwargs):
        """ Constructor. No description
        """
        super(SmoothCoords, self).__init__()
        # attributes.
        self.output_star_file = ""
        self._pdf = None
        self.project = None
        self.psi_error_tolerance = 0.
        self.phi_error_tolerance = 0.
        self.theta_error_tolerance = 0.
        self.coord_error_tolerance = 0.
        self.angle_error_tolerance = 0.
        self.rise_per_subunit = 0.
        self.twist_per_subunit = 0.
        self.output_plot_dir = ""
        self.output_pdf = ""
        self.micrograph_pixel_size = 0.
        self.min_segs = 0
        self.subunits_per_repeat = 0
        self.nuke = False
        self.keep_filament_id = False
        self.input_star = ""
        self.output_star = ""
        self.fit_order = 0
        self.angle_tolerance = 0.
        self.helical_rise = 0.
        self.helical_twist = 0.
        self.twist_tolerance = 0.
        self.win_size=0.

    def run(self):
        """ Overwrite Parameter.run function
        """
        if self.output_plot_dir == "":
            self.output_plot_dir = os.path.join(self.workdir, "plots")
        if self.output_plot_dir != ""  and not os.path.exists(self.output_plot_dir):
            self.mkdir(self.output_plot_dir)
        self.workdir = self.workdir or  self.get_next_workdir()
        if self.output_star_file == "":
            self.output_star_file = os.path.join(self.workdir, "smooth_output.star")
        if self.output_pdf:
            self._pdf = PdfPages(self.output_pdf)
        if abs(self.helical_rise) < 0.0001:
            raise SmoothError("Helical rise must not be 0")
<<<<<<< HEAD
=======
        if abs(self.helical_twist + 1000) < 0.001:
            raise SmoothError("Helical twist must be provided")
        if self.interp_discontinuity:
            self.keep_filament_id = True
>>>>>>> findkin
        if self.micrograph_pixel_size == 0.:
            try:
                from cryofilia.project import CFProject
                self.project = CFProject(os.getcwd())
            except RuntimeError:
<<<<<<< HEAD
                raise "Please provide micrograph_pixel_size if not run in a chuff project directory"
            self.micrograph_pixel_size = self.project.pixel_size
        m_input = Metadata.readFile(self.input_star)
        filaments = m_input.group_by('MicrographName', 'HelicalTubeID')
        output_star = SmoothedCoordinates()
        if not os.path.exists(self.output_star_file) or self.nuke:
            output_filaments = {}
        else:
            output_star.read(self.output_star_file)
            output_filaments = output_star.group_by('MicrographName', 'HelicalTubeID')
        self.save()

        results = self.parallel_job(self.smooth_filament_m,
                                    self.generate_args(filaments,
                                                       output_filaments))
        for smoothed_filament in results:
            for ptcl in smoothed_filament:
                if len(output_star) == 0:
                    output_star.addLabels(ptcl.keys())
                output_ptcl = output_star.newObject()
                output_ptcl.update(ptcl)
                output_star.append(output_ptcl)
        # output the star file
        output_star.write(self.output_star_file)
        output_star.clean_for_relion().write(
            "%s_relion%s" % os.path.splitext(self.output_star_file))

    def generate_args(self, filaments, output_filaments):
        """ Generate arguments for smooth_filament function
        """
        for mgname, fila_id in filaments:
            fila = filaments[(mgname, fila_id)]
            tubename = (mgname, fila_id)
            bname = os.path.splitext(os.path.basename(mgname))[0]
            if not self.nuke and tubename in output_filaments:
                continue
            kwargs = {}
            if not self.dont_output_plot:
                kwargs['output_plot'] = os.path.join(self.output_plot_dir,
                        "%s_%d.jpg" % (bname, fila_id))
            yield [fila], kwargs

    def smooth_filament_m(self, fila, output_plot=None):
        """ smooth_filament takes a filament as input,
            output the smoothed coordinates
            The returned filament is a SmoothedCoordinates instance,
            containing all smoothed parameters for each particles.
            This is the main function for parallelization
            :param fila: The input filament
            :type fila: Metadata
            :param output_plot: The plot name for output
            :type output_plot: str
            :return: The smoothed filament
            :type: SmoothedCoordinates
        """
        # create plot dir.
        output_plot_name = output_plot
        if not isinstance(fila, SmoothedCoordinates):
            fila = SmoothedCoordinates.copy_from(fila, extra=True)
        if output_plot_name is None:
            if self.output_plot_dir:
                if not os.path.exists(self.output_plot_dir):
                    # this is from MPParameter, dealing with multiprocess
                    self.mkdir(self.output_plot_dir)
                mg_name = os.path.basename(fila.mg_name)
                fila_id = fila.fila_id
                output_plot_name = os.path.join(self.output_plot_dir,
                                                "%s_%d.pdf" % (mg_name, fila_id))

        fila0 = fila
        if not isinstance(fila0, SmoothedCoordinates):
            fila0 = SmoothedCoordinates.copy_from(fila0, extra=True)

        # fix seam first
        if self.num_pf > 1:
            # TODO: Move to intialize method
            if self.rise_per_subunit == 0.:
                if self.helical_rise == 0.:
                    raise SmoothError("Could not determine helical_rise_per_subunit")
                logger.info("Use rise: %.4f, num_pf: %d, num_starts: %.1f",
                             self.helical_rise, self.num_pf, self.num_starts)
                self.rise_per_subunit = self.helical_rise * self.num_starts / self.num_pf

            if self.twist_per_subunit == 0.:
                # This won't happend for num_pf > 1
                self.twist_per_subunit = (360. + self.helical_twist) / self.num_pf
            fila = fixup_seam_pos_filament_star(fila, self.rise_per_subunit,
                                          self.twist_per_subunit,
                                          self.helical_twist,
                                          self.micrograph_pixel_size)


        spacing = self.helical_rise / self.micrograph_pixel_size
        # return immediately if only 2 box
        if len(fila) <= 2:
            return fila
        def _m_smooth(fila):
            fila_est = fila.copy()
            # initialize index
            for ptcl in fila_est:
                ptcl.index = 0
            for iter in xrange(5):
                indices = fila_est.getValues("index")
                index_coords_m(fila_est, spacing,
                               micrograph_pixel_size=self.micrograph_pixel_size,
                               winsize=self.win_size)
                if indices == fila_est.getValues("index"):
                    # converged
                    break
            # check the polarity
            for idx in range(len(fila_est)):
                fila[idx].index = fila_est[idx].index
                fila[idx].fitness = fila_est[idx].fitness

            fila_interp = fila.copy()
            interpolate_coords_m(fila_interp,
                                 micrograph_pixel_size=self.micrograph_pixel_size,
                                 expect_dphi=-self.helical_twist,
                                 winsize=self.win_size,
                                 fit_order=self.fit_order)
            fila_smooth = fila.copy()
            interpolate_coords_m(fila_smooth,
                                 smooth_method=SMOOTH_ALL,
                                 micrograph_pixel_size=self.micrograph_pixel_size,
                                 expect_dphi=-self.helical_twist,
                                 winsize=self.win_size,
                                 fit_order=self.fit_order)
            return fila_interp, fila_smooth

        fila_interp, fila_smooth = _m_smooth(fila)
        polarity = get_polarity_m(fila_smooth, self.micrograph_pixel_size)
        if np.sum(polarity) < 0:
            fila.reverse()
            fila_interp, fila_smooth = _m_smooth(fila)
        # print fila_interp.getValues("index")
        for ptcl in fila_smooth:
            ptcl.phi = ptcl.phi % 360

        diagnosis_m(fila_smooth,
                  micrograph_pixel_size=self.micrograph_pixel_size,
                  direction_error_tolerance=self.direction_error_tolerance,
                  phi_error_tolerance=self.phi_error_tolerance,
                  theta_error_tolerance=self.theta_error_tolerance,
                  psi_error_tolerance=self.psi_error_tolerance,
                  twist_error_tolerance=self.twist_tolerance,
                  dist_error_tolerance=self.dist_tolerance/self.micrograph_pixel_size)
        # smooth should be done in one threads. This is a fast step.
        # but we need to keep the fila_interp and original fila information.
        fig = plot_smooth_result_m(fila_smooth, self.micrograph_pixel_size,
                            fila_interp=fila_interp,
                            fila_ori=fila,
                           expect_dphi=-self.helical_twist,
                           expect_rise=self.helical_rise,
                           ylim=self.helical_rise * 2)
        if self._pdf:
            self._pdf.savefig(fig)
        if output_plot_name.endswith(".pdf"):
            with PdfPages(output_plot_name) as pdf:
                pdf.savefig(fig)
        else:
            fig.savefig(output_plot_name)
        plt.close(fig)
=======
                import sys
                print sys.exc_info()
                raise "Please provide micrograph_pixel_size if not run in a chuff project directory"
            self.micrograph_pixel_size = self.project.pixel_size
        if self.micrograph_pixel_size == 0.:
            raise RuntimeError("The micrograph pixel size is 0. Please specify --micrograph_pixel_size in command")
        m_input = Metadata.readFile(self.input_star)
        try:
            filaments = m_input.group_by('MicrographName', 'HelicalTubeID')
        except KeyError: # No Helical Tube ID. This should not happen in normal case
            filaments = m_input.group_by("MicrographName")
            n_f = OrderedDict()
            for key, value in filaments.items():
                n_f[(key, 1)] = value
            filaments = n_f
        output_star = SmoothedCoordinates()
        if not os.path.exists(self.output_star_file) or self.nuke:
            output_filaments = {}
        else:
            output_star.read(self.output_star_file)
            output_filaments = output_star.group_by('MicrographName', 'HelicalTubeID')
        self.save()

        results = self.parallel_job("self.smooth_filament_m",
                                    self.generate_args(filaments,
                                                       output_filaments))
        for smoothed_filament in results:
            for ptcl in smoothed_filament:
                if len(output_star) == 0:
                    output_star.addLabels(ptcl.keys())
                output_ptcl = output_star.newObject()
                output_ptcl.update(ptcl)
                output_star.append(output_ptcl)
        # output the star file
        output_star.write(self.output_star_file)
        output_star.clean_for_relion().write(
            "%s_relion%s" % os.path.splitext(self.output_star_file))

    def generate_args(self, filaments, output_filaments):
        """ Generate arguments for smooth_filament function
        """
        for mgname, fila_id in filaments:
            fila = filaments[(mgname, fila_id)]
            if len(fila) < 2: continue
            tubename = (mgname, fila_id)
            bname = os.path.splitext(os.path.basename(mgname))[0]
            if not self.nuke and tubename in output_filaments:
                continue
            kwargs = {}
            if not self.dont_output_plot:
                kwargs['output_plot'] = os.path.join(self.output_plot_dir,
                        "%s_%d.jpg" % (bname, fila_id))
            yield [fila], kwargs

    def smooth_filament_m(self, fila, output_plot=None, keep_original_ptcls=False):
        """ smooth_filament takes a filament as input,
            output the smoothed coordinates
            The returned filament is a SmoothedCoordinates instance,
            containing all smoothed parameters for each particles.
            This is the main function for parallelization
            :param fila: The input filament
            :type fila: Metadata
            :param output_plot: The plot name for output
            :type output_plot: str
            :return: The smoothed filament
            :type: SmoothedCoordinates
        """
        
        if keep_original_ptcls:
            self.keep_filament_id = False
        # create plot dir.
        output_plot_name = output_plot
        if not isinstance(fila, SmoothedCoordinates):
            fila = SmoothedCoordinates.copy_from(fila, extra=True)
        logger.info("smoothing %s:%d", fila.mg_name, fila.fila_id) 
        if output_plot_name is None:
            if self.output_plot_dir:
                if not os.path.exists(self.output_plot_dir):
                    # this is from MPParameter, dealing with multiprocess
                    self.mkdir(self.output_plot_dir)
                mg_name = os.path.basename(fila.mg_name)
                fila_id = fila.fila_id
                output_plot_name = os.path.join(self.output_plot_dir,
                              "%s_%d.pdf" % (mg_name, fila_id))

        # fix seam first
        if self.num_pf > 1:
            # TODO: Move to intialize method
            if self.rise_per_subunit == 0.:
                if self.helical_rise == 0.:
                    raise SmoothError("Could not determine helical_rise_per_subunit")
                logger.info("Use rise: %.4f, num_pf: %d, num_starts: %.1f",
                             self.helical_rise, self.num_pf, self.num_starts)
                self.rise_per_subunit = self.helical_rise * self.num_starts / self.num_pf

            if self.twist_per_subunit == 0.:
                # This won't happend for num_pf > 1
                self.twist_per_subunit = (360. + self.helical_twist) / self.num_pf
            fila = fixup_seam_pos_filament_star(fila, self.rise_per_subunit,
                                          self.twist_per_subunit,
                                          self.helical_twist,
                                          self.micrograph_pixel_size)

        spacing = self.helical_rise / self.micrograph_pixel_size
        mt = False
        # return immediately if only 2 box
        if len(fila) <= 2:
            return fila
        polarity = get_polarity_m_1(fila,
                                    micrograph_pixel_size=self.micrograph_pixel_size)
        if np.sum(polarity) < 0:
            fila.reverse()
            polarity = -polarity
            polarity = polarity[::-1]
        def _m_smooth(fila, polarity=None):
            fila_est = fila.copy()
            # initialize index
            for ptcl in fila_est:
                ptcl.index = 0
            for iter in xrange(5):
                indices = fila_est.getValues("index")
                index_coords_m(fila_est, spacing,
                               micrograph_pixel_size=self.micrograph_pixel_size,
                               winsize=self.win_size,
                               polarity=polarity)
                if indices == fila_est.getValues("index"):
                    # converged
                    break
            for idx in range(len(fila_est)):
                fila[idx].index = fila_est[idx].index
                fila[idx].fitness = fila_est[idx].fitness
            fila_interp = fila_est.copy()

            interpolate_coords_m(fila_interp,
                                 micrograph_pixel_size=self.micrograph_pixel_size,
                                 expect_dphi=-self.helical_twist,
                                 winsize=self.win_size,
                                 fit_order=self.fit_order)
            fila_smooth = fila_est.copy()
            interpolate_coords_m(fila_smooth,
                                 smooth_method=SMOOTH_ALL,
                                 micrograph_pixel_size=self.micrograph_pixel_size,
                                 expect_dphi=-self.helical_twist,
                                 winsize=self.win_size,
                                 fit_order=self.fit_order)
            return fila_interp, fila_smooth

        fila_interp, fila_smooth = _m_smooth(fila, polarity=polarity)

        for ptcl in fila_smooth:
            ptcl.phi = ptcl.phi % 360

        diagnosis_m(fila_smooth,
                  micrograph_pixel_size=self.micrograph_pixel_size,
                  direction_error_tolerance=self.direction_error_tolerance,
                  phi_error_tolerance=self.phi_error_tolerance,
                  theta_error_tolerance=self.theta_error_tolerance,
                  psi_error_tolerance=self.psi_error_tolerance,
                  twist_error_tolerance=self.twist_tolerance,
                  dist_error_tolerance=self.dist_tolerance/self.micrograph_pixel_size)
        if self.interp_discontinuity:
            print("Correcting discontinuity")
            fila_smooth_filt = fila_smooth.filter(lambda x: x.discontinuity < 0.5)
            fila_interp, fila_smooth = _m_smooth(fila_smooth_filt)
        diagnosis_m(fila_smooth,
                  micrograph_pixel_size=self.micrograph_pixel_size,
                  direction_error_tolerance=self.direction_error_tolerance,
                  phi_error_tolerance=self.phi_error_tolerance,
                  theta_error_tolerance=self.theta_error_tolerance,
                  psi_error_tolerance=self.psi_error_tolerance,
                  twist_error_tolerance=self.twist_tolerance,
                  dist_error_tolerance=self.dist_tolerance/self.micrograph_pixel_size)
        # smooth should be done in one threads. This is a fast step.
        # but we need to keep the fila_interp and original fila information.
        fig = plot_smooth_result_m(fila_smooth, self.micrograph_pixel_size,
                            fila_interp=fila_interp,
                            fila_ori=fila,
                           expect_dphi=-self.helical_twist,
                           expect_rise=self.helical_rise,
                           ylim=self.helical_rise * 2)
        if self._pdf:
            self._pdf.savefig(fig)
        if output_plot_name.endswith(".pdf"):
            with PdfPages(output_plot_name) as pdf:
                pdf.savefig(fig)
        else:
            fig.savefig(output_plot_name)
        plt.close(fig)
        
        if keep_original_ptcls:
            # keep original particles, find the nearest ptcls in original stack
            smooth_x, smooth_y = fila_smooth.getValues(['CoordinateX', 'CoordinateY'])
            smooth_coords = zip(smooth_x, smooth_y, range(len(smooth_x)))
            # O(N^2) 
            scale = 10000.*fila[0]['ScannerPixelSize']/fila[0]['Magnification']
            scale /= self.micrograph_pixel_size
            for ptcl in fila:
                x,y = ptcl.x, ptcl.y
                min_dist2 = 1000000000000
                min_idx = 0
                for smx, smy, idx in smooth_coords:
                    dist2 = (smx-x) * (smx-x) + (smy-y) * (smy-y)
                    if dist2 < min_dist2:
                        min_dist2 = dist2
                        min_idx = idx
                if min_dist2 < 1000000000:
                    min_ptcl = fila_smooth[min_idx]
                    ptcl.x, ptcl.y = min_ptcl.x, min_ptcl.y
                    ptcl.shx = (x - min_ptcl.x) / scale
                    ptcl.shy = (y - min_ptcl.y) / scale
                    ptcl.phi = min_ptcl.phi
                    ptcl.theta = min_ptcl.theta
                    ptcl.psi = min_ptcl.psi
                    
>>>>>>> findkin
        if not self.keep_filament_id:
            fila_smooth = self.split_filament(fila_smooth)
        return fila_smooth

    def split_filament(self, m_filament):
        """ Split filament by reasigning filament id
        """
        result = Metadata.new_like(m_filament)
        fid = m_filament[0].fila_id
        current = []
        for ptcl in m_filament:
            tmp_ptcl = ptcl.copy()
            if not tmp_ptcl.discontinuity:
                ptcl.fila_id = fid
                current.append(ptcl)
            if len(current) > 0 and (tmp_ptcl.discontinuity
                                     or ptcl is m_filament[-1]):
                if len(current) >= self.min_segs:
                    fid += 100
                    result += current
                current = []
        return result

<<<<<<< HEAD

=======
>>>>>>> findkin
    def __getstate__(self):
        """ For pickle. Used for mpi communication
        """
        state = super(SmoothCoords, self).__getstate__()
        if 'progress_bar' in state:
            del state['progress_bar']
        return state

class SmoothError(RuntimeError):
    """ Smooth Error
    """
    pass

def diagnosis_m(m_in, *args, **kwargs):
    """ Diagnosis on smoothed coords
    calculate net_twist
    """
    n_ptcls = len(m_in)
    micrograph_pixel_size = kwargs.get("micrograph_pixel_size")
    coords = get_values(m_in, micrograph_pixel_size)
    discontinuities = diagnosis(coords, *args, **kwargs)
    for idx in range(n_ptcls):
        m_in[idx].discontinuity = discontinuities[idx]

def diagnosis(coords,winsize=7, micrograph_pixel_size=1.,
                   pixel_size=None, scale=None,
                   direction_error_tolerance=0,
                   phi_error_tolerance=0,
                   theta_error_tolerance=0,
                   psi_error_tolerance=0,
                   twist_error_tolerance=0,
                   dist_error_tolerance=0):
    from cryofilia.helix.smoothing.discontinuity import (
        coord_discontinuity, twist_discontinuity, dist_discontinuity)
     
    dist_disc = dist_discontinuity(coords[:, :3], threshold=dist_error_tolerance)
    coord_disc = coord_discontinuity(coords[:, :3], threshold=direction_error_tolerance)
    twist_disc = twist_discontinuity(coords[:, 3], threshold=twist_error_tolerance)
    return coord_disc | twist_disc | dist_disc
    #return twist_disc

def index_equispaced(coords, unit_vec, spacing, trimmed):
    """ Get index with equal spaced,
    The zero index is the center of remaining points after trimmed lsq fit
    """
    all_n = coords.shape[0]
<<<<<<< HEAD
=======
    if all_n == 1:
        return np.zeros(1), spacing
>>>>>>> findkin
    ind = np.arange(all_n)
    ind = ind[~trimmed]
    coords_sel = coords[ind, :]
    n_sel = ind.size
    t_inds = np.round(np.abs(np.arange(n_sel) - (n_sel // 2)))
    ind_center = np.argmin(t_inds)
    # distance = np.matmul(coords_sel - coords_sel[0, :], unit_vec)
    # spacing_adjust = distance[-1] / round(distance[-1] / spacing)
    spacing_adjust = spacing
    point_indices = np.matmul(coords - coords_sel[ind_center, :], unit_vec)
    point_indices = np.round(point_indices / spacing_adjust)
    return point_indices, spacing_adjust

def get_polarity_m(m_in, micrograph_pixel_size):
    """ Fix polarity of filament
    """
    coords = get_values(m_in, micrograph_pixel_size=micrograph_pixel_size)
<<<<<<< HEAD
    # The vector for each ptcl, except last one
    coord_diff = coords[1:, :3] - coords[:-1, :3]
    dists = np.sqrt(np.sum(coord_diff * coord_diff, axis=1))
=======
    return get_polarity(coords)

def get_polarity_m_1(m_in, micrograph_pixel_size):
    """ Get polarity assignment using original coordinates.
    This is used to assess the polarity before smooth.
    Because the aligned particles might have same center
    """
    coords = get_values(m_in, micrograph_pixel_size)
    coords[:, 0] = m_in.getValues("CoordinateX")
    coords[:, 1] = m_in.getValues("CoordinateY")
    if m_in[0].hasKey("CoordianteZ"):
        coords[:, 2] = m_in.getValues("CoordinateZ")
    return get_polarity(coords)

def get_polarity(coords):
    # The vector for each ptcl, except last one
    coord_diff = np.zeros([coords.shape[0], 3])
    coord_diff[1:, :] = coords[1:, :3] - coords[:-1, :3]
    coord_diff[0, :] = coord_diff[1,:]
    dists = np.sqrt(np.sum(coord_diff * coord_diff, axis=1))
    dists[dists==0] = 1
>>>>>>> findkin
    coord_diff = (coord_diff.T / dists).T
    from cryofilia.euler import Euler
    coord_from_angle = np.zeros(coord_diff.shape)
    unit_v = np.array([0,0,1])
    for idx in range(coord_diff.shape[0]):
        coord_from_angle[idx, :] = Euler(coords[idx, 3], coords[idx, 4], coords[idx, 5]).transform(unit_v)
    polarity = coord_diff * coord_from_angle
    polarity = np.sum(polarity, axis=1)
<<<<<<< HEAD
    polarity = np.round(polarity)
=======
    polarity = np.sign(polarity)
>>>>>>> findkin
    return polarity

def interpolate_coords_m(m_in, smooth_method=SMOOTH_NONE, winsize=7,
                         micrograph_pixel_size=None,
                         pixel_size=None, scale=None,
                         fit_order=2,
                         expect_dphi=0.):
    """ Interpolate coords using metadata input
    """
    coords = get_values(m_in, micrograph_pixel_size=micrograph_pixel_size)
    coord_indices = np.array([ptcl.index for ptcl in m_in])
    fitnesses = np.array([ptcl.fitness for ptcl in m_in])
    coords_interpolated, interpolated, original_indices = interpolate_coords(
        coord_indices, coords, fitnesses, smooth_method=smooth_method,
        winsize=winsize, fit_order=fit_order, expect_dphi=expect_dphi)
    m_result = m_in.new_like(m_in)
    ori_idx = 0

    for index in range(len(interpolated)):
        if not interpolated[index]:
            ori_idx = int(original_indices[index])
        ptcl = m_in[ori_idx].copy()
        ptcl.x = coords_interpolated[index][0]
        ptcl.y = coords_interpolated[index][1]
        ptcl.z = coords_interpolated[index][2]
        ptcl.shx = 0
        ptcl.shy = 0
        ptcl.shz = 0
        ptcl.phi = coords_interpolated[index][3]
        ptcl.theta = coords_interpolated[index][4]
        ptcl.psi = coords_interpolated[index][5]
        m_result.append(ptcl)
        ptcl.interpolated = interpolated[index]
    m_in.clear()
    m_in += m_result
    return m_in

def interpolate_coords(coord_indices, coords, fitnesses,
                       smooth_method=SMOOTH_XY,
                       winsize=7, fit_order=2,
                       expect_dphi=0.,
                       expect_dtheta=0.,
                       expect_dpsi=0.):
    """ Interploate coords using indexed points.
    This will add point if the two indices are not adjacent,
    or remove point is the adjacent points have same index
    :param coord_indecs: The index for each point.
    :param coords: The points. Could be xy or angles
    :param fitnesses: The distances from the point to expected location
    :param smooth_all:
    :param winsize:
    :type coord_indices: ndarray 1d
    :type coords: ndarray nd
    :type fitnesses: ndarray 1d
    :type smooth_all: int
    :type winsize: int
    :return: coords_interpolated, interpolated, original_indices
            -1 in original_indices is interplated.
    :rtype: ndarray, ndarray, ndarray
    """
<<<<<<< HEAD
    ndim = coords.shape[1]
    coord_indices = coord_indices.astype(np.int32)
    ind_sorted = np.lexsort((fitnesses, coord_indices))

=======
    coords = coords[coord_indices>=0]
    fitnesses = fitnesses[coord_indices>=0]
    coord_indices = coord_indices[coord_indices>=0]
    ndim = coords.shape[1]
    coord_indices = coord_indices.astype(np.int32)
    ind_sorted = np.lexsort((fitnesses, coord_indices))
>>>>>>> findkin
    indices, index_of_indices = np.unique(coord_indices[ind_sorted], return_index=True)

    coord_indices_sort = coord_indices[ind_sorted][index_of_indices]
    coords_sort = coords[ind_sorted][index_of_indices]
    original_index_sort = ind_sorted[index_of_indices]
    # The number in interpolated results
    n_result = int(coord_indices_sort[-1] + 1)
    coords_interpolated = np.zeros([n_result, ndim])
    interpolated = np.zeros(n_result, dtype=np.bool)
    original_indices = np.zeros(n_result, dtype=np.int32) - 1
    good_indices = {}
<<<<<<< HEAD

=======
>>>>>>> findkin
    for idx, index in enumerate(indices):
        good_indices[index] = idx
    n_good_indices = len(good_indices)
    idx_in_ori = 0
    for idx_in_result  in range(n_result):
        if idx_in_result in good_indices:
            idx_in_ori = good_indices[idx_in_result]

        if idx_in_result not in good_indices:
            interpolated[idx_in_result] = True
        else:
            original_indices[idx_in_result] = original_index_sort[idx_in_ori]
<<<<<<< HEAD
        if not interpolated[idx_in_result] and smooth_method != SMOOTH_ALL:
=======
        if ((not interpolated[idx_in_result] )
            and smooth_method != SMOOTH_ALL):
>>>>>>> findkin
            coords_interpolated[idx_in_result] = coords_sort[idx_in_ori]
        else:
            # need interploate
            # idx_in_ori is the first point after idx_in_result
            # we need point before idx_in_ori and after idx_in_ori
            # use data in coords_sorted, where unique indices has length winsize
            min_i, max_i = get_window(idx_in_ori, winsize, n_good_indices)
            if max_i - min_i + 1 < fit_order+1:
                print "Warning: Too less point for interpolate, skipping"
            else:
                idx_tmp = coord_indices_sort[min_i:max_i+1]
                coord_tmp = coords_sort[min_i:max_i+1]
                # interplate
                # do lsq fit on coords
<<<<<<< HEAD
                #print fit_order
                #print idx_tmp, coord_tmp[:, :]
                if max_i - min_i + 1 >= fit_order + 1:
                    coord_params = trimmed_lsq_fit_arr(
                        idx_tmp, coord_tmp[:, :3], fit_order=fit_order, lsq=True)[0]
                # print coord_params, trimmed, best_error
=======
                if max_i - min_i + 1 >= fit_order + 1:
                    coord_params = trimmed_lsq_fit_arr(
                        idx_tmp, coord_tmp[:, :3], fit_order=fit_order, lsq=True)[0]
>>>>>>> findkin
                poly = np.poly1d(coord_params[0,:])
                coords_interpolated[idx_in_result, 0] = poly(idx_in_result)
                poly = np.poly1d(coord_params[1,:])
                coords_interpolated[idx_in_result, 1] = poly(idx_in_result)
                poly = np.poly1d(coord_params[2,:])
                coords_interpolated[idx_in_result, 2] = poly(idx_in_result)
                # do lsq fit to get all angles
                for dim in xrange(3, ndim):
<<<<<<< HEAD
    
=======
>>>>>>> findkin
                    if dim == 3:
                        delta = expect_dphi
                    elif dim == 4:
                        delta = expect_dtheta
                    elif dim == 5:
                        delta = expect_dpsi
                        delta = 0.
<<<<<<< HEAD
    
=======
>>>>>>> findkin
                    angle_param, trimmed, best_error = trimmed_lsq_fit_arr(
                        idx_tmp,
                        fix_rollover(coord_tmp[:, dim], delta, indices=idx_tmp),
                        fit_order, lsq=True)
                    poly = np.poly1d(angle_param)
                    coords_interpolated[idx_in_result, dim] = poly(idx_in_result)
<<<<<<< HEAD

            idx_in_result += 1

    return coords_interpolated, interpolated, original_indices

def index_coords_m(m_in, spacing, winsize=7, micrograph_pixel_size=1.,
                   pixel_size=None, scale=None):
    """ Index coords using metadata input
    """
    coords = get_values(m_in, micrograph_pixel_size)[:, :3]
    coords_indices, fitness, coord_estimates = index_coords(coords, spacing, winsize)
=======
            idx_in_result += 1
    #plt.cla()
    #plt.plot(np.linalg.norm(coords_interpolated[1:, :3] - coords_interpolated[:-1, :3], axis=1) * 1.13)
    #plt.savefig('a.jpg')
    #plt.cla()
    #plt.axis('equal')
    #plt.plot(coords_interpolated[:, 0], coords_interpolated[:, 1], 'x-', label='inerp')
    #plt.plot(coords[:, 0], coords[:, 1], '+-', label='ori')
    #plt.legend()
    #plt.savefig('b.jpg')
    #exit()
    return coords_interpolated, interpolated, original_indices

def interp_discontinuity_m(fila_smooth, micrograph_pixel_size, expect_dphi=0.):
    coords = get_values(fila_smooth, micrograph_pixel_size=micrograph_pixel_size)
    discontinuity = np.array(fila_smooth.getValues("discontinuity"))
    
    discontinuity[-1] = False # TO keep the filament the same length as original
    coords_interpolated, interpolated, original_indices = interp_discontinuity(
        coords, discontinuity, expect_dphi)
    for idx in xrange(len(fila_smooth)):
        if interpolated[idx]:
            ptcl = fila_smooth[idx]
            coord = coords_interpolated[idx, :]
            ptcl.x = coord[0]
            ptcl.y = coord[1]
            ptcl.z = coord[2]
            ptcl.shx = 0
            ptcl.shy = 0.
            ptcl.shz = 0.
            ptcl.phi = coord[3]
            ptcl.theta = coord[4]
            ptcl.psi = coord[5]
    
    
def interp_discontinuity(coords, discontinuity, expect_dphi=0.):
    """ Interpolate discontinuity points
    """
    indices = np.arange(coords.shape[0])
    indices = indices[discontinuity < 0.5]
    coords_good = coords[indices]
    coords_interpolated, interpolated, original_indices = interpolate_coords(
        indices, coords, np.zeros(coords_good.shape[0]), expect_dphi=expect_dphi)
    return coords_interpolated, interpolated, original_indices

def index_coords_m(m_in, spacing, winsize=7, micrograph_pixel_size=1.,
                   pixel_size=None, scale=None, polarity=None):
    """ Index coords using metadata input
    """
    coords = get_values(m_in, micrograph_pixel_size)[:, :3]
    # index for half spacing first
    coords_indices, fitness, coord_estimates = index_coords(
            coords, spacing/2, winsize, polarity=polarity)
    # check which spacing is better
    coords_indices_parity = coords_indices % 2
    if sum(coords_indices_parity) < coords_indices.size / 2:
        parity = 0
    else:
        parity = 1
    need_redo_indexing = False
    # let move every even coordinates half towards to the odd indices
    for i in range(coords_indices.size):
        if coords_indices_parity[i] % 2 == parity:
            need_redo_indexing = True
            # get direction to move
            amp = 0 
            i1, j1 = i, i - 1
            while abs(amp) < spacing * 3:
                if j1 < 0:
                    i1 += 1
                    if i1 >= coords_indices.size:
                        print("Warning: Could not get direction")
                        amp = 0
                        break
                    j1 = 0
                direction = coords[i1,:3] - coords[j1, :3]
                amp = np.linalg.norm(direction)
                j1 -= 1
            if amp > 0:
                direction = direction / amp
                # move towards direction half spacing
                coords[i, :3] += direction * (spacing / 2)
    # redo the indicing if any adjustment in coords
    if need_redo_indexing:
        coords_indices, fitness, coord_estimates = index_coords(
            coords, spacing, winsize,
            polarity=polarity)
>>>>>>> findkin
    # put result into m_in
    for index in range(len(m_in)):
        ptcl = m_in[index]
        ptcl.index = coords_indices[index]
        ptcl.fitness = fitness[index]
        ptcl.x = coord_estimates[index, 0]
        ptcl.y = coord_estimates[index, 1]
        ptcl.shx = 0
        ptcl.shy = 0
    return m_in

<<<<<<< HEAD
def index_coords(coords, spacing, winsize=7):
    """ Get the index of coords with equal space
=======
def index_coords(coords, spacing, winsize=7, polarity=None):
    """ Get the index of coords with equal space.
    Indexes only use points that is correct polarity, wrong polarity will be assigned as -1 
>>>>>>> findkin
    :param coords: The input coords. All points should in same R sapce.
    :type coords: ndarray
    :param spacing: The single spacing between points.
    :type spacing: float
    :param winsize: The window size for averaging. Default is 7
    :type winsize: int.
    :return: coord_indices, fitnesses, coord_estimates
    :rtype: ndarray, ndarray, ndarray
    """
<<<<<<< HEAD
=======
    ori_coords = coords
    if polarity is None:
        polarity = np.ones(ori_coords.shape[0])
    coords = ori_coords[polarity>0]
>>>>>>> findkin
    n_coords, ndim = coords.shape
    coord_indices = np.zeros(n_coords)
    coord_estimates = np.zeros([n_coords, ndim])
    fitnesses = np.zeros(n_coords)
    coord_trimmed = np.zeros(n_coords)
    last_trimmed = np.array([False] * n_coords)
    first_ind = 0
    last_ind = n_coords - 1
    if n_coords < winsize:
        winsize = n_coords
    last_max_i, last_coord_index = None, []
    for ind in range(first_ind, last_ind + 1):
        min_i, max_i = get_window(ind, winsize, n_coords)
        ind_local = ind - min_i
        unit_vec, origin, trimmed = trimmed_nd_lsq_fit(coords[min_i:max_i+1, :3], lsq=True)[:3]
        coord_index, spacing_adjust = index_equispaced(
            coords[min_i:max_i+1, :3] - origin, unit_vec, spacing, trimmed)
        coord_trimmed[ind] = trimmed[ind_local]
        ind2 = np.arange(winsize)[~trimmed]
        index_centroid = sum(coord_index[ind2]) / ind2.size
        adjusted_origin = origin - index_centroid * spacing_adjust * unit_vec
        offset = coord_index[ind_local] * spacing_adjust * unit_vec
        coord_estimates[ind, :3] = adjusted_origin + offset
        fitnesses[ind] = np.linalg.norm(coords[ind, :] - coord_estimates[ind, :])
        if ind == first_ind:
            coord_index_offset = 0
        else:
            current_coord_index = np.zeros(n_coords)
            current_coord_index[min_i:max_i+1] = coord_index
            last_trimmed[min_i:max_i+1] |= trimmed
            match_min_i = min_i
            match_max_i = last_max_i
            last_coord_index = last_coord_index[match_min_i:match_max_i+1]
            last_coord_index = last_coord_index[~last_trimmed[match_min_i:match_max_i+1]]
            current_coord_index = current_coord_index[match_min_i:match_max_i+1]
            current_coord_index = current_coord_index[~last_trimmed[match_min_i:match_max_i+1]]
            cnt = Counter(last_coord_index - current_coord_index)
            offset = cnt.most_common(1)[0][0]
            coord_index_offset += offset
        coord_indices[ind] = coord_index[ind_local] + coord_index_offset
        last_coord_index = np.zeros(n_coords)
        last_coord_index[min_i:max_i+1] = coord_index
        last_trimmed = np.array([False] * n_coords)
        last_trimmed[min_i:max_i+1] = trimmed
        last_max_i = max_i
    coord_indices -= coord_indices[0]
<<<<<<< HEAD
=======
    coord_indices_good = coord_indices
    coord_indices = -np.ones(ori_coords.shape[0])
    coord_indices[polarity>0] = coord_indices_good
    fitnesses_good = fitnesses
    fitnesses = np.ones(ori_coords.shape[0]) * 100000000
    fitnesses[polarity>0] = fitnesses_good
    coord_estimates_good = coord_estimates
    coord_estimates = ori_coords.copy()
    coord_estimates[polarity>0, :] = coord_estimates_good
>>>>>>> findkin
    return coord_indices, fitnesses, coord_estimates

def fixup_seam_pos_star(m_star, rise_per_subunit, twist_per_subunit,
                        twist_per_repeat, micrograph_pixel_size=None,
                        pixel_size=None):
    """ Fixup the seam position in the metadata
        :param m_star: The metadata input
        :param twist_per_subunit: The twist angle between subunit. CC positive
        :param rise_per_repeat: The distance along helical axis between repeats
        :param twist_per_repeat: The twist angle between repeat. CC positive
        :param micrograph_pixel_size: The micrograph pixel size in Angstrom
                    to calculate the rise in pixel
                    and to calculate the coordinates
        :type m_star: Metadata
        :type rise_per_subunit: float
        :type twist_per_subunit: float
        :type twist_per_repeat: float
        :type micrograph_pixel_size: float
        :type pixel_size: float
        :return: The fixed metadata
        :rtype: Metadata
    """
    tubs = m_star.group_by("MicrographName", "HelicalTubeID")
    new_tubs = m_star.new_like(m_star)
    for mgname, tubid in tubs.iterkeys():
        tub = tubs[(mgname, tubid)]
        tub = fixup_seam_pos_filament_star(tub, rise_per_subunit,
                                           twist_per_subunit, twist_per_repeat,
                                           micrograph_pixel_size, pixel_size)
        new_tubs += tub
    return new_tubs

def fixup_seam_pos_filament_star(fila_star, rise_per_subunit, twist_per_subunit,
                        twist_per_repeat, micrograph_pixel_size=None,
                        pixel_size=None):
    """ Fixup the seam position in the filament with metadata
        The origin(shift) in relion is reported as pixels.
        :param fila_star: The metadata input for one filament
        :param twist_per_subunit: The twist angle between subunit. CC positive
        :param rise_per_repeat: The distance along helical axis between repeats
        :param twist_per_repeat: The twist angle between repeat. CC positive
        :param micrograph_pixel_size: The micrograph pixel size in Angstrom
                    to calculate the rise in pixel
                    and to calculate the coordinates
        :type fila_star: Metadata
        :type rise_per_subunit: float
        :type twist_per_subunit: float
        :type twist_per_repeat: float
        :type micrograph_pixel_size: float
        :type pixel_size: float
        :return: The fixed metadata
        :rtype: Metadata
    """
    if len(fila_star) == 0:
        logger.warn("No need to fixup for only one ptcl")
        return fila_star.copy()
    if micrograph_pixel_size is None or micrograph_pixel_size <= 0:
        raise Exception("micrograph_pixel_size must be provided")
    if pixel_size is None or pixel_size <= 0:
        if ((not fila_star.hasLabel("Magnification"))
            or (not fila_star.hasLabel("DetectorPixelSize"))):
            raise RuntimeError("input should contain both Magnificaiton "
                               + "and DetectorPixelSzie")
        pixel_size = 10000. * fila_star[0]['DetectorPixelSize'] / fila_star[0]['Magnification']
    scale = pixel_size / micrograph_pixel_size
    xs0, ys0 = fila_star.getValues(["CoordinateX", "CoordinateY"])
    phis, thetas, psis = fila_star.getValues(["AngleRot", "AngleTilt",
                                             "AnglePsi"])
    coords = get_values(fila_star, micrograph_pixel_size)
    center_xs = coords[:, 0].tolist()
    center_ys = coords[:, 1].tolist()
    # fix psis
    psis_model = fix_rollover(psis, 0, 180)
    #psis_model = smooth_angle(psis, 4, 1, 20)[0]
    for index in xrange(len(psis_model)):
        if abs(psis[index] - psis_model[index]) > 10:
<<<<<<< HEAD
            #print (psis[i] - psis_model[i] + 540) % 360 - 180
=======
>>>>>>> findkin
            if abs((psis[index] - psis_model[index] + 540) % 360 - 180) > 90:
                # reverse the ptcl
                thetas[index] = 180 - thetas[index]
                phis[index] = (phis[index] + 180) % 360
            psis[index] = psis_model[index]
    xs1, ys1, phis1, thetas1, psis1 = fixup_seam_pos(
        center_xs, center_ys,
        phis, thetas, psis,
        rise_per_subunit/micrograph_pixel_size,
        twist_per_subunit, twist_per_repeat)
    # reconstruct the shx, shy and phi
    m_result = fila_star.new_like(fila_star)
    for index in xrange(len(xs0)):
        ptcl = fila_star[index].copy()
        ptcl['OriginX'] = (xs0[index] - xs1[index]) / scale
        ptcl['OriginY'] = (ys0[index] - ys1[index]) / scale
        ptcl['AngleRot'] = phis1[index]
        ptcl['AnglePsi'] = psis1[index]
        ptcl['AngleTilt'] = thetas1[index]
        m_result.append(ptcl)
    return m_result

def fixup_seam_pos(coord_xs, coord_ys, phis, thetas, psis,
                       rise_per_subunit, twist_per_subunit,
                       twist_per_repeat):
    """ Bring the phi angle to same protofilimant
        Do not change theta and psi.
        :param coord_xs: The input x coordinates
        :param coord_ys: The input y coordinates
        :param phis: The input phi angles. Used to calculate the pf
        :param thetas: The input theta angles. Used to calculate the shift
        :param psis: The input psi angles. Used to calculate the shift
        :param twist_per_subunit: The twist angle between subunit. CC positive
        :param rise_per_repeat: The distance along helical axis between repeats
        :param twist_per_repeat: The twist angle between repeat. CC positive
        :type coord_xs: list[float]
        :type coord_ys: list[float]
        :type phis: list[float]
        :type thetas: list[float]
        :type psis: list[float]
        :type rise_per_subunit: float
        :type twist_per_subunit: float
        :type twist_per_repeat: float
        :return: The fixed xs, ys, phis
    """
<<<<<<< HEAD
    # print xs, ys, phis, thetas, psis
    # print rise_per_subunit, twist_per_subunit, twist_per_repeat
=======

>>>>>>> findkin
    if len(set([len(coord_xs), len(coord_ys), len(phis)])) != 1:
        raise RuntimeError("The input data are not same length")
    if len(coord_xs) == 1:
        return coord_xs[:], coord_ys[:], phis[:], thetas[:], psis[:]
    result_xs1 = coord_xs[:]
    result_ys1 = coord_ys[:]
    phis = phis[:]
    # bring phi to same protofilament
    new_phis = fix_rollover(phis, twist_per_repeat, twist_per_subunit)
    # shift x, y
    for index in xrange(len(result_xs1)):
        phi_diff = new_phis[index] - phis[index]
        psi = psis[index] * np.pi / 180.
        # calculate how much we need to shift in z
        # to match original volume.
        rise_diff = phi_diff / twist_per_subunit * rise_per_subunit
<<<<<<< HEAD
        #print dz, phi_diff / twist_per_subunit
        # calculate how much we need to shift x, y after projection
        # to match original projection
        # print phi_diff / twist_per_subunit
        dxy = rise_diff * np.sin(-thetas[index] * np.pi / 180.)
        #print dz, dxy
        result_xs1[index] += dxy * np.cos(-psi)
        result_ys1[index] += dxy * np.sin(-psi)
        #print xs[i], ys[i]
=======
        # calculate how much we need to shift x, y after projection
        # to match original projection
        dxy = rise_diff * np.sin(-thetas[index] * np.pi / 180.)
        result_xs1[index] += dxy * np.cos(-psi)
        result_ys1[index] += dxy * np.sin(-psi)
>>>>>>> findkin
    return result_xs1, result_ys1, new_phis, thetas, psis

