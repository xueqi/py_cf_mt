"""
"""

import os
import numpy as np
from cryofilia.EMImage import EMImage
from relion import MPIParameter
from cryofilia.helix.filament.microtubule import make_syn_mt, make_syn_mt_kinesin
from cryofilia.proj import Projector
from cryofilia.helix.smooth_function import fix_rollover, SmoothCoords, SmoothedCoordinates
from relion import Metadata
import logging

logger=logging.getLogger(__name__)
EPSILON=0.0001
from .model import FilamentParticle
class RefAlignData(Metadata):
    _labels={
            "mg_name"   : ["MicrographName"],
            "id"    : ["HelicalTubeID"],
            "ccc"   : ["crossCorrelation", float, "cross correlation coefficient"],
            "pfs"   : ["pfs", int, "number of protofilaments"],
            "starts"    : ["starts", float, "number of starts"],
            "radon_done": ["radonDone", bool, "radon has been done"],
            "phi0"  : ["phiNoSeam", float, "phi angle before seam search"],
            "selected" : ["selectedSeam", bool, "Selected by select seam op"],
            }
    _labels.update(FilamentParticle._labels)
RefAlignData()

class RefAlignParameter(MPIParameter):
    def __init__(self, *args, **kwargs):
        super(RefAlignParameter, self).__init__()

    def add_arguments(self):
        self.add_argument('input_star',
            help="The input star file", type=str, required=True,
            group="I/O")
        self.add_argument('output_star',
            help="The output star file. optional", type=str,
            group="I/O")
        self.add_argument('ref_volume',
            help="the reference volume list", nargs="*", type=str,
            group="I/O")
        self.add_argument('num_pfs',
            help="Number of protofilament", nargs="*", type=int, default=1,
            group="Microtubule")
        self.add_argument('pdb',
            help="13 pfs pdb fit", nargs = "+",
            group="Microtubule")
        self.add_argument('num_starts',
            help="Number of starts", type=float,
            group="Microtubule")
        self.add_argument("box_size",
            help="box size for alignemnt", type=int,
            group="Refinement")
        self.add_argument("bin_factor",
            help="bin factor for ref align. This is relative bin compare to "
                 + "micrograph", type=float,
            group="Refinement")
        self.add_argument("voxel_size",
            type = float, help = "voxel_size. default to pixel size if not "
                 + "provided",
            group="Refinement")
        self.add_argument("no_decoration",action="store_true", 
            help="no decoration. bare microtubule",
            group="Microtubule")
        self.add_argument("coarse_dphi", type=float,
            help= "coarse rotation for align", default=3,
            group="Refinement")
        self.add_argument("coarse_dtheta",type=float,
            help="coarse tilt for align", default=5,
            group="Refinement")
        self.add_argument("theta_range", type=float,
            help="Theta range for search", default=30,
            group="Refinement")
        self.add_argument("dont_write_match", action="store_true",
            help="do not write stack match",
            group="Refinement")
        self.add_argument("stack_box_size", type=int,
            help="output stack dimension. Default is same as refine",
            group="Refinement")
        self.add_argument("helical_rise", default = 80, type=float,
            help="Initial helical rise",
            group="Refinement")
        self.add_argument("helical_twist", type=float,
            help="Initial helical twist",
            group="Refinement")
        self.add_argument("nsubunits", type=float,
            help="Number of subunits per repeat for ref_align", default=4,
            group="Refinement")
        self.add_argument("debug", action="store_true",
            help="Debug session", group="Advanced")
        self.add_argument("cache_dir", help="cache directory for projection",
            group="Advanced")
        self.add_argument("output_relion", action="store_true",
            help="Also output for relion result", group="Advanced")
        self.add_argument("do_not_smooth", action="store_true",
            help="Do not smooth the coords", group="Advanced")
        self.add_argument("oversample", type=float, default=1,
            help="Oversample factor for projection", group="Advanced")
        self.add_argument("mt_name",
            help="test microtubule name", group="Advanced")
        self.add_argument("ctf", action="store_true",
            help="use ctf", group="Advanced")
        super(RefAlignParameter, self).add_arguments()

class RefAlign(RefAlignParameter):
    """ RefAlign is a multi reference alignment method
    """
    def __init__(self, *args, **kwargs):
        super(RefAlign, self).__init__(*args, **kwargs)
        self._ptcl_edge_mask = None
        self._project_mask = None
        self.projectors = {}
        self._x_range = None
        self.cf = None
        self.star_dir = None
        self.selected_dir = None
        self.radon = None
        self.smooth = None
        self._ccmask = None
        self._cccmask = None
        self.pixel_size = None
<<<<<<< HEAD
        
=======
        self.cache_dir = None
>>>>>>> findkin
    def initialize(self):
        ''' Initialize parameters from command line
        '''
        if self._rank == 0:
            # read parameter file from cf-parameters.m
            from chuff import ChuffParameter
            self.cf=ChuffParameter()
            self.workdir=self.workdir or self.get_next_workdir()
            # add handler for root logger
            logging.getLogger().addHandler(logging.FileHandler(os.path.join(self.workdir, 'ref_align.log')))
            if not self.input_star:
                self.error(RefAlignError("Must provide the input star file"))
            if not os.path.exists(self.input_star):
                self.error(RefAlignError("The input star file %s does not exist" % self.input_star))
            if not self.output_star:
                self.output_star="%s/%s_out.star" % (self.workdir, os.path.basename(os.path.splitext(self.input_star)[0]))
            self.num_starts=self.num_starts or 3
            if self.bin_factor == 0:
                self.bin_factor = int(5. / self.cf.pixel_size) + 1
            self.pixel_size = self.cf.pixel_size * self.bin_factor
            self.box_size =max(0, self.box_size)
            self.num_pfs=self.num_pfs or [12, 13, 14]

            # ignore box_size if too small
            if self.box_size < 16:
                self.box_size = 0 
            self.star_dir = self.path('stars')
            self.mkdir(self.star_dir)
            self.mkdir(self.match_dir)
            self.selected_dir = self.path('selected')
            self.mkdir(self.selected_dir)
            self.mkdir(self.plot_dir)
            # default use pixel size if voxel_size not provided
            if self.ref_volume and self.voxel_size == 0:
                self.error("Voxel size must be provided if ref_volume is provided")
            self.voxel_size = self.voxel_size or self.pixel_size
            # check if the ref_volumes exists if provided
            for ref_vol in self.ref_volume:
                if not os.path.exists(ref_vol):
                    self.error("Reference volume %s provided, but it does not exists" % ref_vol)
            # get box size
            if not self.box_size: # no self.box_size provided. eg. box_size = 0
                if len(self.ref_volume) == 0:
                    if self.cf.filament_outer_radius is not None:
                        self.box_size = int(self.cf.filament_outer_radius * 3 / self.voxel_size / 4 + 1) * 4
                    else:
                        self.error("could not get box size without filament_outer_radius provided.\nPlease provide box_size.")

                else:
                    # ref_volume is not Empty, get the volume size
                    vol_size = EMImage.get_image_size(self.ref_volume[0])[0]
                    # calculate the maximum volume size after resampling
                    max_size_allowed = vol_size * self.voxel_size / self.pixel_size
                    # make the box_size divisible by 4
                    self.box_size = int(max_size_allowed / 4) * 4
                    if max_size_allowed != self.box_size:
                        resize_volume = True
            self.stack_box_size = self.stack_box_size or self.box_size
            self.init_smooth_coords()
            self.init_radon()
            if not self.cache_dir:
                try:
                    self.cache_dir = self.cf.cache_dir
                except:
                    self.warn("Cache dir is not set in cf parameter.\nNo cache is used for projection")
            
            if not self.ref_volume:
                ref_vol_dir=os.path.join(self.workdir, 'refs')
                if not os.path.exists(ref_vol_dir):
                    self.mkdir(ref_vol_dir)
                for num_pf in self.num_pfs:
                    ref_vol=os.path.join(ref_vol_dir, 'mt_pf%d.mrc' % num_pf)
                    self.ref_volume.append(ref_vol)
                self.ref_volume=",".join(self.ref_volume)
            logger.info("====================RefAlign Param=================")
            logger.info(str(self))
            logger.info("===================================================")
            self.save()
        else:
            self.pixel_size = None
        # bcast properties
        self.bcasts()
        if len(self.num_pfs) != len(self.ref_volume):
            self.error("The number of symmetry is not the same as the number of ref_volumes provided")
        # bcast others
        self.pixel_size = self.bcast(self.pixel_size)
        self.cf = self.bcast(self.cf)
        self.star_dir = self.bcast(self.star_dir)
        self.selected_dir = self.bcast(self.selected_dir)
        self.radon = self.bcast(self.radon)
        self.smooth = self.bcast(self.smooth)

    def run(self):
        ''' The main control for reference alignment for microtubule.
        '''
        # set default values
<<<<<<< HEAD
=======
        import os
        if 'CF_CACHE_DIR' in os.environ:
            cache_dir = os.environ['CF_CACHE_DIR']
            if os.path.exists(cache_dir):
                self.cache_dir = cache_dir
                logger.info("Using cache dir %s" % cache_dir)
>>>>>>> findkin
        self.initialize()
        # get references
        for ref_vol, num_pf in zip(self.ref_volume, self.num_pfs):
            # make synthetic volume if not exists
            vol = self.readVolume(ref_vol, num_pf)
            projector = Projector(vol, cache_dir=self.cache_dir, tag=num_pf)
            projector.voxel_size = self.pixel_size
            self.projectors[num_pf] = projector

        logger.info("Search Range along axis: [%d %d]" % tuple(self.x_range))
        # parallel work on micrographs, return all star file informations
<<<<<<< HEAD
        results = self.parallel_job(self.ref_align_mg, self.generate_args())
=======
        results = self.parallel_job("self.ref_align_mg", self.generate_args())
>>>>>>> findkin

        if self._rank == 0:
            result = sum(results, None)
            result.write(self.output_star)
            result.clean_for_relion().write("%s_relion%s" % os.path.splitext(self.output_star))
            # write pfs
            pfs = result.group_by('pfs')
            for pf, m_pf in pfs.items():
                m_pf_fname = "%s_pf%d.star" % (os.path.splitext(self.output_star)[0], pf)
                m_pf.write(m_pf_fname)
                print m_pf_fname, len(m_pf), "particles"
                m_pf.clean_for_relion().write("%s_relion%s" % os.path.splitext(m_pf_fname))
            # also write micrograph_ctf.star for relion
            a =  result.new_like(result)
            mgs = result.group_by("MicrographName")
            for mgname, mg in mgs.items():
                a.append(mg[0])
            a.clean_for_relion().write(os.path.join(self.workdir, 'micrographs_ctf.star'))


    def generate_args(self):
        ''' Generate argument for parallel jobs
        '''
<<<<<<< HEAD
        if self._rank != 0: return
=======
        if self._rank != 1: return
>>>>>>> findkin
        # read particle information
        st = RefAlignData.readFile(self.input_star)
        # group by micrograph
        mgs=st.group_by('rlnMicrographName')
        logger.info("Total %d micrographs." % len(mgs))
        for mgname, mg in mgs.items():
            if self.mt_name:
                mgname1 = "_".join(self.mt_name.split("_")[:-1])
                if not mgname1 in mgname: continue
            yield [mg], {}

    def readVolume(self, ref_vol, num_pf):
        ''' read reference volume. If not exists, make a synthetic one
        '''
        if self._rank == 0:
            if not os.path.exists(ref_vol):
<<<<<<< HEAD
=======

>>>>>>> findkin
                logger.info("Generating synthetic volume for pf%d: %s" % (num_pf, ref_vol))
                # generate a volume with oversample=2, and write to disk
                self.oversample = 2
                ref_com = None
                if len(self.pdb) == 0:
                    pdb = []
                else:
                    pdb = self.pdb
                    from cryofilia.pdb_util import PDB
                    ref_com = PDB.get_center(self.pdb[0])
<<<<<<< HEAD
                if self.no_decoration:
                    vol=make_syn_mt(tubpdbfiles=pdb[:1], 
                            num_pf=num_pf, 
                            voxelSize=self.voxel_size/self.oversample, 
                            boxSize=self.box_size*self.oversample, 
                            ref_com = ref_com, ref_num_pf=13)
                else:
                    vol=make_syn_mt_kinesin(tubpdbfiles=pdb[1:], 
                            num_pf=num_pf, 
                            voxelSize = self.voxel_size/self.oversample, 
                            ref_com = ref_com, 
                            ref_num_pf = 13,
                            boxSize=self.box_size * self.oversample)
                from cryofilia.filter import FourierFilter as ff
                rl, rh = 21,19
                bl, bh = self.voxel_size / self.oversample / rl, self.voxel_size / self.oversample / rh
                ff.butterworth(bl, bh, image=vol)
                vol.resample(self.box_size)
                vol.write_image(ref_vol)
            else:
                vol=EMImage(ref_vol)
                stdv = np.std(vol.data)
                if stdv != 0:
                    vol /= stdv
                vol_bin_factor=self.pixel_size / self.voxel_size
=======
                
                vol_params = (tuple(pdb), num_pf, self.voxel_size/self.oversample, self.box_size * self.oversample, tuple(ref_com or []))
                vol_params_hash = str(hash(vol_params))
                if self.cache_dir:
                    if os.path.exists(os.path.join(self.cache_dir, vol_params_hash)):
                        tmp_ref_vol = open(os.path.join(self.cache_dir, vol_params_hash)).read()
                        if os.path.exists(tmp_ref_vol):
                            logger.info("Using cached volume %s for pf%d" % (tmp_ref_vol, num_pf))
                            os.symlink(tmp_ref_vol, ref_vol)
                            vol = EMImage(ref_vol)
                        else:
                            os.remove(os.path.join(self.cache_dir, vol_params_hash))
                # we could not find from cache also
                if not os.path.exists(ref_vol):
                    if self.no_decoration:
                        vol=make_syn_mt(tubpdbfiles=pdb[:1], 
                                num_pf=num_pf, 
                                voxelSize=self.voxel_size/self.oversample, 
                                boxSize=self.box_size*self.oversample, 
                                ref_com = ref_com, ref_num_pf=13)
                    else:
                        vol=make_syn_mt_kinesin(tubpdbfiles=pdb[1:], 
                                num_pf=num_pf, 
                                voxelSize = self.voxel_size/self.oversample, 
                                ref_com = ref_com, 
                                ref_num_pf = 13,
                                boxSize=self.box_size * self.oversample)

                    from cryofilia.filter import FourierFilter as ff
                    rl, rh = 21,19
                    bl, bh = self.voxel_size / self.oversample / rl, self.voxel_size / self.oversample / rh
                    ff.butterworth(bl, bh, image=vol)
                    vol.resample(self.box_size)
                    vol.write_image(ref_vol)
                    if self.cache_dir:
                        open(os.path.join(self.cache_dir, vol_params_hash), 'w').write(os.path.realpath(ref_vol))
            else:
                vol = EMImage(ref_vol)
                stdv = np.std(vol.data)
                if stdv != 0:
                    vol /= stdv
                vol_bin_factor = self.pixel_size / self.voxel_size
>>>>>>> findkin
                # resample the volume if needed. Synthetic volume should be always same as the boxsize
                if abs(vol_bin_factor - 1.) > -.01:
                    vol.resample(scale=1. / vol_bin_factor)
                    vol_size=vol.get_xsize()
                    # check if vol_size is the same as box_size
                    if self.box_size > vol_size:
                        raise Exception("box_size: %d !>  voxel_size: %d\nThis should not happen" % (self.box_size, self.voxel_size))
                    vol=vol.clip(self.box_size)
        else:
            vol = None
        self.voxel_size = self.bcast(self.voxel_size)
        vol = self.bcast(vol)
        return vol

    def ref_align_mg(self, m_mg):
        ''' Reference alignment on one micrographs. One micrograph might has more than one filament. Group to micrograph to save time on some common operations.
        Not really make sense now.
        '''
        # This add extra labels for m_mg, which is used to store result in m_mg, and also keep the old labels(True?). 
        # This is used to access the field using the common name, not from the label.
        if not isinstance(m_mg, RefAlignData):
            m_mg = RefAlignData.copy_from(m_mg)

        from cryofilia.ctf import CTF
        has_ctf_info = True
        try:
            d1, d2 = m_mg[0]['DefocusU'], m_mg[0]['DefocusV']
            dangle = m_mg[0]['DefocusAngle']
            voltage = m_mg[0]['Voltage']
            cs = m_mg[0]['SphericalAberration']
            amp_c = m_mg[0]['AmplitudeContrast']
        except:
            has_ctf_info = False
            import sys
            print sys.exc_info()
        finally:
            ctf_img = None
            if self.ctf and has_ctf_info:
                ctf = CTF(voltage=voltage, cs=cs, defocusu=d1,
                        defocusv=d2, defocusangle=dangle,
                        amp_contrast=amp_c)
                ctf_img = ctf.toImage(self.box_size, self.pixel_size, fftshift=True)
                ctf_img *= -1
                #ctf_img.write_image('ctf.mrc')
        mgname = m_mg[0].mg_name
        logger.info("Aligning micrograph: %s" % mgname)
        result = Metadata.new_like(m_mg)
        try:
            mg_size = EMImage.get_image_size(mgname)
            if len(m_mg) == 0: return result
            # group by filaments
            mts = m_mg.group_by('HelicalTubeID')
            # loop all filament
            for mt_id in mts:
                if self.mt_name:
                    tid = int(self.mt_name.split("_")[-1])
                    if mt_id != tid:
                        continue
                boxes = mts[mt_id]

                # remove boxes that is outside micrograph
                bs075 = self.box_size * 0.75 * self.bin_factor
                for i in range(len(boxes) - 1, -1, -1):
                    cx, cy = boxes[i]['CoordinateX'], boxes[i]['CoordinateY']
                    if cx - bs075 < 0 or cx + bs075 > mg_size[0] or cy + bs075 < 0 or cy + bs075 > mg_size[0]:
                        boxes.pop(i)
                if len(boxes) < 2: continue
                smoothed_filament = self.align_filament(boxes, ctf_img=ctf_img)
                if len(smoothed_filament) == 0:
                    continue
                for sf in smoothed_filament:
                    scx, scy = sf.x - sf.shx, sf.y - sf.shy
                    # update other infors from the closed ptcl in original boxes
                    mind = 100000000
                    minp = None
                    for box in boxes:
                        cx, cy = box.x - box.shx, box.y - box.shy
                        dx, dy = scx - cx, scy - cy
                        d2 = dx * dx + dy * dy
                        if d2 <  mind:
                            mind, minp = d2, box
                    if minp is not None:
                        t = minp.copy()
                        t.update(sf)
                        result.append(t)
        except:
<<<<<<< HEAD
=======
            import traceback as tb
            tb.print_exc()
>>>>>>> findkin
            print("Could not align %s\nskip" % mgname)
        return result
    
    def align_filament(self, boxes, ctf_img=None):
        ''' Align single filament. Return aligned filament
        '''
        # basename used to construct other files
        mgname = boxes[0].mg_name
        bname = os.path.basename(os.path.splitext(mgname)[0])
        mt_id = boxes[0].id
        smoothed_star_file = os.path.join(self.star_dir, "%s_%d_smoothed.star" % (bname, mt_id))
        if os.path.exists(smoothed_star_file) and not self.smooth.nuke:
            result = Metadata.readFile(smoothed_star_file)
            if len(result) > 0:
                return result
   
        logger.info("Aligning filament: %s:%d" % (mgname, mt_id))
        b_name = "%s_%d" % (bname, mt_id)
        pixel_size = self.cf.pixel_size
        unbined_box_size = self.box_size * self.bin_factor
        num_pfs = self.num_pfs
        # this re box the filament to make less ptcls
        boxes = self.rebox(boxes)
            
        nboxes=len(boxes)
        x,y,phi, theta, psi=boxes.getValues(['CoordinateX',  'CoordinateY', 'AngleRot', 'AngleTilt', 'AnglePsi'])
        logger.info("Tube id: %s, with %d boxes" % (mt_id, len(boxes)))
        ptcls=[]
        this_align={}       # stores the alignment for different pfs
        smooth_align = {}   # stores the smoothed alignemnt for different pfs. 
        # loop through mt types, do initial reference alignment
        for num_pf in self.projectors:
            projector = self.projectors[num_pf]
            align_par = self.align_filament_with_projector(boxes, projector, num_pf, self.num_starts, ctf_img=ctf_img)
            output_plot_name = os.path.join(self.path("plots"), "%s_pf%d.jpg" % (b_name, num_pf))
            this_align[num_pf] = align_par
            self.smooth.num_pf = num_pf
            self.smooth.num_starts = self.num_starts / 2.
            smoothed_filament = self.smooth.smooth_filament_m(align_par, output_plot=output_plot_name)
            smooth_star_pf = os.path.join(self.star_dir, "%s_pf%d_smooth.star" % (b_name, num_pf))
            smoothed_filament.write(smooth_star_pf)
            smooth_align[num_pf] = smoothed_filament
        
        selected_num_pf, selected_align_par = self.select_num_pf(num_pfs,
                            [this_align[num_pf] for num_pf in num_pfs], 
                            smooth_align)
        ind_star_file = os.path.join(self.star_dir, "%s_pf%d_ind.star" % (b_name, selected_num_pf))
        ind_smooth_star_file = os.path.join(self.star_dir, "%s_pf%d_ind_smooth.star" % (b_name, selected_num_pf))
        ind_star_plot_file = os.path.join(self.plot_dir, "%s_pf%d_ind.jpg" % (b_name, selected_num_pf))
        smooth_plot_name = os.path.join(self.plot_dir, "%s_pf%d_ind_smooth.jpg" % (b_name, selected_num_pf))
        
        if not os.path.exists(ind_star_file) or self.nuke:
#        if 1:
            # do individual alignment on selected_align_par
            #from cryofilia.helix.smoothing.utils import get_values
            # print(get_values(selected_align_par, self.pixel_size * self.bin_factor))
            ind_par = self.smooth.smooth_filament_m(selected_align_par)
            #print(get_values(ind_par, self.pixel_size * self.bin_factor))
#            if 1:
            ind_align = self.ref_align_individual(ind_par, ctf_img=ctf_img)
#                ind_align.write("ind_align.star")
#            else:
#                from cryofilia.align import AlignResult
#                ind_align = AlignResult.readFile('ind_align.star')
            self.update_alignment_with_boxes(ind_align, ind_par)
            ind_par = ind_align
            ind_par.write(ind_star_file)
        else:
            ind_par = RefAlignData.readFile(ind_star_file)
        if not os.path.exists(ind_smooth_star_file) or self.nuke:
#        if not os.path.exists(smooth_plot_name) or self.nuke:
#        if 1:
#            ind_par = RefAlignData.readFile(ind_star_file)
#            print ind_star_file
#            print smooth_plot_name
            smoothed_filament = self.smooth.smooth_filament_m(ind_par, output_plot=smooth_plot_name)
            smoothed_filament.write(ind_smooth_star_file)

        # debug
#        if 1:
#            ind_par = RefAlignData.readFile(ind_star_file)
#            use smooth
#            self.plot_individual_alignment(ind_par, ind_star_plot_file)
        else:
            smoothed_filament = SmoothedCoordinates.readFile(ind_smooth_star_file)
        if not self.dont_write_match:
            #self.write_match_stack(smoothed_filament, self.projectors[selected_num_pf], selected_num_pf, ctf_img=ctf_img)
            self.write_match_stack(ind_par, self.projectors[selected_num_pf], selected_num_pf, ctf_img=ctf_img)
        self.select_link(bname, mt_id, selected_num_pf)
        # update with original infos
        #return selected_align_par
        return smoothed_filament

    def select_link(self, bname, mt_id, num_pf):
        b_name = "%s_%d_pf%d" % (bname, mt_id, num_pf)
        selected_match_ref_file="%s_stack_ref.jpg" % b_name
        selected_match_file = "%s_stack.jpg" % b_name 
        selected_star_file = "%s.star" % b_name
        selected_plot_file = "%s.jpg" % b_name
        selected_individual_file ="%s_ind.star" % b_name
        selected_individual_plot = "%s_ind_smooth.jpg" % b_name
        for fname, src_dir in [
                [selected_match_ref_file, self.match_dir],
                [selected_match_file, self.match_dir],
                [selected_star_file, self.star_dir], 
                [selected_plot_file, self.plot_dir],
                [selected_individual_file, self.star_dir],
                [selected_individual_plot, self.plot_dir]
                ]:
            try:
                os.symlink(os.path.realpath(os.path.join(src_dir, fname)), 
                        os.path.join(self.selected_dir, fname))
            except OSError as e:
                if e.errno != 17: # not "File exists"
                    raise e
                
    
    def plot_individual_alignment(self, m, outfile):
        import matplotlib.pyplot as plt

        cxs, cys, shxs, shys = m.getValues(['CoordinateX', 
            'CoordinateY', 'OriginX', 'OriginY'])
        phi, theta, psi = m.getValues(['AngleRot', 'AngleTilt', 'AnglePsi'])
        cxs = [cxs[i] - shxs[i] for i in range(len(cxs))]
        cys = [cys[i] - shys[i] for i in range(len(cys))]
        coords = np.array(zip(cxs, cys))
        dists = np.linalg.norm(coords[1:] - coords[:-1], axis=1) * self.cf.micrograph_pixel_size
        
        theta = np.array(theta) - 90
        mean_psi = np.mean(psi)
        psi = np.array(psi) - mean_psi

        fig = plt.figure(figsize=(12,6))
        fig.suptitle(outfile)
        dist_ax = fig.add_subplot(121)
        dist_ax.set_ylim([0, 2 * self.helical_rise])
        dist_ax.plot(dists, '+-', label="distances")
        dist_ax.set_xlabel('index')
        dist_ax.set_ylabel("distnace/(A)")
        handles, labels = dist_ax.get_legend_handles_labels()
        dist_ax.legend(handles, labels)
        
        angle_ax = fig.add_subplot(122)
        avg_phi = np.mean(phi)
        ymin, ymax = min(phi), max(phi)
        ymin = min(avg_phi - 20, ymin)
        ymax = max(avg_phi + 20, ymax)
        x1 = range(len(phi))
        angle_ax.set_ylim([ymin, ymax])
        angle_ax.set_ylabel("phi")
        angle_ax.plot(x1, phi, 'r+-', markersize=10, label = "smoothed_phi")
        angle_ax1 = angle_ax.twinx()
        angle_ax1.set_ylabel("dtheta/dpsi")
        angle_ax1.plot(x1, theta,
                'bx-', markersize=10, label = "tilt")
        angle_ax1.plot(x1, psi, 'go-', label = "dpsi")
        angle_ax1.set_ylim(-20, 20)
        handles, labels = angle_ax.get_legend_handles_labels()
        handles1, labels1 = angle_ax1.get_legend_handles_labels()
        handles.extend(handles1)
        labels.extend(labels1)
        angle_ax.legend(handles, labels)
        fig.savefig(outfile)
        plt.close(fig)

    def rebox(self, boxes):
        # Pre treate the boxes, so the boxes are sepeartated by self.nsubunits 
        hr_pix = self.helical_rise / self.cf.micrograph_pixel_size
        step = self.nsubunits * self.helical_rise / self.cf.micrograph_pixel_size
        box0 = boxes[0]
        new_boxes = boxes.new_like(boxes)
        new_boxes.append(box0)
        for i in range(1, len(boxes)):
            cx0, cy0 = box0.x, box0.y
            cx, cy = boxes[i].x, boxes[i].y
            dx, dy = cx - cx0, cy - cy0
            dist = np.sqrt(dx * dx + dy * dy)
            dif = dist - step
            # this means the distance is equal or more than the step
            if (dif > -hr_pix / 2) or (i == len(boxes) - 1):
                box0 = boxes[i]
                new_boxes.append(box0)
        return new_boxes
    def box_filament(self, filament, box_size, prerotate=None, recenter=True):
        ''' Box a filament particles.
        Currently the coordinates are rounded to floor.
        :param prerotate: If prerotate the ptcl, the prerotate should be the list of rotation angles, otherwise None
        :param recenter: Default to true. If don't want to recenter, pass False, in which the originX/Y would not be used
            Caller should reset the metadata shift.

        '''
        mgname = filament[0].mg_name
        mg = EMImage(mgname)
        cxs, cys, shxs, shys = filament.getValues(['rlnCoordinateX',
            'rlnCoordinateY', 'rlnOriginX', 'rlnOriginY'])
        if recenter:
            cxs = [cx - shx for cx, shx in zip(cxs, shxs)]
            cys = [cy - shy for cy, shy in zip(cys, shys)]
        cxs = [int(cx) for cx in cxs]
        cys = [int(cy) for cy in cys]
        ptcls = mg.box_particles(zip(cxs, cys), box_size=box_size, prerotate=prerotate)
        return ptcls

    def radon_align(self, boxes, ptcls=None):
        ''' Radon align boxes. inplace update boxes
        :param boxes: Metdata of the filament
        :param ptcls: The list of particles, in bin 1. 
        '''
        from cryofilia.helix.radon import inplane_align
        logger.info("Radon aligning")
        if all([b.radon_done for b in boxes]):
            return
        if ptcls is None:
            ptcls=[]
        unbined_box_size=self.box_size * self.bin_factor
        if len(ptcls) != len(boxes):
            del ptcls[:]
            ptcls.extend(self.box_filament(boxes, unbined_box_size))
        coords = np.array(zip([_.x for _ in boxes], [_.y for _ in boxes]))
        vs = coords[1:] - coords[:-1]
        orig_psi = -np.arctan2(vs[:, 1], vs[:,0]) * 180 / np.pi
        orig_psi = orig_psi.tolist()
        orig_psi.insert(0, orig_psi[0])
        
        #orig_psi=[box.psi for box in boxes]
        est_angles, repeat_dist, repeat_dists = self.radon.align_stack(ptcls, return_repeat_distance=True, ori_psis=orig_psi)
        logger.info("radon align: inplance rotations: \n%s" % str(est_angles))
        # smooth the angle
        for i in range(len(boxes)):
            boxes[i].psi=est_angles[i]
            boxes[i].radon_done=True

    def align_filament_with_projector(self, boxes, projector, num_pf, num_start, ctf_img=None):
        # align_par is the parameter for this filament after alignment
        logger.info("Protofilament number: %d" % num_pf)
        if len(boxes) < 2:
            logger.warn("number particles to less in boxes, at least 2 boxes needed")
            return None
        align_par = None
        star_file = None
        mgname = boxes[0].mg_name
        bname = os.path.basename(os.path.splitext(mgname)[0])
        mt_id = boxes[0].id
        mt_name = "%s_%d" % (bname, mt_id)
        ptcls = None
        # check if the alignment is done
        if self.star_dir is not None:
            star_file = os.path.join(self.star_dir, "%s_pf%d.star"
                    % (mt_name, num_pf))
            if os.path.exists(star_file) and not self.nuke:
                align_par = RefAlignData.readFile(star_file)
                if len(align_par) != len(boxes):
                    # this mt is not done yet. redo it
                    align_par = None
        if align_par is None:
            # radon align
            self.radon_align(boxes)
            orig_psi = boxes.getValues('rlnAnglePsi')
            ptcls = ptcls or  self.box_filament_and_preprocess(boxes)
            # initial align to find seam 
            align_par = self.ref_align_mt_single(ptcls,
                    num_pfs=num_pf,
                    projector=projector, mt_name=mt_name,
                    ctf_img=ctf_img)
            self.update_alignment_with_boxes(align_par, boxes)
            for ptcl in align_par:
                ptcl.pfs = num_pf
                ptcl.starts = num_start

            if star_file is not None:
                align_par.write(star_file)
        return align_par
    def update_alignment_with_boxes(self, align_par, boxes):
        '''
        # update the shifts to original value. 
        # The shift calculate from the alignment is how much the ptcl 
        # should shift to match the reference. # The inverse...
        # to convert the shift to original shift, rotate the shift vector by angle -box['rlnAnglePsi'], and take neg for relion convention
        '''
        align_par.addLabels(boxes.getActiveLabels())
        for i in range(len(boxes)):
            ox = align_par[i]['rlnOriginX']
            oy = align_par[i]['rlnOriginY']
            box = boxes[i]
            tmp_psi = align_par[i]['rlnAnglePsi']
            ori_psi = box['rlnAnglePsi']
            box = box.copy()
            # only update the phi, theta and psi
            box.phi, box.theta, box.psi, box.phi0 = align_par[i].phi, align_par[i].theta, align_par[i].psi, align_par[i].phi0
            try:
                box.selected = align_par[i].selected
            except:
                pass
            box.ccc = align_par[i].ccc
            box.psi = ori_psi + tmp_psi
            align_par[i] = box
            align_par[i]['rlnAnglePsi'] = ori_psi + tmp_psi
            cs = np.cos(-ori_psi * np.pi / 180.)
            ss = np.sin(-ori_psi * np.pi / 180.)
            align_par[i]['rlnOriginX'] = (cs * ox - ss * oy) * self.bin_factor
            align_par[i]['rlnOriginY'] = (ss * ox + cs * oy) * self.bin_factor

    def write_match_stack(self, align_par, projector, num_pf, ctf_img=None):
        ''' Write matched experimental image and reference image
        '''
        if len(align_par) == 0:
            self.warn("No smoothed coord output")
            return
        mt_id=align_par[0].id
        mgname=align_par[0].mg_name
        bname=os.path.basename(os.path.splitext(mgname)[0])
        stack_image=os.path.join(self.match_dir,
            "%s_%d_pf%d_stack.jpg" % (bname, mt_id, num_pf))
        stack_ref_image = os.path.join(self.match_dir, 
                            "%s_%d_pf%d_stack_ref.jpg" % (bname, mt_id, num_pf))
        if (os.path.exists(stack_image) 
                and os.path.exists(stack_ref_image) 
                and not self.nuke):
            return
        logger.debug("Writing match images")
        # calculate the helical twist and helical rise, for stacking
        if self.stack_box_size is None:
            self.stack_box_size = self.box_size
        
        xs, ys, shxs, shys, psis=align_par.getValues(['CoordinateX', 'CoordinateY', 'OriginX', 'OriginY', 'AnglePsi'])
        xs=[xs[i] - shxs[i] for i in range(len(xs))]
        ys=[ys[i] - shys[i] for i in range(len(ys))]
        coords=np.array([xs,ys]).T
        d1=coords[1:,:] - coords[:-1, :]
        dists=np.linalg.norm(d1,axis=1)
        # get the distances is not far away.
        dists1=dists[abs(dists * self.cf.pixel_size - self.cf.helical_repeat_distance) < self.cf.helical_repeat_distance/2]
        if len(dists1) > 0:
            helical_rise_pix=np.median(dists1)
        else:
            logger.warning("Could not determin the helical rise. skip g")
            return
        # get the centered ptcls stack:
        # the order of ptcls might be inversed, so rebox the particles from original image
        ptcls_stack=[]
        # make vertical
        ptcls_stack=self.box_filament(align_par, self.stack_box_size * self.bin_factor,  prerotate=[psi + 90 for psi in psis])
        for ptcl in ptcls_stack:
            ptcl.ramp()
            ptcl.resample(new_size=self.stack_box_size)
            ptcl.inverse()
        stack_i = self.stack_image_helper(helical_rise_pix / self.bin_factor, self.stack_box_size, ptcls_stack, group=2)
#        stack_i.resample(scale=2)
        stack_i.write_image(stack_image)
        phis, thetas=align_par.getValues(['AngleRot', 'AngleTilt'])
        # project vertical
        old_res = projector.project_resolution
        projector.project_resolution = 20
        projections=[projector.project(phi, theta, -90)
               for phi, theta in zip(phis, thetas)]
        if ctf_img is not None:
            for proj in projections:
                proj.fftdata *= ctf_img.data.astype(proj.dtype)
        projector.project_resolution = old_res
        stack_r=self.stack_image_helper(helical_rise_pix / self.bin_factor, self.stack_box_size, projections, group=2)
#        stack_r.resample(scale=2)
        stack_r.write_image(stack_ref_image)

    def select_num_pf(self, num_pfs, metadatas, smoothed_metadatas=None, 
            use_selected=False):
        '''
            select seam according to ccc
        '''
        assert(len(num_pfs) == len(metadatas))
        assert(num_pfs > 0)
        if len(num_pfs) == 1:
            return num_pfs[0], metadatas[0]
        # calculate median cccs
        def get_mean_ccc(filament):
            if use_selected:
                try:
                    cccs = [f.ccc for f in filament if f.selected]
                except:
                    cccs = [f.ccc for f in filament]
            else:
                cccs = [f.ccc for f in filament]
            if len(cccs) == 0: return 0
            return np.mean(cccs)
        tmp=[]
        # get all filaments
        for num_pf, m in zip(num_pfs, metadatas):
            cccs=get_mean_ccc(m)
            tmp.append([cccs, num_pf, m])
        tmp.sort(key=lambda x : x[0], reverse=True)
        max_ccc, max_pf, max_md=tmp[0]
        second_ccc, second_pf, second_md=tmp[1]

        logger.info("max ccc: %f, pf: %d, second ccc: %f, second pf: %d" 
                     % (max_ccc, max_pf, second_ccc, second_pf))
        return max_pf, max_md
    
    def ref_align_0(self, ptcls, ga, ar, num_pfs, debug_single):

        for i, ptcl in enumerate(ptcls):
#            if debug_single:
#                logger.info("Coarse Align 0")
            r = ga.align(ptcl, 0, 
                    90, 1, 1,  # theta
                    0, int(360. / num_pfs / self.coarse_dphi), self.coarse_dphi, # phi
                    ccmapMask=self.ccmap_mask
                    )
            s = "%7.4f %7.4f %7.4f %7.4f %7.4f %6.4f" % (r.shx, r.shy, r.phi, r.theta, r.psi, r.ccc)
            if debug_single:
                logger.info("%3d %s", i, s)
            
#                logger.info("Fine Align 0")
            r = ga.align(ptcl, 0,
                    r.theta, 1, 1,
                    r.phi, int(self.coarse_dphi * 2 + 1), 1,
                    ccmapMask=self.ccmap_mask)
            best0 = r
            best = best0
            if debug_single:
                s = "%7.4f %7.4f %7.4f %7.4f %7.4f %6.4f" % (r.shx, r.shy, r.phi, r.theta, r.psi, r.ccc)
                logger.info("%3d %s", i, s)

#                logger.info("Coarse Align 180")
            r = ga.align(ptcl, 180,
                    90, 1, 1,  # theta
                    0, int(360. / num_pfs / self.coarse_dphi), self.coarse_dphi, # phi
                    ccmapMask=self.ccmap_mask
                )
            if r.ccc > best.ccc: best = r
            if debug_single:
                s = "%7.4f %7.4f %7.4f %7.4f %7.4f %6.4f" % (r.shx, r.shy, r.phi, r.theta, r.psi, r.ccc)
                logger.info("%3d %s", i, s)
#                logger.info("Fine Align 180")
            r = ga.align(ptcl, 180, 
                    #best.theta, 1, 1,
                    #best.phi, int(self.coarse_dphi * 2 + 1), 1,
                    r.theta, 1,1,
                    r.phi, int(self.coarse_dphi * 2 + 1), 1,
                    ccmapMask=self.ccmap_mask)
            best180 = r
            best = best0 if best0.ccc > best180.ccc else best180
            if debug_single:
                s = "%7.4f %7.4f %7.4f %7.4f %7.4f %6.4f" % (r.shx, r.shy, r.phi, r.theta, r.psi, r.ccc)
                logger.info("%3d %s", i, s)
#                logger.info("Seam Search 0")
            r = ga.align(ptcl, 0, 
                    90, int((self.theta_range - 1) / self.coarse_dtheta) + 1, self.coarse_dtheta,
                    best0.phi, num_pfs, 360./num_pfs,
                    ccmapMask=self.ccmap_mask)
            if r.ccc > best.ccc: best = r
            if debug_single:
                s = "%7.4f %7.4f %7.4f %7.4f %7.4f %6.4f" % (r.shx, r.shy, r.phi, r.theta, r.psi, r.ccc)
                logger.info("%3d %s", i, s)
#                logger.info("Seam Search 180")
            r = ga.align(ptcl, 180, 
                    90, int((self.theta_range - 1) / self.coarse_dtheta) + 1, self.coarse_dtheta,
                    best180.phi, num_pfs, 360./num_pfs,
                    ccmapMask=self.ccmap_mask)
            if r.ccc > best.ccc: best = r
            if debug_single:
                s = "%7.4f %7.4f %7.4f %7.4f %7.4f %6.4f" % (r.shx, r.shy, r.phi, r.theta, r.psi, r.ccc)
                logger.info("%3d %s", i, s)
#                logger.info("Coarse Align Found seam")
            r = ga.align(ptcl, best.psi,
                    90, int((self.theta_range - 1) / self.coarse_dtheta) + 1, self.coarse_dtheta,
                    best.phi, int(min(30., 360./num_pfs) / self.coarse_dphi), self.coarse_dphi,
                    ccmapMask=self.ccmap_mask)
            if debug_single:
                s = "%7.4f %7.4f %7.4f %7.4f %7.4f %6.4f" % (r.shx, r.shy, r.phi, r.theta, r.psi, r.ccc)
                logger.info("%3d %s", i, s)
#                logger.info("Fine Align Found seam")
            r = ga.align(ptcl, best.psi,
                    best.theta, int(self.coarse_dtheta * 2 + 1), 1,
                    best.phi, int(self.coarse_dphi * 2 + 1), 1,
                    ccmapMask=self.ccmap_mask)
            s = "%7.4f %7.4f %7.4f %7.4f %7.4f %6.4f" % (r.shx, r.shy, r.phi, r.theta, r.psi, r.ccc)
            logger.info("%3d %s", i, s)
<<<<<<< HEAD
            logger.info("")
=======
>>>>>>> findkin
            best = r
            ar[i].update(r)

    def local_align(self, ptcl, ga, r, num_phis=None):
        '''
        '''
        num_phis = num_phis or int(self.coarse_dphi * 4 + 1)
        r1 = ga.align(ptcl, 0,
                r.theta, int(self.coarse_dtheta * 2 + 1), 1,
                r.phi, num_phis, 1,
                ccmapMask=self.ccmap_mask)
        r.update(r1)

    def ref_align_2(self, ptcls, ga, ar, num_pfs, debug_single):
        ''' Indivdual alignment. Start with best_phi, theta=90, best_psi

        '''
        for i, ptcl in enumerate(ptcls):
            r = ar[i]
            if debug_single:
                logger.info("Coarse Align Selected seam")
            self.local_align(ptcl, ga, r, num_phis=30)
            s = "%7.4f %7.4f %7.4f %7.4f %7.4f %6.4f" % (r.shx, r.shy, r.phi, r.theta, r.psi, r.ccc)
            logger.info("%3d %s", i, s)

    def ref_align_1(self, ptcls, ga, ar, num_pfs, debug_single):
        # do final = 1
        # refine in selected seam
        for i, ptcl in enumerate(ptcls):
            r = ar[i]
            if debug_single:
                logger.info("Coarse Align Selected seam")
            r = ga.align(ptcl, r.psi,
                    90, int((self.theta_range - 1) / self.coarse_dtheta) + 1, self.coarse_dtheta,
                    r.phi, int(min(30., 360./num_pfs) / self.coarse_dphi), self.coarse_dphi,
                    ccmapMask=self.ccmap_mask)
            s = "%7.4f %7.4f %7.4f %7.4f %7.4f %6.4f" % (r.shx, r.shy, r.phi, r.theta, r.psi, r.ccc)
            if debug_single:
                logger.info("%3d %s", i, s)
                logger.info("Fine Align Selected seam")
            r = ga.align(ptcl, r.psi,
                    r.theta, int(self.coarse_dtheta * 2 + 1), 1,
                    r.phi, int(self.coarse_dphi * 2 + 1), 1,
                    ccmapMask=self.ccmap_mask)
            s = "%7.4f %7.4f %7.4f %7.4f %7.4f %6.4f" % (r.shx, r.shy, r.phi, r.theta, r.psi, r.ccc)
            logger.info("%3d %s", i, s)
            ar[i].update(r)
    def box_filament_and_preprocess(self, boxes):
        ''' box filament and preprocess for reference alignment
        '''
        prerotate = boxes.getValues("AnglePsi")
        unbined_box_size=int(self.box_size * self.bin_factor)
        ptcls = self.box_filament(boxes, unbined_box_size, prerotate=prerotate)
        for i, ptcl in enumerate(ptcls):
            ptcl.ramp()
            ptcl.resample(new_size=self.box_size)
            ptcl.inverse()
            ptcl.normalize()
            ptcl.mult(self.edge_mask)
        return ptcls

    def ref_align_individual(self, boxes, debug_single=False, ctf_img=None):
        ''' Refalign individual repeat, which need to reextract the ptcls
            
            :param boxes: re-spaced boxes.
        '''
        from cryofilia.align import GridAligner
<<<<<<< HEAD
=======

        logger.info("Final fine alignment for all particles")
>>>>>>> findkin
        ptcls = self.box_filament_and_preprocess(boxes)
        num_pfs = boxes[0].pfs
        ga = GridAligner(self.projectors[num_pfs], debug=debug_single, ctf_img=ctf_img)
        # do a ref_align_1 currently. More precise later
        from cryofilia.align import AlignResult

        ar = AlignResult()
        for boxi in boxes:
            obj = ar.newObject()
            obj.phi, obj.theta, obj.psi = boxi.phi, boxi.theta, boxi.psi
            ar.append(obj)
        self.ref_align_2(ptcls, ga, ar, num_pfs, debug_single)
        return ar

    def update_param_with_alignment(self, m, ar):
        ''' Update m with align result
        '''
        assert(len(m) == len(ar))
        for i in range(len(m)):
            m0, ar0 = m[i], ar[i]
            m0.phi, m0.theta, m0.psi, m0.shx, m0.shy, m0.ccc = (
                    ar0.phi, ar0,theta, ar0.psi, ar0.shx, ar0.shy, ar0.ccc)
            m0.phi0, m0.pfs, m0.selected = ar0.phi0, ar0.pfs, ar0.selected
    def update_alignment_with_seam(self, ar, seam):
        for i in range(len(ar)):
            ar0, s0 = ar[i], seam[i]
            ar0.phi, ar0.theta, ar0.psi, ar0.selected = s0.seam_phi, s0.proto_theta, s0.seam_psi, s0.selected

    def ref_align_mt_single(self, ptcls, num_pfs=None, projector=None, origin_psi=None, mt_name="", ctf_img=None):
        ''' Version 1 for ref align
        :return: AlignResult instance. This only contains alignment information.
        '''
        from cryofilia.align import GridAligner, AlignResult
        ga = GridAligner(projector, project_mask = self.project_mask, debug=False, ctf_img=ctf_img)
        ar = AlignResult()
        for i in range(len(ptcls)):
            a = ar.newObject()
            ar.append(a)

        debug_single = self.debug and True
        self.ref_align_0(ptcls, ga, ar, num_pfs, debug_single)
        # save phi before select seam
        for i in range(len(ptcls)):
            ar[i].phi0 = ar[i].phi
        import matplotlib.pyplot as plt
        plt.cla()
        plt.plot([r.phi for r in ar], '+-', label="phi")
        plt.plot([r.theta for r in ar], 'o-', label="theta")
        plt.plot([r.psi for r in ar], '*-', label="psi")
        # guess seam
        seam = self.guess_seam(ar, num_pfs)
        for r in seam:
            logger.info(str(r))
        plt.plot([s.seam_phi for s in seam], 'x', label='guess seam')
        plt.legend(framealpha=0.7, loc=1)
        plt.savefig(os.path.join(self.plot_dir, "%s_pf%d_ref_align.jpg" % (mt_name, num_pfs)))

        # update result with seam
        self.update_alignment_with_seam(ar, seam)
        self.ref_align_1(ptcls, ga, ar, num_pfs, debug_single)
        seam = self.guess_seam(ar, num_pfs, phi_tolerance=10)
        self.update_alignment_with_seam(ar, seam)
        for r in seam:
            logger.info(str(r))
        return ar
            
    def guess_seam(self, noseam_result, num_pf, phi_tolerance=360):
        ''' Guess the seam and return result
        ''' 
        from .seam import FindSeam
        find_seam = FindSeam(self.num_starts, num_pf, self.cf.helical_repeat_distance, pixel_size = self.pixel_size, phi_tolerance=phi_tolerance, num_repeat_per_box=self.nsubunits)

        return find_seam.guess_seam(noseam_result)

    @staticmethod
    def cosmask(box_size, radius, edge_width, direction=0.):
        ''' Cosine mask along direction. 
        Direction is not used now. Defaut is x.
        :param radius: The radius from center that the value do not change
        :param edge_width: The falloff of the edge from 1 to 0, in half period.
        :return: EMImage cosine mask.
        '''

        ct = box_size / 2
        r = radius
        w = edge_width
        if r <= 1: r *= box_size
        if w <= 1: w *= box_size
        r, w = int(r), int(w)
        ma=np.abs(np.arange(box_size) - ct) - r
        ma[ma < 0]=0
        ma[ma > w] = w
        ma=np.pi / w * ma
        ma=np.cos(ma) / 2 + 0.5
        ma=np.tile(ma, (box_size, 1))
        return EMImage(ma)
    
    @property
    def x_range(self):
        if self._x_range is None:
            x_range=int(self.cf.helical_repeat_distance * 1.2 / self.pixel_size) / 2 * 2
            self._x_range=[-x_range / 2, x_range/2]
        return self._x_range

    @property
    def edge_mask(self):
        if not self._ptcl_edge_mask:
            self._ptcl_edge_mask = RefAlign.cosmask(self.box_size, 0.4, 0.15)
        return self._ptcl_edge_mask 
    @property
    def project_mask(self):
        if not self._project_mask:
            self._project_mask = RefAlign.cosmask(self.box_size, 0.3, 0.2)
        return self._project_mask
    @property
    def ccmap_mask(self):
        if not self._ccmask:
            n = self.box_size
            self._ccmask = EMImage(n,n)
            self._ccmask.data[:, n/2 + self.x_range[0]:n/2+self.x_range[1]]=1
        return self._ccmask
        
    def fix_angle(self, angle, angle_inc, num_pfs=None):
        '''
            use fix_rollover to get best fit of angles
            :param angle: The angle list to fix
            :param angle_inc: The expected angle increasement. 
                For phi, this is the twist. For psi, should be 0
            :param num_pfs: The number of protofilament to search. 
            @return: fixed angle and the best protofilament number.
        '''    
        angle=np.array(angle)
        angle_model=np.array(fix_rollover(angle 
                            + angle_inc / 2, 0, angle_inc)[0]) - angle_inc / 2

        if num_pfs is None:
            num_pfs=int(np.ceil(360 / angle_inc))
        min_err=360 * len(angle)
        best_pf=-1
        for i in range(num_pfs):
            err=(angle_model + angle_inc * i - angle + 180) % 360 - 180
            err=np.sum(abs(err)) 
            if err < min_err:
                min_err=err
                best_pf=i
        return best_pf, (angle_model + angle_inc * best_pf + 180) % 360 - 180

    def find_seam(self, phi, twist_per_pf):
        '''
            Given list phi, and twist per protofilament, return the corrected phi in one seam
            find_seam usually only works if there is decorations
        '''
        phi=np.array(phi)
        # first try to bring to the range of 360 / num_pfs
        phi1, _=fix_rollover(phi, 0, twist_per_pf)
        
        # then find the super twist
        # diff between each two is not good to find the twist
        # for instance if the samping angle is 1 degree, 
        # then most of the diff would be 0
        # use the average of half median
        dphi1=[phi1[i] - phi1[i-1] for i in range(1, len(phi1))]
        dphi1=[(dp + 180) % 360 - 180 for dp in dphi1]
        
        # find median
        dphi1.sort()
        qt=len(dphi1) / 4
        mdphi1=np.average(dphi1[qt:len(dphi1)-qt])
        
        phi_model, _=fix_rollover(phi, mdphi1, twist_per_pf)
        errs=[]
        for i in range(int(np.ceil(360 / twist_per_pf))):
            err=(phi_model + twist_per_pf * i - phi + 180) % 360 - 180
            err=np.sum(abs(err)) 
            errs.append([err, i])
        errs.sort(key=lambda x: x[0])
        phi1=(phi_model + twist_per_pf * errs[0][1] + 180) % 360 - 180 
        return phi1 
    

    def stack_image_helper(self, helical_rise, box_size, ptcls, group=1):
        ''' 
            insert one subunit in group number of particle averged mt to new image to reconstruct a compressed mt
            :param helical_rise: number of pixels for each subunit
            :param group: The number of subunits to average for each subunit
            :param box_size: The width of the box output
        '''
        n=len(ptcls)
        n1=n / group
        if n1 == 0: n1=1
        box_len=int(helical_rise * n1) # helical_rise could be real number
        ptcl_stack=EMImage(box_size, box_len)
        # average the group
        new_ptcls=[]
        for i in range(n1):
            new_ptcl = EMImage(ptcls[0].size, dtype=np.float64)
            for _pt in ptcls[i*group:(i+1)*group]:
                new_ptcl += _pt
            new_ptcls.append(new_ptcl)
        iii = 1

        tt=[]
        for i in range(n1):
            center=[box_size / 2, int(helical_rise / 2 + i * helical_rise)]
            to_insert=new_ptcls[i].clip([box_size, int(helical_rise) + 1])
            tt.append(to_insert)
            ptcl_stack.insert_image(to_insert, position=center)
        
        return ptcl_stack
    def init_smooth_coords(self):

        self.smooth = SmoothCoordsMT()
        self.smooth.workdir = self.workdir
        self.smooth.data_redundancy = 3
        self.smooth.max_n_outliers = 2
        self.smooth.fit_order = 1
        self.smooth.micrograph_pixel_size = self.cf.micrograph_pixel_size
        self.smooth.win_size = 7
        self.smooth.subunits_per_repeat = 2
<<<<<<< HEAD
        self.smooth.twist_tolerance = 5
        self.smooth.direction_error_tolerance = 5
        self.smooth.dist_tolerance = 5 
        self.smooth.psi_error_tolerance = 1
        self.smooth.theta_error_tolerance = 1
        self.smooth.phi_error_tolerance = 1
        self.smooth.angle_tolerance = 1
=======
        self.smooth.twist_tolerance = 10
        self.smooth.direction_error_tolerance = 10
        self.smooth.dist_tolerance = 5 
        self.smooth.psi_error_tolerance = 3
        self.smooth.theta_error_tolerance = 3 
        self.smooth.phi_error_tolerance = 3
        self.smooth.angle_tolerance = 3
>>>>>>> findkin
        self.smooth.micrograph_pixel_size = self.cf.pixel_size
        self.smooth.min_segs = 3
        self.smooth.output_plot_dir = self.plot_dir
        self.smooth.helical_rise = self.helical_rise
        self.smooth.helical_twist = self.helical_twist
        self.smooth.nuke = True
        logger.debug("=========================================")
        logger.debug("Smooth Parameter")
        logger.debug(str(self.smooth))
        logger.debug("=========================================")
    
    def init_radon(self):
        from cryofilia.helix.radon import Radon
        self.radon = Radon()
        self.radon.pixel_size = self.cf.pixel_size
        self.radon.pixel_size_radon = self.pixel_size * self.bin_factor / 2
        self.radon.layer_line_resolution=40
        self.radon.search_halfw_width=15
    
    @property
    def match_dir(self):
        return self.path("matches")
    @property
    def plot_dir(self):
        return self.path("plots")

    def start(self, *args, **kwargs):
        super(RefAlign, self).start(output=self.path("output.txt"), *args, **kwargs)

class AlignResult(list):
    def __init__(self, *args):
        self.extend([0,0,0,0,0,0,0])
        try:
            self[0] = args[0]
            self[1] = args[1]
            self[2] = args[2]
            self[3] = args[3]
            self[4] = args[4]
            self[5] = args[5]
    
        except IndexError:
            pass
    
    @property
    def shx(self):
        return self[0]
    
    @shx.setter
    def shx(self, value):
        self[0] = value

    @property
    def shy(self):
        return self[1]
    
    @shy.setter
    def shy(self, value):
        self[1] = value

    @property
    def phi(self):
        return self[2]
    
    @phi.setter
    def phi(self, value):
        self[2] = value

    @property
    def theta(self):
        return self[3]
    
    @theta.setter
    def theta(self, value):
        self[3] = value

    @property
    def psi(self):
        return self[4]
    
    @psi.setter
    def psi(self, value):
        self[4] = value

    @property
    def ccc(self):
        return self[5]
    
    @ccc.setter
    def ccc(self, value):
        self[5] = value


    def __str__(self):
        return "%6.2f %6.2f %6.2f %6.2f %6.2f %6.4f" % (self.shx, self.shy, self.phi, self.theta, self.psi, self.ccc)


class RefAlignError(RuntimeError):
    pass

class RefAlignPlot(object):
    def __init__(self, filename):
        self.filename = filename
        
    def plot(self, *args, **kwargs):

        pass

class SmoothCoordsMT(SmoothCoords):
    pass
#     def fixup_align_params(self, coords, phi, theta, psi, repeat_distance=None):
#         repeat_distance = repeat_distance or self.helical_rise
#         coords1, phi1, theta1, psi1 = super(SmoothCoordsMT, self).fixup_align_params(coords, phi, theta, psi, subunits_per_repeat=1)
#         return super(SmoothCoordsMT, self).fixup_align_params(coords1, 
#                 phi1, theta1, psi1)
#     
    
