''' Image filter
'''
import numpy as np
from cryofilia.EMImage import EMImage
from cryofilia.testImage import TestImage

N1 = None
passband1, stopband1 = None, None
filt1 = None

class FourierFilter(object):
   
    @staticmethod
    def butterworth(passband, stopband, N=0, image=None):
        global N1, filt1, passband1, stopband1
        if image is not None:
            N = image.size
        N = list(N)
        if N == N1 and passband1 == passband and stopband1 == stopband:
            filt = filt1
        else:
            eps = 0.882
            aa = 10.624
            order = 2 * np.log10(eps / np.sqrt(aa * aa - 1)) 
            order /= np.log10(passband / stopband)
            rad = passband / (eps ** (2/order))
            filt = TestImage.fourierRadius(N, dtype=np.float64).data
            filt = np.sqrt(1.0 / (1.0 + (filt / rad) ** order))
            # update global
            N1 = N
            filt1 = filt
            passband1= passband
            stopband1=stopband
        if image is not None:
            image.fftdata *= filt
        return EMImage(filt)
