''' TestImage class.

Create a test image, or module image for image processing
'''

import numpy as np

from cryofilia.EMImage import EMImage

class TestImage(object):
    ''' generate synthetic images using pattern

    '''
    @staticmethod
    def cylinder(dims, radius=None, radius_2=None, axis='z'):
        ''' Generate a cylinder volume, with dims.'''
        try:
            _ = dims[0]
        except TypeError:
            dims = [dims, dims, dims]
        try:
            _ = dims[2]
        except IndexError:
            dims = [dims[0], dims[0], dims[0]]
        box_size = dims[0]
        img = EMImage(dims)
        if radius is None:
            radius = box_size / 2
        if radius_2 is None:
            radius_2 = 0
        bs2 = box_size / 2
        idx_x, idx_y = np.mgrid[-bs2:bs2, -bs2:bs2]
        idx_r = idx_x*idx_x + idx_y*idx_y
        radius2 = radius * radius
        r22 = radius_2 * radius_2
        idx_c = 1
        if axis == 'z':
            img.data[:, np.logical_and(idx_r <= radius2, idx_r >= r22)] = idx_c
        elif axis == 'x' or axis == 'y':
            img.data[np.logical_and(idx_r <= radius2, idx_r >= r22), :] = idx_c
            if axis == 'y':
                img.data = img.data.transpose((0, 2, 1))
        return img
    
    @staticmethod
    def sphere(box_size, radius=None):
        ''' Generate a sphere volume
        '''
        if radius is None:
            radius = box_size / 2
        bs2 = box_size / 2
        idx_z, idx_y, idx_x = np.mgrid[-bs2:bs2, -bs2:bs2, -bs2:bs2]
        idx_r = idx_x*idx_x + idx_y*idx_y + idx_z*idx_z
        radius2 = radius * radius
        idx_r[idx_r < radius2] = 1
        idx_r[idx_r >= radius2] = 0
        return EMImage(idx_r)
    
    @staticmethod
    def circle(box_size, radius=None, inner_radius = None):
        ''' Generate a circle in 2D
        '''
        if radius is None:
            radius = box_size / 2
        if inner_radius is None:
            inner_radius = 0
        bs = box_size
        idx_y, idx_x = np.mgrid[-(bs/2):(bs + 1)/2,
                -(bs/2):(bs+1)/2]
        idx_r = idx_x*idx_x + idx_y*idx_y
        radius2 = radius * radius
        iradius2 = inner_radius * inner_radius
        r = np.zeros([box_size, box_size])
        r[idx_r < radius2] = 1
        r[idx_r < iradius2] = 0
        return EMImage(r)

    @staticmethod
    def gaussian(box_size, radius = None, width = 0):
        try:
            _ = box_size[0]
        except TypeError:
            box_size = [box_size]
        if radius is None:
            radius = box_size[0] / 2 - width
        r = TestImage._radius(box_size, dtype = np.float64)
        arr = r.copy()
        if radius < 0:
            arr = -radius - arr
            if width > -radius: width = -radius
            arr.data[arr.data<0] = 0
        else:
            arr = arr - radius
            arr.data[arr.data<0] = 0
        weight = 1/float(width)
        arr = np.exp(-0.5 * arr.data * arr.data * weight)
        if radius > 0:
            arr[r.data>(radius + width)] = 0
        else:
            arr[r.data < (-radius - width)] = 0
        return EMImage(arr)

    @staticmethod
    def index(box_size, dtype = np.int32, center = None):
        return TestImage._index(box_size, dtype, center)

    @staticmethod
    def _index(box_size, dtype = np.int32, center = None):
        if isinstance(box_size, int):
            box_size = [box_size]
        if center is None: center = [b / 2 for b in box_size]
        if len(box_size) == 1:
            arr = np.mgrid[0:box_size[0]] - center[0]
        elif len(box_size) == 2:
            arr = np.mgrid[0:box_size[0], 0:box_size[1]]
            arr[0] -= center[0]
            arr[1] -= center[1]
        elif len(box_size) == 3:
            arr = np.mgrid[0:box_size[0], 0:box_size[1], 0:box_size[2]]
            arr[0] -= center[0]
            arr[1] -= center[1]
            arr[2] -= center[2]
        return arr

    @staticmethod
    def radius(box_size, dtype = np.float32):
        return TestImage._radius(box_size, dtype)

    @staticmethod
    def _radius(box_size, dtype = np.int32):
        ''' Return a radius array with center at box_size/2
        @param box_size: The box size array.
        '''
        arr = TestImage._index(box_size, dtype)
        arr = np.sqrt(np.sum([x*x for x in arr], axis = 0))
        if dtype == np.float64:
            return EMImage(arr)
        else:
            return EMImage(arr.astype(np.float32))

    @staticmethod
    def fourierRadius(dims, dtype=np.float32, rfft=False, fourierShift=False):
        ''' Create fourier radius for fourier transform
        :param dims: The dimensions. Use int or int list
        '''
        try:
            _ = dims[0]
        except TypeError:
            dims = [dims]
        # inverse dims for numpy
        dims.reverse()
        radius = np.zeros(dims, dtype=dtype)
        for i in range(len(dims)):
            dim = dims[i]
            dimi = [1] * len(dims)
            dimi[i] = dim
            center = dim / 2
            idxes = np.mod((np.arange(dim) + center), dim) - center
            idxes = idxes.astype(dtype)
            idxes /= dim
            idxes2 = idxes * idxes
            idxes2 = idxes2.reshape(dimi)
            radius += idxes2
        return EMImage(np.sqrt(radius))

if __name__ == "__main__":
    pass
