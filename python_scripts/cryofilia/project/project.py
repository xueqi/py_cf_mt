'''
Project is a project that contains all informations for one project.
Each project should have same micrograph and camera information.
Multiple datasets with different conditions should treat as different project.

CFProject class is a list style Metadata

'''
import os
from .parameter import Parameter

class CFProjectParameter(Parameter):
    """ The CFProject class is global structure for a project
    """
    def __init__(self):
<<<<<<< HEAD
        super(CFProjectParameter, self).__init__()

    def add_arguments(self):
        self.add_argument('filament_outer_radius', type=float, default=200,
                help=" Outer filament radius (Angstroms); needed for volume")
        self.add_argument('helical_repeat_distance', type=float, default=83.516,
                help="Helical repeat distance in Angstroms")
        self.add_argument('num_repeat_residues', type=int, default=1229,
                help="Number of residues in one asymmetric unit")
=======
        super(CFProjectParameter, self).__init__(no_workdir=True)

    def add_arguments(self):
        self.add_argument('filament_outer_radius', type=float, default=200,
                help=("Outer filament radius (Angstroms); "
                    "needed for making default volume"))
        self.add_argument('helical_repeat_distance', type=float, default=83.516,
                help="Helical repeat distance in Angstroms")
#        self.add_argument('num_repeat_residues', type=int, default=1229,
#                help="Number of residues in one asymmetric unit")
>>>>>>> findkin
        self.add_argument('target_magnification', type=float,
                help="the magnification on micrograph")

        self.add_argument('scanner_pixel_size', type=float,
                help="Pixel size of detector or CCD (micrometers)\n6.35 for Nikon Coolpix, or 16 for Gatan 4Kx4K, 5.0 for Gatan K2")
        self.add_argument('spherical_aberration_constant', type=float, default=2.0,
                help="Typical values: 2.0 (F30) 2.0 (CM12) 4.1 (JEOL4000), 3.1 (JEOL3100)")
        self.add_argument('accelerating_voltage', type=float, default=300,
                help=" Microscope voltage, in kV")
        self.add_argument('amplitude_contrast_ratio', type=float, default=0.07,
                help="Amplitude contrast ration. 0.07 for cryo")
        self.add_argument('pixel_size', type=float,
                help="pixel size on micrograph after movie alignment, in Angstrom")
        self.add_argument('interactive', action="store_true",
                help="Interactive add options")
        super(CFProjectParameter, self).add_arguments()

class CFProject(CFProjectParameter):
    """ A project instance. Contains the project informations """
    def __init__(self, proj_dir=None):
        super(CFProject, self).__init__()
        # default use current directory
        if proj_dir is None:
            proj_dir = os.getcwd()
<<<<<<< HEAD

=======
        self.workdir = proj_dir
>>>>>>> findkin
        m_file = os.path.join(proj_dir, 'cf-parameters.m')
        if os.path.exists(self.star_file):
            self.read(self.star_file)
        elif os.path.exists(m_file):
<<<<<<< HEAD
            from chuff import ChuffParameter
            cf = ChuffParameter()
            for key, value in vars(cf).items():
                setattr(self, key, value)
            self.write(self.star_file)
        else:
            print("No project parameter file found")
=======
            try:
                from chuff import ChuffParameter
                cf = ChuffParameter()
                for key, value in vars(cf).items():
                    setattr(self, key, value)
                self.write(self.star_file)
            except ImportError:
                print("Warning: Could not read old type parameter")
            except AttributeError:
                print("Warning: file format is wrong for %s" % m_file)

>>>>>>> findkin
    def run(self):
        ''' Init the project
        '''
        if not self.interactive:
            if self.target_magnification <= 0. or self.scanner_pixel_size <= 0.:
                if self.pixel_size <= 0.:
                    self.pixel_size = raw_input("Pixel Size(Angstrom): ")
                    if not self.pixel_size:
                        raise Exception("Please provide pixel_size in angstrom")
                self.target_magnification = 10000.
                self.scanner_pixel_size = self.pixel_size
            else:
                self.pixel_size = self.scanner_pixel_size * 10000. / self.target_magnification
            self.workdir = os.getcwd()
            for key, value in self[0].items():
                print key, '=', value
            print ''' If the parameters shows above is incorrect, please specify by rerun cf_init_project
            '''
        else:
            for key, act in self._args.items():
<<<<<<< HEAD
                if key in ["interactive", 'nuke', 'workdir', 'qsub']: continue
=======
                if key in ["interactive", 'nuke', 'workdir', 'qsub', 'debug']: continue
>>>>>>> findkin
               
                hlp = act.help
                if hlp == "": hlp = key
                vs = getattr(self, key)
                v1 = vs
                if vs is True or vs is False:
                    if vs is True: v1 = "(Y)/N"
                    else: v1 = "Y/(N)"
<<<<<<< HEAD
                prpt = "%s: %s: (%s) " % (key, hlp, v1)
=======
                prpt = "%s: (%s: (%s) )" % (key, hlp, v1)
>>>>>>> findkin
                while True:
                    new_v = raw_input(prpt)
                    if new_v != "":
                        if vs is True or vs is False:
                            if new_v.lower().startswith("y"):
                                setattr(self, key, True)
                            elif new_v.lower().startswith("n"):
                                setattr(self, key, False)
                            else:
                                continue
                        else:
                            setattr(self, key, new_v)
                    break
<<<<<<< HEAD
=======
            if self.pixel_size == 0:
                print("******************************************")
                print("Error: The pixel size is 0. Please re-run "
                        "cf_init_project and specify pixel_size")
                print("******************************************")
                return
>>>>>>> findkin
        self.write(self.star_file)
        # write the .m file
        f = open('cf-parameters.m', 'w')
        for key in self[0]:
            f.write("%s = %s;\n" % (key[3:], self[0][key]))
        f.close()
    
    @property
    def star_file(self):
        return os.path.join(self.workdir, 'cf_parameter.star')
    @property
    def micrograph_pixel_size(self):
        """ Used by some old script"""
        return self.pixel_size
    @micrograph_pixel_size.setter
    def micrograph_pixel_size(self, value):
        self.pixel_size = value
class OldCF(object):
    ''' Create an cf directory for old scripts.
    '''
    def __init__(self, *args, **kwargs):
        super(OldCF, self).__init__()
        self.chumps_round = 1
    
    def create_dirs(self):
        if not os.path.exists(self.scan_dir):
            os.mkdir(self.scan_dir)
        if not os.path.exists(self.chumps_dir):
            os.mkdir(self.chumps_dir)
        if not os.path.exists(self.cf_parameter_file):
            try:
                os.symlink(os.path.realpath('cf-parameters.m'), self.cf_parameter_file)
            except:
                import sys
                print sys.exc_info()
                print("Could not create cf-parameter file")
        if not os.path.exists(self.smooth_dir):
            os.mkdir(self.smooth_dir)

    @property
    def scan_dir(self):
        return os.path.join(self.workdir, "scans")

    @property
    def chumps_dir(self):
        return os.path.join(self.workdir, "chumps_round%d" % self.chumps_round)

    @property
    def cf_parameter_file(self):
        return os.path.join(self.workdir, 'cf-parameters.m')
    
    @property
    def smooth_dir(self):
        return os.path.join(self.chumps_dir, "smoothed_coords")
    
    
def get_next_workdir(task_name):
    ''' Get next workdir in task_name, and create the directory
    :return: task_name/jobxxx
    '''
    if not os.path.exists(task_name):
        os.mkdir(task_name)
    for i in range(1,10000):
        if not os.path.exists(os.path.join(task_name, 'job%03d' % i)):
            try:
                os.mkdir(os.path.join(task_name, 'job%03d' % i))
            except OSError as e:
                if e.errno != 17: #  File exists
                    raise e
            break
    return os.path.join(task_name, 'job%03d' % i)
