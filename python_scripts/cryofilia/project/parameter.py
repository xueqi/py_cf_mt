''' A wrapper for command line and star file interchangable class

'''
import os
from relion import Metadata, EMDLabel, DataType
from chuff_parser import ChuffParser
from chuff.util.mpi import MPI
from chuff.util.project import get_next_workdir
import logging
logger = logging.getLogger(__name__)

class Parameter(Metadata):
<<<<<<< HEAD
    """ Parameter
    """
    _args = {}
    _label=None
    def __init__(self, metaname = '', parser = None, *args, **kwargs):
        super(Parameter, self).__init__(isList = True, metaname = metaname,*args, **kwargs)
=======
    """ Parameter. This a star list style metadata. 
    """
    _args = {}
    _label=None
    def __init__(self, metaname='', parser=None, no_workdir=False, *args, **kwargs):
        super(Parameter, self).__init__(isList=True, metaname=metaname,*args, **kwargs)
>>>>>>> findkin
        if parser is not None:
            # read parser
            par = Parameter.parse(parser)
            self.update(par)
        self._parser = None
        self._arg_groups = {}
        self._args = {}
        self.add_arguments()
        self.cf = None
        self.profile_file = None
        self.logger = logger
        self.progress_bar = None
        self.workdir = ""
<<<<<<< HEAD
=======
        self.create_workdir = not no_workdir
>>>>>>> findkin
        try:
            from chuff import ChuffParameter
            self.cf=ChuffParameter()
        except:
            pass

    def add_arguments(self):
        self.add_argument('workdir', 
                help = "work directory")
        self.add_argument('qsub', action="store_true", 
                help = "submit to queue")
        self.add_argument('nuke', action="store_true",
                help = "Redo calculation")
        self.add_argument('debug', action="store_true",
                help = "Debug")
    
    def add_argument(self, lblName, *args, **kwargs):
        if lblName in self._args:
            logger.warn("%s already in args, skip" % lblName)
            return
        arg_group = self.parser
        group = kwargs.pop("group", None)
        metaname = kwargs.pop("metaname", None)
        nargs = kwargs.get('nargs')
        action = kwargs.get("action")
        tp = kwargs.get('type')
        if action in ['store_true', 'store_false']:
            tp = bool
        if nargs is not None:
            if tp is None:
                tp = kwargs.get('type', str)
            if tp is str:
                tp = DataType.strlist
            elif tp is int:
                tp = DataType.intlist
            elif tp is float:
                tp = DataType.floatlist
        if tp is None:
            tp = str
        if group is not None:
            if group in self._arg_groups:
                arg_group = self._arg_groups[group]
            else:
                arg_group = self.parser.add_argument_group(title = group, description = None)
                self._arg_groups[group] = arg_group
        if metaname is None:
            metaname = lblName
        lbl = EMDLabel.addLabel(metaname, tp, kwargs.get('help', ''))
        act = arg_group.add_argument("--%s" % lblName, *args, **kwargs)
        self._args[lblName] = act
    
        self.addLabel(metaname)
        self.set_attribute(metaname, lblName)

    def to_parser(self):
        ''' Generate a arg parser for this instance
        '''
        return self.parser

    @staticmethod
    def parse(parser, args = None):
        return vars(parser.parse_args(args))
#        return read_chuff_parameters.read_chuff_parameters(parser)

    def update(self, par):
        ''' update from a parsed object

        :param par: The result from read_chuff_parameters.read_chuff_parameters(parser)
        '''
        for key in self._args:
            setattr(self, key, par[key])
    def info(self, *arg, **kwargs):
        return self.logger.info(*arg, **kwargs)

    def debug(self, *arg, **kwargs):
        self.logger.debug(*arg, **kwargs)
    def warn(self, *arg, **kwargs):
        self.logger.warn(*arg, **kwargs)

<<<<<<< HEAD
    def get_next_workdir(self, label = None):
        label = label or self._label or ('CF' + self.__class__.__name__)
        return get_next_workdir(label)
=======
    def get_next_workdir(self, label = None, create=True):
        label = label or self._label or ('CF' + self.__class__.__name__)
        return get_next_workdir(label, create=create)
>>>>>>> findkin
    
    @classmethod
    def new_from_command_line(cls):
        parser = cls().to_parser()
        par = cls.parse(parser)
        p = cls()
        p.update(par)
<<<<<<< HEAD
        p.workdir = p.workdir or p.get_next_workdir()
=======
        p.workdir = p.workdir or p.get_next_workdir(create=p.create_workdir)
>>>>>>> findkin
        return p
    def __repr__(self):
        return "%s instance" % self.__class__
    
    def __str__(self):
        s = ""
        for key, value in self[0].items():
            s += "%s = %s\n" % (key, value)
        return s

    @property
    def parser(self):
        if self._parser is None:
            self._parser = ChuffParser()

        return self._parser
    
    def __getstate__(self):
        state = self.__dict__.copy()
        if '_parser' in state:
            del state['_parser']
        if '_arg_groups' in state:
            del state['_arg_groups']
        if '_args' in state:
            del state['_args']
        if 'logger' in state:
            del state['logger']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)

    def save(self):
        ''' Save parameter to workdir
        '''
        s_file = os.path.join(self.workdir, "parameter.star")
        self.write(s_file)

    def update_progress_bar(self, count):
        if not self.progress_bar:
            return
        self.progress_bar.update(count)

    def path(self, fname):
        return os.path.join(self.workdir, fname)

class MPIParameter(Parameter, MPI):
    def __init__(self, *args, **kwargs):
        super(MPIParameter, self).__init__(*args, **kwargs)
        MPI.__init__(self)

    def add_arguments(self):
        
        self.add_argument("cores", type=int, default=1,
                help="number of cores for paralellization")
        self.add_argument("nodes", type=int, default=1,
                help="number of nodes for parallelization")
        self.add_argument("queue", default="general",
                help="Queue/partition to run the program")
        super(MPIParameter, self).add_arguments()
    
    def bcasts(self):
        ''' Bcast every field in arguments
        '''
        if self._rank == 0:
            obj = self[0]
        else:
            obj = None
        self[0].update(self.bcast(obj))

    def __getstate__(self):
        state = super(MPIParameter, self).__getstate__()
        state['stdout'] = None
        state['stderr'] = None
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
