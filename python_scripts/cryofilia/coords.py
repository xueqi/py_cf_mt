import numpy as np

class Coord(list):
    def __init__(self,x=0,y=0,z=0):
        try:
            _ = x[2]
            super(Coord,self).__init__(x)
        except TypeError:
            super(Coord,self).__init__([x,y,z])
        except IndexError:
            super(Coord,self).__init__(*x)
    
    def __add__(self, other):
        from copy import copy
        result = copy(self)
        result+= other
        return result
    
    def __sub__(self, other):
        from copy import copy
        result = copy(self)
        result -= other
        return result

    def __isub__(self, other):
        try:
            self[0] -= other[0]
            self[1] -= other[1]
            self[2] -= other[2]
        except IndexError:
            pass
        return self
    def __iadd__(self, other):
        try:
            self[0] += other[0]
            self[1] += other[1]
            self[2] += other[2]
        except IndexError:
            pass
        return self
    def transform(self, euler):
        a = np.array(self).T
        b = np.matmul(euler,a)
        return Coord(b.tolist())

    @property
    def x(self):
        return self[0]
    @x.setter
    def x(self,v):
        self[0]=v

    @property
    def y(self):
        return self[1]
    @y.setter
    def y(self,v):
        self[1]=v

    @property
    def z(self):
        return self[2]
    @z.setter
    def z(self,v):
        self[2]=v
