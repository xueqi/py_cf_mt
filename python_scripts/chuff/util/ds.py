'''
Created on Feb 27, 2017

@author: xl256
'''

class IndexedPQ(object):
    '''
        Indexed priory queue. Max heap
        pq.add(key, value, weight) # log(n)
        key, value, weight = pq.getMax() # O(1)
        key, value, weight = pq.popMax() # log(n)
        pq.set(key, weight) # log(n)
        pq.getValue(key)  # O(1)
        
    '''
    def __init__(self, data = None, weights = None):
        if data is not None and weights is not None and len(data) == len(weights):
            self.data = data
            self.weights = weights
            self.heapify()
        else:
            self.data = []
            self.weights = {} # obj : [weight, index, value]
    
    def add(self, obj, value, weight):
        self.data.append(obj)
        self.weights[obj] = [weight, self.size() - 1, value]
        self.sift_up(self.size() - 1)
        
    def has_key(self, key):
        return self.weights.has_key(key)
    def set(self, obj, weight):
        if obj not in self.weights:
            raise Exception("No key named: %s exists" % obj)
        _, idx, _ = self.weights[obj]
        self.weights[obj][0] = weight
        self.sift_up(idx)
        self.sift_down(idx)
    
    def getValue(self, key):
        return self.weights[key][2]
    
    def getMax(self):
        '''
            return key, value, weight pair for max
        '''
        return [self.data[0],self.weights[self.data[0]][2], self.weights[self.data[0]][0]]
    
    def popMax(self):
        '''
        pop the max and
        return key, value, weight pair for max
        '''
        obj, value, weight  = self.getMax()
        # remove obj from weights
        del self.weights[obj]
        # move last item to first
        self.data[0] = self.data[-1]
        self.weights[self.data[0]][1] = 0   # tmp update the index
        # pop last
        self.data.pop()
        # sift_down of first item, to maintain the heap
        self.sift_down(0)
        
        return obj, value, weight
    
    def sift_down(self, idx):
        lc = idx * 2 + 1                    # left child
        rc = idx * 2 + 2                    # right child
        if lc >= self.size(): return
        lco = self.data[lc]                 # left child object
        mxc, mco = lc, lco                  # max child, max child object
        obj = self.data[idx]
        obj_w = self.weights[obj][0]
        if rc < self.size():
            rco = self.data[rc]
            if self.weights[rco][0] > self.weights[lco][0]:
                mxc, mco = rc, rco
        if self.weights[mco][0] > obj_w:
            # swap mco and obj
            self.data[idx] = mco
            self.data[mxc] = obj
            self.weights[obj][1] = mxc              # weight no change, idx change
            self.weights[mco][1] = idx              # update the index
            # recursive 
            self.sift_down(mxc)
        # else: finished. 
        
    def sift_up(self, idx):
        '''
            shift up obj in idx if bigger than parent 
        '''
        if idx == 0: return                 # 
        pt = (idx - 1) / 2
        pto = self.data[pt]
        obj = self.data[idx]
        if self.weights[obj][0] > self.weights[pto][0]:
            self.data[idx] = pto                    # swap the object
            self.data[pt] = obj
            self.weights[pto][1] = idx              # update the index
            self.weights[obj][1] = pt
            
            # recursive
            self.sift_up(pt)
    
    def heapify(self):
        for i in range(self.size() / 2 + 1):
            self.sift_down(i)
        
    def size(self):
        return len(self.data)