'''
    command utils is to run program inside python. New process will be spawned outside current program,
    so the GIL(global interupt lock) won't restrict the cpu resource. 
    But be sure to allocate enough cpu resources when using batch system like slurm
    
    to run command 'ls'
    run_command('ls')
    
    to run command 'ls -l'
    run_command('ls', '-l')
    
    to run command relion_reconstruct --i a.star --o b.mrc --ctf
    run_command('relion_reconstruct', '--i', 'a.star', '--o', 'b.mrc', '--ctf')
    or
    cmd = build_commands('relion_reconstruct', i='a.star', o = 'b.mrc', ctf=True, seperator=' ', prefix='--')
    run_command(cmd)
    
    to run command in background, use join=False
    p = run_command('relion_reconstruct', '--i', 'a.star', '--o', 'b.mrc', '--ctf', join = False)
    and use p.join() to wait the job finish


    to capture the output, use logger

'''

import os
import time
import Queue
from threading import Thread

def enqueue_output(out, queue):
    for line in iter(out.readline, b''):
        queue.put(line)
    queue.put(None)

def _run(cmd, env = None, logger = None, output_queue = None, error_queue = None):
    import subprocess
    try:
        sp = subprocess.Popen(cmd, env=env, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    except OSError as e:
        print cmd
        raise(e)
    q = Queue.Queue()
    t = Thread(target=enqueue_output, args = (sp.stdout, q))
    t.daemon = True
    t.start()
    eq = Queue.Queue()
    et = Thread(target=enqueue_output, args = (sp.stderr, eq))
    et.daemon = True
    et.start()    
    while True:
        try:
            output = q.get_nowait()
            if output_queue and output is not None:
                output_queue.put(output.strip())
            #print output,
            if output is None:
                break
            error = eq.get_nowait()
            if error_queue:
                error_queue.put(error)
        except Queue.Empty:
            pass
        time.sleep(0.01)
    output_queue.put(None)

def run_command(cmd, *args, **kwargs):
    #workdir = kwargs.pop("workdir", None)
    workdir = None
    logger = kwargs.pop("logger", None)
    join = kwargs.pop("join", None)
    env = kwargs.pop("env", None)
    stdout = kwargs.pop("stdout", None)
    cmd = build_commands(cmd, *args, **kwargs)
    return run_command_1(cmd, workdir = workdir, logger = logger, join = join, env = env, stdout=stdout)
    
def run_command_1(cmd, workdir = None, logger = None, join = None, env = None, stdout=None):
    '''
        run command from command line, in a separate process. This is used to overcome the global interrupt lock(GIL)
        @param workdir: The job should run in
        @param logger: The logger objects passed to the command to write the output
        @param join: Wait the process to finish or not
        @param env: The environment should passed to the command
    '''
    if join is None: join = True
    if workdir is not None:
        os.chdir(workdir)
    from multiprocessing import Process, Queue as MQueue

    output_queue = MQueue()
    error_queue = MQueue()

    cmd = map(str, cmd)
    if logger: logger.info(" ".join(cmd))
    p = Process(target = _run, args = (cmd,),
                 kwargs = {'env'            : env, 
                           'logger'         : logger, 
                           'error_queue'    : error_queue, 
                           'output_queue'   : output_queue})
    p.start()
    
    # if join, if wait the program to finish
    if join:
        output = ""
        error = ""
        time.sleep(0.1)
        while True: 
            try:
                tmp_out = output_queue.get_nowait()
                if tmp_out is None:
                    break
                else:
                    output+=tmp_out
                    print tmp_out
                if logger:
                    logger.info(tmp_out.strip())
                tmp_err = error_queue.get_nowait()
                if tmp_err is not None:
                    error += tmp_err
                    print tmp_err
            except Queue.Empty:
                pass
            time.sleep(0.001)
        if logger and error:
            logger.error(error)
        return output
    else: # otherwise just return the process, let the caller to handler
        return p

def build_commands(cmd, *args, **kwargs):
    '''
        build command from function
        cfjob: prefix : '', sep : '='
        python: prefix : '--', sep : ' '
        eman prefix : '--', sep : '='
        default is for cfjob
    '''
    prefix = kwargs.pop("prefix", '')
    seperator = kwargs.pop('sep', '=')
    if type(cmd) is list or type(cmd) is tuple:
        cmd = list(cmd)
    else:
        cmd = [cmd]
    cmd.extend(args)

    for key, value in kwargs.items():
        if value is None: continue
        if value is not False:
            cmd.append('%s%s' % (prefix, key))
            if value is not True:
                if type(value) is list or type(value) is tuple:
                    value = ",".join(map(str, value))
                if seperator.strip():
                    cmd.append(seperator)
                
                cmd.append(str(value))
    return cmd
