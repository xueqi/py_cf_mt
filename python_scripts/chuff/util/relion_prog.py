'''
Created on Feb 28, 2017

@author: xl256
'''

from command import build_commands, run_command

from functools import partial

def qsub_run(cmd, cores = 1, nodes = 1, workdir = None, logger = None, join = True):
    '''
        use slurm to run the job
    '''
    CPU_PER_NODE = 20
    max_job_per_nodes = CPU_PER_NODE / cores
    nodes_needed = nodes / max_job_per_nodes
    if nodes_needed * max_job_per_nodes < nodes: nodes_needed+=1 
#    cmd = build_commands('srun', '-n', nodes, '-c', cores, 
#                         *cmd)
#    exit()
    from chuff.util.batch.slurm import SlurmBatch
    sb = SlurmBatch()
    sb.cores = cores
    sb.ntasks = nodes
    sb.mem_per_cpu = 7500   
    import os

    script_file = "%s.script" % os.path.basename(cmd[0])
    cmd =  " ".join(map(str, cmd))
    sb.submit(cmd, script_file = script_file)
#    run_command(cmd, workdir, logger, join)

def relion_command(cmd_name, logfile = None, errfile = None, *args, **kwargs):
    '''
        Default parameters: 
        logger: None, no log out
        workdir: None, current workdir
        join: True, wait the command to finish
        @param build_command_only: only build the command line and return the command line
    '''
    logger = kwargs.pop('logger', None)
    workdir = kwargs.pop('workdir', None)
    join = kwargs.pop('join', True)
    cores = kwargs.pop('cores', 1)
    nodes = kwargs.pop('nodes', 1)
    qsub = kwargs.pop('qsub', False)
    if (logger is None) and logfile:
        import logging
        fh = logging.FileHandler(logfile)
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.DEBUG)
        logger.addHandler(fh)
    if logger:
        import sys
        logger.addHandler(logging.StreamHandler(sys.stdout))
        logger.info("Running command: %s, with parameter : %s" % (cmd_name, str(kwargs)))
    if cores > 1:
        kwargs['j'] = cores
    build_command_only = kwargs.pop("build_command_only", False)
    
    cmd = build_commands(cmd_name, sep = ' ', prefix = '--', *args, **kwargs)
    if nodes > 1:
        cmd = build_commands('mpirun', "--oversubscribe", '--map-by', 'node', '--np', str(nodes), *cmd)
#    exit()
    if qsub:
        qsub_run(cmd, workdir = workdir, logger = logger, join = join, cores=cores, nodes=nodes)
        return
    
    if build_command_only:
        return cmd
    import os
    print " ".join(cmd)
    run_command(cmd, workdir = workdir, logger = logger, join = join, env = os.environ.copy())

relion_preprocess = partial(relion_command, 'relion_preprocess')
relion_refine = partial(relion_command, 'relion_refine')
relion_refine_mpi = partial(relion_command, 'relion_refine_mpi')
relion_reconstruct = partial(relion_command, 'relion_reconstruct')
relion_postprocess = partial(relion_command, 'relion_post_process')
relion_mask_create = partial(relion_command, 'relion_mask_create')

# `which relion_preprocess` --i /ysm-gpfs/pi/sindelar/mike//krios_session_analysis_first500/CFCTFFind/job001/micrographs_ctf.star --reextract_data_star CFRefAlign/job075/particles_out_pf12_relion.star --recenter --part_star Extract/job029/particles.star --part_dir Extract/job029/ --extract --extract_size 480 --scale 240 --norm --bg_radius 119 --white_dust -1 --black_dust -1 --invert_contrast  --helix --helical_outer_diameter 380 --helical_bimodal_angular_priors

relion_extract = partial(relion_preprocess,  )

refine_mt_opt = {
        'sigma_ang' : 0.5,
        'dont_check_norm' : True,
        'auto_local_healpix_order':0,
        'healpix_order':6
        }
relion_refine_mt = partial(relion_refine, *refine_mt_opt)
relion_refine_mt_mpi = partial(relion_refine_mpi, *refine_mt_opt)

