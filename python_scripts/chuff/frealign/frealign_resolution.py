import os
class FrealignResolution(object):

    def __init__(self, frealign_res_file = None):
        self.res_file = frealign_res_file
        if self.res_file is not None:
            self.resolution_dict = self.read()
        else:
            self.resolution_dict = {}

    def read(self, filename = None):
        if filename is not None:
            self.res_file = filename
        d = {}
        data_flag = False
        with open(self.res_file) as res_f:
            n_ptcls, overall_score, avg_occ = -1, -1, -1
            fraction_mask, fraction_particle = -1, -1
            field_ends = [0, 1, 6, 13, 23, 30, 37, 47, 58, 68, 77, 86, 95, 101, 109]
            n_fields = len(field_ends) - 1
            line = res_f.readline()
            while line:
                if 'Total particles included' in line:

                    n_ptcls, overall_score, avg_occ = [float(m) for m in line.strip().split()[-3:]]
                elif 'Fraction_mask, Fraction_particle' in line:
                    fraction_mask, fraction_particle = [float(m) for m in line.strip().split()[-2:]]
                elif 'RESOL' in line:
                    fields = [line[field_ends[i]: field_ends[i+1]].strip() for i in range(n_fields)]
                    for field in fields:
                        d[field] = []
                    data_flag = True
                elif 'Average' in line:
                    data_flag = False

                elif data_flag:
                    for k in range(1, n_fields):
                        d[fields[k]].append(float(line[field_ends[k]:field_ends[k+1]]))
                line = res_f.readline()
            
            return d
    def getFSC(self, fsc):
        fscs = self.resolution_dict['FSC']
        ress = self.resolution_dict['RESOL']
        for i in range(len(fscs)):
            if fscs[i] < fsc:
                i -= 1
                break
        return ress[i]
    def get_resolution_dict(self):
        return self.resolution_dict
