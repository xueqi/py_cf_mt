#!/usr/bin/env python

import ConfigParser
import StringIO
import argparse
import inspect
import os, sys
import datetime, socket
import cf_utilities

def read_chuff_parameters(argparser = None, \
                          chuff_parameter_file = 'cf-parameters.m', \
                          chuff_machine_parameter_file = 'cf-machine-parameters.m', \
                          errorcheck = None):

  chuff_path = os.path.dirname(os.path.abspath(inspect.stack()[0][1]))
  chuff_path = os.path.split(chuff_path)[0]

###################
# Read the command-line arguments
###################

######
# First convert the command-line arguments to the standard form used by argparse.
#  This allows for the original chuff-style syntax where options are specified 
#  as var=1,10,3 rather than --var 1 10 3 as argparse requires
######

######
# Get rid of spaces around '='
######
  t1 = []
  index = 1
  while index < len(sys.argv):
    if index > 1 and sys.argv[index][0] == '=':
      t1[-1] = t1[-1] + sys.argv[index]
    else:
      t1 = t1 + [sys.argv[index]]
    index = index + 1

######
# Get rid of commas
######
  t2 = []
  for p in t1:
    t2 = t2 + p.split(',')

######
# Replace flags 'flag=' with '--flag'
######
  new_argv = []
  for p in t2:
    if '=' in p:
      p = p.split('=')
      p[0] = '--' + p[0]
      for q in p:
        new_argv.append(q)
    else:
      new_argv.append(p)

######
# Get rid of null strings created by split()
######
  new_argv = filter(None,new_argv)

###################
# We will use ConfigParser to read our octave/matlab compatible parameter file;
#  but in order for this to work, we need to add a section label: 
#   "[chuff parameters]" in this case.
# To do this, we create a virtual file using the StringIO module
###################

#######
# Create the virtual file and add a section header
#######
  chuff_parameters_virtual = StringIO.StringIO()
  chuff_parameters_virtual.write('[chuff parameters]\n')

#######
# Send 'cf-parameters.m' to it
#######
  try:
    chuff_parameters = open(chuff_parameter_file, 'r');
  except IOError:
    chuff_parameter_file = 'chuff_parameters.m'
    try:
      chuff_parameters = open(chuff_parameter_file, 'r');
    except IOError:
      if errorcheck:
        print 'Note: did not find cf-parameters.m in the current directory...'
      chuff_parameters = []

  if chuff_parameters:
    for line in chuff_parameters:
      if line.strip().startswith("#"):continue
      chuff_parameters_virtual.write(line);
    chuff_parameters.close()

#######
# Send 'chuff_machine_parameters.m' to it
#######

  chuff_machine_path = os.path.split(chuff_path)[0] + '/chuff4'
  chuff_machine_parameter_file = chuff_machine_path + '/' + chuff_machine_parameter_file

  try:
    chuff_machine_parameters = open(chuff_machine_parameter_file, 'r');
  except IOError:
    chuff_machine_path = os.path.split(chuff_path)[0]
    chuff_machine_parameter_file = chuff_machine_path + '/chuff_machine_parameters.m'

    try:
      chuff_machine_parameters = open(chuff_machine_parameter_file, 'r');
    except IOError:
      if errorcheck:
        print 'Note: did not find chuff_machine_parameters.m in the chuff4 directory...'
      chuff_machine_parameters = []

  if chuff_machine_parameters:
    for line in chuff_machine_parameters:
        if line.strip().startswith('#'): continue
        chuff_parameters_virtual.write(line);

#######
# Parse the virtual file
#######
  cfg = ConfigParser.RawConfigParser()

  chuff_parameters_virtual.seek(0)

  cfg.readfp(chuff_parameters_virtual)  # Read virtual file (with section header added)
  par=dict(cfg.items("chuff parameters"))
  for p in par:
    par[p]=par[p].split("#",1)[0].strip() # Get rid of inline comments
    par[p]=par[p].strip(";") # Get rid of awk/C-style line terminators
    par[p]=par[p].strip("'") # Get rid of octave-style single quotes

  chuff_parameters_virtual.close()
    
######
# Now use the argparse module to read the command-line arguments
#  note that parse_args() returns a Namespace object; we must use vars()
#  to convert this to a dictionary object
######

  if argparser:
    par.update(vars(argparser.parse_args(new_argv)))

# convert the values from str to float, int,
  float_pars = ["accelerating_voltage", "helical_repeat_distance", 
                "filesync_pause", "scanner_pixel_size", 
                "target_magnification", "amplitude_contrast_ratio", 
                "wedge_offset", "filament_outer_radius", 
                "spherical_aberration_constant"]
  int_pars = ["invert_density", "num_repeat_residues", 
              "max_cpus", "cores", "nodes"]
  for p in par:
    if par[p] is None or par[p] is True or par[p] is False: continue
    if p in float_pars: par[p] = float(par[p])
    elif p in int_pars: par[p] = int(par[p])
    
######
# Now tweak the octave executable, if given, to give the location of the chuff libraries
######

  if 'octave_prog' in par:
    par['octave_prog'] = par['octave_prog'] + \
                     ' --path ' + chuff_path + '/octave_scripts'
    if 'operating_system_type' in par:
      par['octave_prog'] = par['octave_prog'] + \
                         ' --path ' + chuff_path + '/octave_mex_' + par['operating_system_type']

######
# Compute the micrograph pixel size
######

  if 'scanner_pixel_size' in par and 'target_magnification' in par:
    par['micrograph_pixel_size'] = float(par['scanner_pixel_size'])/float(par['target_magnification']) * 10000

######
# Archive the command in a logfile
######
  f = open('cf-commands.log', 'a')
  inputline=sys.argv[0]
  for ind in range (1,len(sys.argv)):
    inputline = inputline + ' ' + sys.argv[ind]
  f.write(inputline + ' # ' + str(datetime.datetime.now()) + ' ' + socket.gethostname() + \
          ' ' + os.path.dirname(os.path.abspath(inspect.stack()[-1][1])) + '\n')

#  f.write('# ' + str(par) + '\n')
  f.close()
<<<<<<< HEAD
  import pickle
  pickle.dump(par, open("par.pkl",'w'))
  print par
=======
>>>>>>> findkin
  return cf_utilities.Bunch(par)
