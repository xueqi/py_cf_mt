'''
Created on May 26, 2017

@author: xueqi
'''
import unittest
import numpy as np

class TestCTF(unittest.TestCase):

    @unittest.skip("diskio")
    def testToImage(self):
        from cryofilia.ctf import CTF
        c = CTF()

        c = CTF(defocusangle = 0.5, defocusu = -3000, defocusv = -2000)
        
        c = CTF(defocusangle = 0.5, defocusu = -30000, defocusv = -20000)
        c.toImage(256, 4).write_image('ctf.mrc')


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()