'''
Created on Jun 22, 2017

@author: xueqi
'''
import unittest

from testCryofilia import TestWithFile
import os
from cryofilia.helix.ref_align import RefAlign, RefAlignParameter

class TestRefAlignParameter(TestWithFile):
    def setUp(self):
        pass
    def tearDown(self):
        pass
    def testInit(self):
        pass

class TestRefAlign(TestWithFile):

    def setUp(self):
        super(TestRefAlign, self).setUp()
        self.temp_dir = '/tmp/tmpWfT9uP'
        if not os.path.exists(self.temp_dir):
            os.mkdir(self.temp_dir)
        os.chdir(self.temp_dir)
        try:
            os.symlink(self.get_data_path("Micrographs"), "Micrographs")
        except OSError:
            pass
    def tearDown(self):
        print self.temp_dir
        #super(TestRefAlign, self).tearDown()

    def testRefAlign(self):
        from relion import Metadata
        rf = RefAlign()
        rf.pixelSize = 4.4
        rf.boxSize = 128
        rf.doRadon = True
        rf.micrographPixelSize = 2.49
        rf.helical_rise = 80
        rf.thetaRange = 15
        rf._align_param.write('ref_align.star', withComment = True)
        rf.add_reference(volume = "mt_14.mrc", num_pf = 14, voxel_size=rf.boxSize)
        #rf.add_reference(num_pf = 14)
        # rf._refVolumes[14]._volume.write_image('mt_14.mrc')
        m = Metadata.readFile(self.get_data_path("CFImport/job002/particles.star"))
        filas = m.group_by('MicrographName', 'HelicalTubeID')
        for fila in filas.values():
            fila = rf.align(fila)
            print fila

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()