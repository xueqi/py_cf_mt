import unittest
import os
import numpy as np

class TestWithFile(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestWithFile, self).__init__(*args, **kwargs)

    def setUp(self):
        import tempfile
        self.temp_dir = tempfile.mkdtemp()

        self.data_dir = self.getTestDataDir()
        super(TestWithFile, self).setUp()

    def getDataDir(self):
        return self.__class__.__name__

    def getTestDataDir(self):
        return os.path.join(os.path.dirname(__file__),
                             'test_data',
                             self.getDataDir())

    def tearDown(self):
        import shutil
        shutil.rmtree(self.temp_dir)
        super(TestWithFile, self).tearDown()


    def get_data_path(self, filename):
        pth = os.path.join(self.data_dir, filename)
        self.assertTrue(os.path.exists(pth), "path should exist: %s" % pth)
        return pth

    def assertSequenceAlmostEqual(self, s1, s2, msg = None):
        return np.testing.assert_array_almost_equal(s1, s2, 2, msg)

    def t_function(self, func, args, expected_result):
        result = func(*args)
