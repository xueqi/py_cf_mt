'''
Created on Apr 2, 2018

@author: xueqi
'''
from . import TestCommand
import unittest
import os

class TestInitProject(TestCommand):
    def test_cmd(self):
        self.command("cf_init_project", None, input="1.5")
        self.assertFalse(os.path.exists(os.path.join(self.wd, "CFCFProject")))
        from cryofilia.project import CFProject
        proj = CFProject(self.wd)
        self.assertEqual(proj.pixel_size, 1.5)
        
    def test_cmd_no_interactive(self):
        self.command("cf_init_project --pixel 1.5")
        self.assertFalse(os.path.exists(os.path.join(self.wd, "CFCFProject")))
        from cryofilia.project import CFProject
        proj = CFProject(self.wd)
        self.assertEqual(proj.pixel_size, 1.5)
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
