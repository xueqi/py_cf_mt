BEGIN {
  min_index = 1;
  max_index = 0;
}

NF > 0 {
  if($1 != "C") {
    line[$1] = $0;

    if($1 > max_index)
      max_index = $1;
    if($1 < min_index)
      min_index = $1;
  } else {
    ++comment_count;
    comments[comment_count] = $0;
  }
}

END  {
  for(i = 1; i <= comment_count; ++i)
    print comments[i];
  for(i = min_index; i <= max_index; ++i)
      print line[i];
}
