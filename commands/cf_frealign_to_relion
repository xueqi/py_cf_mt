#!/usr/bin/env python
''' Convert frealign refinement to relion refinement
'''
import os

from chuff_parser import ChuffParser, ChuffGooey
from relion import Metadata
from chuff.frealign import Frealign


def setup_arg_parser():

    parser = ChuffParser(description = "Convert frealign refine to relion refine")
    
    fr = parser.add_argument_group("Frealign Info")
    fr.add_argument('--frealign_dir', widget = "DirChooser", required = True,
            help = "The frealign directory")
    fr.add_argument('--frealign_round', type = int,  required = True, 
            help = "The round of frealign to export to relion")
    
    rel = parser.add_argument_group("Relion Info")
    rel.add_argument("--old_relion_star", widget = "FileChooser", required = True,
            help = "The star file used to generate this frealign dir")
    rel.add_argument("--output_star", widget = "FileChooser", 
            help = "The output star file")
    
    return parser


@ChuffGooey
def my_func():

    parser = setup_arg_parser()
    par = parser.parse_args()
    from chuff.util.project import get_next_workdir
    wd = get_next_workdir('RelionRefine')
    
    if par.output_star is None:
        output_star = os.path.join(wd, 'particles.star')
    else:
        output_star = par.output_star
    fr = Frealign(par.frealign_dir)
    print("Reading Frealign parameter")
    fr.read(frealign_round = par.frealign_round)
    bin_factor = fr.info['bin_factor']
    for stk_file in list(fr.stacks.keys()):
        bname = os.path.basename(stk_file)
        if bname != stk_file:
            fr.stacks[bname] = fr.stacks.pop(stk_file)
    print("Reading old star file")
    m = Metadata.readFile(par.old_relion_star)
    mgs = m.group_by('MicrographName')
    result = Metadata()
    result.copyLabels(m)
    # copy result from frealign to relion
    print("Copying parameters")
    for mgname in mgs:
        stk_file = "%s.mrc" % os.path.splitext(os.path.basename(mgname))[0]
        if not stk_file in fr.stacks:
            f = open('debug.txt','w')
            f.write("\n".join(fr.stacks.keys()))
            f.write("\n\n\n")
            f.write(stk_file)
            f.close()
            raise Exception("%s not in fr" % stk_file)
        fp = fr.stacks[stk_file]
        assert(len(fp) == len(mgs[mgname]))
        m1 = mgs[mgname]
        for ii in range(len(fp)):
            pm = fp[ii]
            mm = m1[ii]
            for lbl in ['OriginX', 'OriginY', 'CoordinateX', 'CoordinateY', 'DefocusU', 'DefocusV', 'DefocusAngle', 'AngleRot', 'AngleTilt', 'AnglePsi']:
                mm[lbl] = pm[lbl]
                if lbl in ['OriginX', 'OriginY']:
                    mm[lbl] = mm[lbl] * bin_factor
            result.append(mm)
    result.write(output_star)
    print("star file written to %s" % output_star)
    print("Do particle extraction before proceed to refinment/reconstruction")

if __name__ == "__main__":
    my_func()
