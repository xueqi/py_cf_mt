##########################################
# "spifcheck.awk" -- a nonessential "helper" script to check for syntax errors associated with
#  ".spif" variables...
##########################################

BEGIN {
  mathop_regexp = "[=]|[,]";

  mathfunc_regexp = "(" "([Pp][Aa][Dd])" "|" "([Ss][Ii][Nn])" "|" "([Cc][Oo][Ss])" "|" "([Ee][Xx][Pp])" "|" "([Ll][Oo][Gg])" "|" "([Ll][Oo][Nn])" "|" "([Ss][Qq][Rr])" "|" "([Ss][Qq][Rr][tT])" "|" "([Ii][Nn][Tt])" "|" "([Aa][Bb][Ss])" "|" "([Aa][Tt][Aa])" "|" "([Aa][Ss][Ii])" "|" "([Aa][Cc][Oo])" "|" "([Tt][Aa][Nn])" "|" "([Rr][Aa][Nn])" "|" "([Rr][Nn][Nn])" "|" "([Ii][Ff])" "|" "([Tt][Hh][Ee][Nn])" "|" "([Ee][Nn][Dd][Ii][Ff])" "|" "([Dd][Oo])" ")" "(" " *[(]" ")";
                                               # will match all spider function names

  math_comp_regexp = "[.]" "(" "[Ee][Qq]" "|" "[Nn][Ee]" "|" "[Gg][Tt]" "|" "[Gg][Ee]" "|" "[Ll][Tt]" "|" "[Ll][Ee]" ")" "[.]";
                                               # comparators in if... then... statements

  spider_control_regexp = "(^| )[iI][fF] *[(]" "|" "(^| )[tT][hH][eE][nN]( |$)" "|" "(^| )[eE][lL][sS][eE]( |$)" "|" "(^| )[dD][oO]( |$)" "|" "(^| )[gG][oO][tT][oO]( |$)" "|" "(^| )[gG][oO][ \t][tT][oO]( |$)";
                                               # matches spider control statements

  spider_proc_call_regexp = "[@][A-Za-z][A-Za-z0-9_]+";
                                               # matches spider "@" function calls

  get_spider_fns();

  spidersubs_regexp = "[#][#][A-Za-z0-9_]+([[][A-Za-z0-9_.]+[]])*[#][#]";     # "spidersubs" in mt_spiderscript.csh of the form "##blah##"
  var_regexp = "[$][A-Za-z0-9_]+";                     # "regular expression" that defines an initial "$"
                                                       #   followed by any number of alphanumeric characters or 
                                                       #   the underscore "_"

  register = "[xX][0-9][0-9]?";                  # spider register like "x11"
  newregister = "(\\[)[a-zA-Z_0-9]+(\\])";   # spider register like "[var]"
  register_regexp = "(" register ")|(" newregister ")";

  suspect_variable = "(^|[^0-9])[A-Za-z][A-Za-z0-9_]+";    # template for a "suspicious variable"
                                                           #   (variable-like name but missing the dollar sign...)
                                                           # reject leading numbers to avoid flagging nos like "1E4"...

  suspect_variable_real = "[A-Za-z][A-Za-z0-9_]+";     # template for a "suspicious variable"
                                                       #   (but without the leading non-numeric character)
  spider_cmd = "( +|^)[A-Za-z][A-Za-z] ";              # template for spider two-letter commands
  spider_regcmd = spider_cmd "[ \t]*" suspect_variable;  # template for registers following a spider command
}

{
  if(last_line ~ "^[ \t]*vm") {              # if the last line was "vm", this line is a shell script line
    gsub(".*","",$0);                     #  and will contain weird, unix strings and would generate lots
  }                                        #  of spif warnings, so we just skip the line.
  last_line = $0;

  gsub("[;].*","",$0);                    # eliminate comments
  gsub("[?].*[?]","",$0);                 # eliminate spider prompts of the form '?....?'

  if($0 ~ mathop_regexp || $0 ~ spider_regcmd) {
    sub("^[ \t]*[sS][dD].*","");              # eliminate entire "SD" lines...
    sub("^[ \t]*(/bin/rm|rm)[ \t]+.*","");          # eliminate entire "rm" lines from shell commands...
    sub("^[ \t]*(/bin/mv|mv)[ \t]+.*","");          # eliminate entire "mv" lines from shell commands...
    sub("^[ \t]*(/bin/cp|cp)[ \t]+.*","");          # eliminate entire "cp" lines from shell commands...

    for(i = 1; i <= n; ++i)
      gsub("^ *("spider_fns2[i] ")( +|$)","",$0);              # eliminate spider commands

    gsub(var_regexp,"",$0);                 # eliminate spif variables
    gsub(register_regexp,"",$0);            # eliminate registers
    gsub("[lL][bB][0-9]*","",$0);           # eliminate LB loop statements

    gsub(math_comp_regexp, "", $0);            # eliminate math operators
    gsub(mathfunc_regexp, "", $0);   # eliminate math functions
    gsub(spidersubs_regexp, "", $0);               # eliminate "spidersubs" used by mt_spiderscript.csh
    gsub(spider_control_regexp, "", $0);    # eliminate spider control statements
    gsub(spider_proc_call_regexp, "", $0);    # eliminate spider control statements

    while(match($0,suspect_variable)) {                    # detect variable-like strings
      bad_var = substr($0,RSTART,RLENGTH);
      match(bad_var,suspect_variable_real);
      print " -> "last_line;
      print "################";
      print "WARNING: \""substr(bad_var, RSTART,RLENGTH)"\" may be a spif variable missing the \"$\"";
      print "################";
      sub(suspect_variable,"",$0);
    }
  }
}

function get_spider_fns()
{
  spider_fns = "";

#############################
# A modification is required in this list:  all "short" commands like AC come after their "long" variants 
#   like "AC MS"... otherwise weird errors will occur!!!
# Easy way to do this: take the original spider list (but with the extra MD commands found under its web page),
#   and reverse its order....
#############################

  spider_fns = spider_fns "(WV S)" "|" "(WV P)" "|" "(WV)" "|" "(WU)"; 
  spider_fns = spider_fns "|" "(WT TV)" "|" "(WT)" "|" "(WI B)" "|" "(WI)" ;
  spider_fns = spider_fns "|" "(VO RAS)" "|" "(VO RA)" "|" "(VO MQ)" "|" "(VO MD)"; 
  spider_fns = spider_fns "|" "(VO EA)" "|" "(VM)" "|" "(VA F)" ;
  spider_fns = spider_fns "||" "(UD S)" "|" "(UD N)" "|" "(UD ICE)" "|" "(UD IC)"; 
  spider_fns = spider_fns "|" "(UD E)" "|" "(UD)" "|" "(TT COPY)" ;
  spider_fns = spider_fns "|" "(TT)" "|" "(TP)" "|" "(TM)" "|" "(TH M)"; 
  spider_fns = spider_fns "|" "(TH F)" "|" "(TH C)" "|" "(TH)" ;
  spider_fns = spider_fns "|" "(TF SNR)" "|" "(TF L)" "|" "(TF ED)" "|" "(TF EA)"; 
  spider_fns = spider_fns "|" "(TF DNS)" "|" "(TF DEV)" "|" "(TF DDF)" ;
  spider_fns = spider_fns "|" "(TF D)" "|" "(TF CTS)" "|" "(TF CT3)" "|" "(TF CT)"; 
  spider_fns = spider_fns "|" "(TF CRF)" "|" "(TF C3)" "|" "(TF C)" ;
  spider_fns = spider_fns "||" "(TF)" "|" "(SZ)" "|" "(SU)" "|" "(ST)"; 
  spider_fns = spider_fns "|" "(SR)" "|" "(SQRT)" "|" "(SQ)" ;
  spider_fns = spider_fns "|" "(SP)" "|" "(SO C)" "|" "(SO)" "|" "(SN RF)"; 
  spider_fns = spider_fns "|" "(SN RB)" "|" "(SL)" "|" "(SK R)" ;
  spider_fns = spider_fns "|" "(SK)" "|" "(SH F)" "|" "(SH)" "|" "(SE)"; 
  spider_fns = spider_fns "|" "(SD SHUFFLE)" "|" "(SD S)" "|" "(SD E)" ;
  spider_fns = spider_fns "|" "(SD C)" "|" "(SD)" "|" "(SA P)" "|" "(SA E)"; 
  spider_fns = spider_fns "|" "(SA 3)" "|" "(SA)" "|" "(RT SQ)" ;
  spider_fns = spider_fns "||" "(RT M)" "|" "(RT C)" "|" "(RT B)" "|" "(RT 90)"; 
  spider_fns = spider_fns "|" "(RT 3LS)" "|" "(RT 3L)" "|" "(RT 3DS)" ;
  spider_fns = spider_fns "|" "(RT 3D)" "|" "(RT 3AS)" "|" "(RT 3A)" "|" "(RT 3)"; 
  spider_fns = spider_fns "|" "(RT)" "|" "(RR)" "|" "(RP)" ;
  spider_fns = spider_fns "|" "(RO I)" "|" "(RO)" "|" "(RM SUM3N)" "|" "(RM RESET)"; 
  spider_fns = spider_fns "|" "(RM MAKE3)" "|" "(RM INV3N)" "|" "(RM INV2N)" ;
  spider_fns = spider_fns "|" "(RM FTN)" "|" "(RM FTINVN)" "|" "(RM FILTER)" "|" "(RM COMPLN)"; 
  spider_fns = spider_fns "|" "(RM 3DN)" "|" "(RM 2DN)" "|" "(RM)" ;
  spider_fns = spider_fns "||" "(RF SN)" "|" "(RF 3SN)" "|" "(RF 3)" "|" "(RF)"; 
  spider_fns = spider_fns "|" "(RE)" "|" "(RC)" "|" "(RA)" ;
  spider_fns = spider_fns "|" "(PW L)" "|" "(PW 2)" "|" "(PW)" "|" "(PT)"; 
  spider_fns = spider_fns "|" "(PS Z)" "|" "(PS X)" "|" "(PS A)" ;
  spider_fns = spider_fns "|" "(PS)" "|" "(PP V)" "|" "(PP P)" "|" "(PP LL)"; 
  spider_fns = spider_fns "|" "(PP L)" "|" "(PP)" "|" "(PO)" ;
  spider_fns = spider_fns "|" "(PL HI)" "|" "(PL FIT)" "|" "(PL)" "|" "(PK M)"; 
  spider_fns = spider_fns "|" "(PK DR)" "|" "(PK DC)" "|" "(PK D)" ;
  spider_fns = spider_fns "||" "(PK C)" "|" "(PK 3D)" "|" "(PK 3)" "|" "(PK)"; 
  spider_fns = spider_fns "|" "(PJ ST)" "|" "(PJ SHAD)" "|" "(PJ INTER)" ;
  spider_fns = spider_fns "|" "(PJ CYL)" "|" "(PJ COL)" "|" "(PJ AT)" "|" "(PJ 3Q)"; 
  spider_fns = spider_fns "|" "(PJ 3)" "|" "(PI REG)" "|" "(PF)" "|" "(PD)" ;
  spider_fns = spider_fns "|" "(PB CG3)" "|" "(PB RT3)" "|" "(PB RT3A)" "|" "(PB SH)"
  spider_fns = spider_fns "|" "(PA)" "|" "(OR QM)" "|" "(OR Q)"; 
  spider_fns = spider_fns "|" "(OR NQ)" "|" "(OR MQ)" "|" "(OR AM)" ;
  spider_fns = spider_fns "|" "(OR A)" "|" "(OR 3Q)" "|" "(OR 3A)" "|" "(OR 2M)"; 
  spider_fns = spider_fns "|" "(OR 2)" "|" "(OP)" "|" "(OD)" ;
  spider_fns = spider_fns "||" "(NK M)" "|" "(NK)" "|" "(NEG A)" "|" "(NEG)"; 
  spider_fns = spider_fns "|" "(NC)" "|" "(MY FL)" "|" "(MY)" ;
  spider_fns = spider_fns "|" "(MX)" "|" "(MU O)" "|" "(MU M)" "|" "(MU D)"; 
  spider_fns = spider_fns "|" "(MU)" "|" "(MS IF)" "|" "(MS I)" ;
  spider_fns = spider_fns "|" "(MS F)" "|" "(MS)" "|" "(MR)" "|" "(MO 3)"; 
  spider_fns = spider_fns "|" "(MO)" "|" "(MN S)" "|" "(MN)" ;
  spider_fns = spider_fns "|" "(MM C)" "|" "(MM)" "|" "(MK 3)" "|" "(MK)"; 
  spider_fns = spider_fns "|" "(MD NO RESULTS)" "|" "(MD PIPE)" "|" "(MD SET SEED)" ;
  spider_fns = spider_fns "||" "(MD SET REGS)" "|" "(MD SET MP)" "|" "(MD VB OFF)" "|" "(MD VB ON)"; 
  spider_fns = spider_fns "|" "(MD OP OFF)" "|" "(MD OP ON)" "|" "(MD TR OFF)" ;
  spider_fns = spider_fns "|" "(MD TR ON)" "|" "(MD STA)" "|" "(MD ME)" "|" "(MD)"; 
  spider_fns = spider_fns "|" "(MA Z)" "|" "(MA Y)" "|" "(MA X)" ;
  spider_fns = spider_fns "|" "(MA L)" "|" "(MA)" "|" "(LI X)" "|" "(LI T)"; 
  spider_fns = spider_fns "|" "(LI RT)" "|" "(LI RD)" "|" "(LI R)" ;
  spider_fns = spider_fns "|" "(LI D)" "|" "(LI)" "|" "(LD T)" "|" "(LD)"; 
  spider_fns = spider_fns "|" "(LB)" "|" "(LA L)" "|" "(LA)" ;
  spider_fns = spider_fns "||" "(IQ WO)" "|" "(IQ SYNC)" "|" "(IQ REG)" "|" "(IQ GONE)"; 
  spider_fns = spider_fns "|" "(IQ FI)" "|" "(IP T)" "|" "(IP)" ;
  spider_fns = spider_fns "|" "(IN S)" "|" "(IN)" "|" "(IF_THEN)" "|" "(IF_GOTO)"; 
  spider_fns = spider_fns "|" "(IA)" "|" "(HI TR)" ;
  spider_fns = spider_fns "|" "(HI T)" "|" "(HI R)" "|" "(HI M)" "|" "(HI JOINT)"; 
  spider_fns = spider_fns "|" "(HI E)" "|" "(HI DR)" "|" "(HI D)" ;
  spider_fns = spider_fns "|" "(HI)" "|" "(HF)" "|" "(HD R)" "|" "(HD D)"; 
  spider_fns = spider_fns "|" "(HD)" "|" "(GR T)" "|" "(GR)" ;
  spider_fns = spider_fns "||" "(GP)" "|" "(GOTO)" "|" "(FV)" "|" "(FT R)"; 
  spider_fns = spider_fns "|" "(FT)" "|" "(FS M)" "|" "(FS)" ;
  spider_fns = spider_fns "|" "(FR L)" "|" "(FR G)" "|" "(FR F)" "|" "(FR)"; 
  spider_fns = spider_fns "|" "(FQ NP)" "|" "(FQ)" "|" "(FP)" ;
  spider_fns = spider_fns "|" "(FI X)" "|" "(FI N)" "|" "(FI A)" "|" "(FI)"; 
  spider_fns = spider_fns "|" "(FF PLOT)" "|" "(FF)" "|" "(FD)" ;
  spider_fns = spider_fns "|" "(FC E)" "|" "(FC)" "|" "(EV)" "|" "(ER WA)"; 
  spider_fns = spider_fns "|" "(ER SK)" "|" "(ER EDM)" "|" "(ER DOC)" ;
  spider_fns = spider_fns "||" "(ER)" "|" "(EP TT)" "|" "(EP TM)" "|" "(EP)"; 
  spider_fns = spider_fns "|" "(EN DE)" "|" "(EN D)" "|" "(EN)" "|" "(ED)" ;
  spider_fns = spider_fns "|" "(EC STAT)" "|" "(EC SELECT)" "|" "(EC FILTER)" "|" "(EC CLOSE)"; 
  spider_fns = spider_fns "|" "(EC CL)" "|" "(DU V)" "|" "(DU)" ;
  spider_fns = spider_fns "|" "(DR ERR)" "|" "(DR DIFF)" "|" "(DOC SUBTRACT)" "|" "(DOC SPLIT)"; 
  spider_fns = spider_fns "|" "(DOC SORT)" "|" "(DOC REN)" "|" "(DOC RAN)" ;
  spider_fns = spider_fns "|" "(DOC MIRROR)" "|" "(DOC MINE)" "|" "(DOC MERGE)" "|" "(DOC CREATE)"; 
  spider_fns = spider_fns "|" "(DOC COMBINE)" "|" "(DOC AND)" "|" "(DO)" ;
  spider_fns = spider_fns "||" "(DIV)" "|" "(DI NF)" "|" "(DI)" "|" "(DF)"; 
  spider_fns = spider_fns "|" "(DE A)" "|" "(DE)" "|" "(DC S)" ;
  spider_fns = spider_fns "|" "(DC)" "|" "(CV REPL)" "|" "(CP TO XPLOR)" "|" "(CP TO VV)"; 
  spider_fns = spider_fns "|" "(CP TO TIFF)" "|" "(CP TO TIF)" "|" "(CP TO TARGA)" "|" "(CP TO SGI)" ;
  spider_fns = spider_fns "|" "(CP TO SF3)" "|" "(CP TO RAW)" "|" "(CP TO POST)" "|" "(CP TO OPEND)"; 
  spider_fns = spider_fns "|" "(CP TO MRC)" "|" "(CP TO CCP4)" "|" "(CP TO BRIX)" ;
  spider_fns = spider_fns "|" "(CP TO ASCII)" "|" "(CP I)" "|" "(CP FROM XP)" "|" "(CP FROM VAX)"; 
  spider_fns = spider_fns "|" "(CP FROM TERM)" "|" "(CP FROM SGI)" "|" "(CP FROM SF3)" ;
  spider_fns = spider_fns "||" "(CP FROM RAW)" "|" "(CP FROM PE)" "|" "(CP FROM PDB)" "|" "(CP FROM MRC)"; 
  spider_fns = spider_fns "|" "(CP FROM EMI)" "|" "(CP FROM CCP4)" "|" "(CP FROM ASCII)" ;
  spider_fns = spider_fns "|" "(CP FIX)" "|" "(CP)" "|" "(CO)" "|" "(CN N)"; 
  spider_fns = spider_fns "|" "(CN)" "|" "(CL KM)" "|" "(CL HE)" ;
  spider_fns = spider_fns "|" "(CL HD)" "|" "(CL HC)" "|" "(CL CLA)" "|" "(CI)"; 
  spider_fns = spider_fns "|" "(CG PH)" "|" "(CG 3)" "|" "(CG)" ;
  spider_fns = spider_fns "|" "(CE VAR)" "|" "(CE TOP)" "|" "(CE ST)" "|" "(CE SOBEL)"; 
  spider_fns = spider_fns "|" "(CE RIDGE)" "|" "(CE RAN)" "|" "(CE PREWITT)" ;
  spider_fns = spider_fns "||" "(CE OR)" "|" "(CE OD)" "|" "(CE MIN)" "|" "(CE MED)"; 
  spider_fns = spider_fns "|" "(CE MAX)" "|" "(CE LAP)" "|" "(CE LAH)" ;
  spider_fns = spider_fns "|" "(CE HURST)" "|" "(CE HI)" "|" "(CE HARALICK)" "|" "(CE GNC)"; 
  spider_fns = spider_fns "|" "(CE G)" "|" "(CE FREI)" "|" "(CE FIT)" ;
  spider_fns = spider_fns "|" "(CE AD)" "|" "(CC P)" "|" "(CC N)" "|" "(CC MS)"; 
  spider_fns = spider_fns "|" "(CC H)" "|" "(CC C)" "|" "(CC)" ;
  spider_fns = spider_fns "|" "(CA VIS)" "|" "(CA SRP)" "|" "(CA SRI)" "|" "(CA SRD)"; 
  spider_fns = spider_fns "|" "(CA SR)" "|" "(CA SME)" "|" "(CA SM)" ;
  spider_fns = spider_fns "||" "(CA SI)" "|" "(CA S)" "|" "(BP W2)" "|" "(BP S2)"; 
  spider_fns = spider_fns "|" "(BP RP)" "|" "(BP R2)" "|" "(BP MEM2)" ;
  spider_fns = spider_fns "|" "(BP CG)" "|" "(BP 3F)" "|" "(BP 3D)" "|" "(BP 32F)"; 
  spider_fns = spider_fns "|" "(BL)" "|" "(BC)" "|" "(AT WN)" ;
  spider_fns = spider_fns "|" "(AT SA)" "|" "(AT PK)" "|" "(AT IT)" "|" "(AS R)"; 
  spider_fns = spider_fns "|" "(AS F)" "|" "(AS AD)" "|" "(AS)" ;
  spider_fns = spider_fns "|" "(AR SCA)" "|" "(AR IF)" "|" "(AR)" "|" "(AP SR)"; 
  spider_fns = spider_fns "|" "(AP SH)" "|" "(AP SHC)" "|" "(AP SA)" "|" "(AP RQ)" "|" "(AP RNS)" ;
  spider_fns = spider_fns "||" "(AP RN)" "|" "(AP RD)" "|" "(AP RA)" "|" "(AP NQ)"; 
  spider_fns = spider_fns "|" "(AP MS)" "|" "(AP MQ R)" "|" "(AP MQ)" ;
  spider_fns = spider_fns "|" "(AP MD)" "|" "(AP I)" "|" "(AP CA)" "|" "(AD S)"; 
  spider_fns = spider_fns "|" "(AD R)" "|" "(AD F)" "|" "(AD)" "|" "(ADD)" ;
  spider_fns = spider_fns "|" "(AC S)" "|" "(AC NS)" "|" "(AC N)" "|" "(AC MSS)"; 
  spider_fns = spider_fns "|" "(AC MS)" "|" "(AC)" ;

  spider_fns = spider_fns "||" tolower(spider_fns);
# AWK BUG!! if a regular expression is longer than about 5 lines, "gsub" etc returns nonsense!!

  n = split(spider_fns,spider_fns2,"[|][|]");
}
