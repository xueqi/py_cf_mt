function [m k]=RemoveOutliers(m,nsd,maxfract)
% function [out numcorr]=RemoveOutliers(m,nsd,maxfract);
% Remove outlier pixels from images by forming local averages of
% neighboring pixels. nsd is the number of standard deviations at which
% outliers are defined; default is 5.  maxfract is the maximum fraction of
% points allowed to be outliers (default is 10^-4); if necessary nsd is
% incremented until the fraction of outliers falls below this value.
% The returned value k gives the
% number of pixels so corrected.
% If m is an integer array, the values are converted to single.
% fs 2 sep 07, revised 30 Jun 09, 7 Sep 10

msize=size(m);
nx=msize(1);
ny=msize(2);
if isinteger(m)  % convert to floating-point
    m=single(m);
end;
if nargin<3
    maxfract=1e-4;
end;
if nargin<2
    nsd=5;  % number of standard deviations for allowed points.
end;

maxno=maxfract*nx*ny;  % maximum .1% of points are outliers

mr=reshape(m,numel(m),1);  % convert it all to a vector
me=mean(mr);
sd=sqrt(var(mr));
ol=find(abs(mr-me)>nsd*sd);

% prevent hangups in high-contrast images by incrementing nsd
while numel(ol)>maxno
    nsd=nsd+1;
    ol=find(abs(mr-me)>nsd*sd);
end;

% Pick up the subscripts of the outlying points
[is js]=ind2sub(msize,ol);

maxstep=3;  % maximum size of neighborhood to search is (maxstep+1)^2

for k=1:numel(ol);
    sn=0;
    istep=0;
    while (sn<1) && (istep<maxstep) % search a neighborhood
        istep=istep+1;              % neighborhood size
        % make a copy of the points surrounding the outlier
        i=is(k); j=js(k);  % Coordinate of the point in the original image
        % define a box surrounding the point
        imin=max(1,i-istep);
        imax=min(nx,i+istep);
        jmin=max(1,j-istep);
        jmax=min(ny,j+istep);
        mlocal=m(imin:imax,jmin:jmax);
        
        % Replace the outlier with the average of in-bounds surrounding points.
        olpoint=sub2ind(size(mlocal),i-imin+1,j-jmin+1); % local index of outlier
        s=0;
        sn=0;
        for q=1:numel(mlocal)
            if (q ~= olpoint) && (abs(mlocal(q)-me)<nsd*sd)
                s=s+mlocal(q);
                sn=sn+1;
            end;
        end;
    end; % while
    if sn<1
        % Failed to find an in-bounds point in the neighborhood.  This can
        % happen only at the edge of a micrograph.
        disp(['RemoveOutliers warning: search with istep = ' num2str(istep) ...
            ' failed at coords ' num2str(i) ' ' num2str(j) '; value set to mean.']);
        m(i,j)=me;
    else
        m(i,j)=s/sn;
    end;
    
end;
k=numel(ol);
