function box = read_chumps_box(micrograph,box_num,coords,box_dim,...
                               bin_factor,rotation,filter)
stdout = 1;

if(nargin < 5)
  bin_factor = 1;
end

if(nargin < 6)
  rotation = 0;
end

if(nargin < 7)
  filter = ones(2*box_dim);
end

if(nargin < 7)
  filter = ones(2*box_dim);
end

%%%%%%%%%%%%%%%%%%
% Read in boxed image, after re-centering the coordinates using the found
%  coords
%%%%%%%%%%%%%%%%%%

int_coords = round(coords(box_num,:));
frac_coords = coords(box_num,:) - int_coords;

pad_box_dim = 2*box_dim;
box_origin = int_coords-floor(pad_box_dim/2);

[pad_box,mrcinfo,msg] = ReadMRCwin(micrograph, pad_box_dim, box_origin, 3);

if(pad_box == -1)
  fprintf(stdout,'  WARNING: failed to read box from micrograph: (%d, %d)\n',...
          box_origin(1),box_origin(2));
  return
end

pad_box = double(fftn(pad_box) .* FourierShift(2*box_dim(1), -frac_coords));
if(nargin > 3)
  pad_box = pad_box .* filter;
end
pad_box = real(ifftn(pad_box));

if(rotation != 0)
  pad_box = rotate2d(pad_box, rotation);
end

box = pad_box(floor(1 + box_dim-box_dim/2:1 + box_dim-box_dim/2 + box_dim-1),...
        floor(1 + box_dim-box_dim/2:1 + box_dim-box_dim/2 + box_dim-1));

if(bin_factor ~= 1)
  box = BinImageCentered(box, bin_factor);
end
