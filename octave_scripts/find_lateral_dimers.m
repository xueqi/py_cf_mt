function id_map = find_lateral_dimers(sorted_map,seam_mask, sorted_map_file)

% sorted_map = readSPIDERfile(sorted_map_file);
s = size(sorted_map);

result_maps = zeros(s(1),s(2),2);
id_map = zeros(s(1),s(2));

for pf = 1:s(1)-1
  if(seam_mask(pf,1))
    register = 1;
  else
    register = 2;
  end

for row = register:2:s(2)
  vacant_previous_pf = 0;
  if(pf == 1)
    vacant_previous_pf = 1;
  else
    if(!sorted_map(pf-1,row))
      vacant_previous_pf = 1;
    endif
  endif
  if(sorted_map(pf,row) && vacant_previous_pf == 1)
% Set variable "ind" to 1 greater than the last pf where the site is occupied
    for ind=pf+1:s(1)
%      fprintf('%2d %3d %3d %1d %d\n',pf,row,ind,sorted_map(pf,row),sorted_map(pf,ind));
      if(!sorted_map(ind,row))
        break
      end
    end
    if(ind == s(1) && sorted_map(ind,row))
      ind = ind + 1; % Account for the possibility that the last pf is occupied
    endif

    n_kinesins = (ind-pf);
%    printf('%d %d %d\n',row,pf,n_kinesins);
%    fprintf('%2d %d\n',pf,n_kinesins);
    if(mod(n_kinesins,2) == 0)
      result_index = 1;  % corresponds to a "good" run of consecutive kinesins (even number)
    else
      result_index = 2;  % corresponds to a "bad" run of consecutive kinesins (odd number)
    end

    result_maps(pf,row,result_index) = 1;
    id = 0;
    if(result_index == 1)
      id_map(pf,row) = id+1;
    end

    for ind=pf+1:s(1)
      id = mod((id + 1),2);
      if(!sorted_map(ind,row))
        break
      else
        result_maps(ind,row,result_index) = 1;
        if(result_index == 1)
          id_map(ind,row) = id+1;
        end
      end
    end
  end
end
end

if(nargin > 2)
  output_file = sprintf('%s_lat_good.spi',sorted_map_file(1:prod(size(sorted_map_file))-4));
  writeSPIDERfile(output_file,result_maps(:,:,1));
  output_file = sprintf('%s_lat_bad.spi',sorted_map_file(1:prod(size(sorted_map_file))-4));
  writeSPIDERfile(output_file,result_maps(:,:,2));

  output_file = sprintf('%s_lat_good_id.spi',sorted_map_file(1:prod(size(sorted_map_file))-4));
  writeSPIDERfile(output_file,id_map);
end

fprintf('%4d %4d %6.4f\n',sum(sum(result_maps(:,:,1))),sum(sum(result_maps(:,:,2))),sum(sum(result_maps(:,:,1)))/sum(sum(result_maps(:,:,2))));

prod(s)
