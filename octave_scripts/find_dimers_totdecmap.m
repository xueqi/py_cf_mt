function id_map = find_dimers_totdecmap(tot_decoration_map, singledimer, seam_mask)

if(nargin < 2)
  singledimer = 0;
end

% [seam_mask, pf_offset] = find_seams_totdecmap(tot_decoration_map);
% sorted_map = readSPIDERfile(sorted_map_file);
s = size(tot_decoration_map);

result_maps = zeros(s(1),s(2),2);
id_map = zeros(s(1),s(2));


for pf = 1:s(1)
  if(!seam_mask(pf,1))
    continue
  end
  for row = 2 : s(2)-1
  
    if(tot_decoration_map(pf,row) && !tot_decoration_map(pf,row-1))
      for ind=row+1:s(2)
%      fprintf('%2d %3d %3d %1d %d\n',pf,row,ind,sorted_map(pf,row),sorted_map(pf,ind));
        if(!tot_decoration_map(pf,ind))
          break
        end
      end
      n_kinesins = (ind-row);
%    fprintf('%2d %d\n',pf,n_kinesins);
      if(mod(n_kinesins,2) == 0)
        result_index = 1;
      else
        result_index = 2;
      end

      if(ind == s(2) && tot_decoration_map(pf, ind))
        result_index = 2;
      end

      if(singledimer)
        if(n_kinesins != 2)
          result_index = 2;
        end
      end

      result_maps(pf,row,result_index) = 1;
      id = 0;
      if(result_index == 1)
        id_map(pf,row) = id+1;
      end

      for ind=row+1:s(2)
        id = mod((id + 1),2);
        if(!tot_decoration_map(pf,ind))
          break
        else
          result_maps(pf,ind,result_index) = 1;
          if(result_index == 1)
            id_map(pf,ind) = id+1;
          end
        end
      end
    end
  end
end

fprintf('%4d dimers (%d subunits);\n%4d subunits discarded;\n%6.1f%% kept\n',sum(sum(result_maps(:,:,1)))/2,sum(sum(result_maps(:,:,1))),sum(sum(result_maps(:,:,2))),100*sum(sum(result_maps(:,:,1)))/(sum(sum(result_maps(:,:,1)))+sum(sum(result_maps(:,:,2)))));
