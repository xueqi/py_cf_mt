function id_map = find_monomer(sorted_map, seam_mask)

% sorted_map = readSPIDERfile(sorted_map_file);
s = size(sorted_map);

id_map = zeros(s(1),s(2));

for pf = 1:s(1)
  
  if(seam_mask(pf,1))
    register = 1;
  else
    register = 2;
  end

  for row = register+2:2:s(2)-2
    if(sorted_map(pf,row) && !sorted_map(pf,row-2) && !sorted_map(pf,row+2))
      id_map(pf,row) = 1;
    end
  end
end
