function [output_box_bounds, output_tot_bounds] = ...
  box_to_tot_findmap(box_map, input_box_repeat_bounds, box_repeat_origin, ...
                     tot_map, box_num, sites_per_repeat)

kin_lib_size = prod(size(box_map));

filament_polarity = 1;

%%%%%%%%%%%%%%%%%%%%
% Note below: directional_psi is now ensured to be 270, i.e. there are no backwards filaments!
%if(directional_psi == 270)
%  filament_polarity = 1;
%else
%  filament_polarity = -1;
%end

% The following would center place the central subunit of a repeat at the origin, as opposed to
%     lib_origin_offset = 0, which places the first subunit at the origin:
%   lib_origin_offset = -floor(sites_per_repeat/2);

lib_origin_offset = 0;

box_lowerb = 1 + (input_box_repeat_bounds(1)-1)*sites_per_repeat + lib_origin_offset;
box_upperb =      input_box_repeat_bounds(2)*sites_per_repeat + lib_origin_offset;

tot_first_repeat = box_num + ...
   filament_polarity*(input_box_repeat_bounds(1)-box_repeat_origin);
tot_last_repeat = box_num + ...
   filament_polarity*(input_box_repeat_bounds(2)-box_repeat_origin);

tot_lowerb = 1 + (tot_first_repeat-1)*sites_per_repeat;
tot_upperb =      tot_last_repeat*sites_per_repeat;

if(tot_lowerb < 1)
  box_lowerb = box_lowerb + (1-tot_lowerb);
  tot_lowerb = 1;
end
if(tot_upperb > prod(size(tot_map)))
  box_upperb = box_upperb - (tot_upperb - prod(size(tot_map)));
  tot_upperb = prod(size(tot_map));;
end

if(box_lowerb < 1)
  tot_lowerb = tot_lowerb + (1-box_lowerb);
  box_lowerb = 1;
end
if(box_upperb > kin_lib_size)
  tot_upperb = tot_upperb - (box_upperb - kin_lib_size);
  box_upperb = kin_lib_size;
end

output_box_bounds = [box_lowerb box_upperb];
output_tot_bounds = [tot_lowerb tot_upperb];
