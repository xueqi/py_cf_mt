function info = readSPIDERdoc_dlmlike(filename,strip)

if(isoctave)
  info = dlmread(filename);
else
  temp = readSPIDERdoc(filename);
  s = size(temp);
  info = zeros([s(1) s(2)+2]);
  info(1:s(1),3:s(2)+2) = temp;
  info(1:s(1),1) = 1:s(1);
end

if(nargin > 1)
  if(strip != 0)
    f = fopen(filename);

    s = size(info);
    i = 1;
    goodi = 1;
    while(i <= s(1))
      fl = fgetl(f);
      snew = size(info);
      if(fl(2) == ';')
%        printf('%d: %s\n', i, fl);
        info(goodi:snew(1)-1,:)=info(goodi+1:snew(1),:);
        info = info(1:snew(1)-1,:);
      else
        goodi=goodi+1;
      end
      i=i+1;
    end
    fclose(f);
  end
end
