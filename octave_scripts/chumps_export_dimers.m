function [box_nums, result_xs, result_ys, result_phis, result_thetas, result_psis] = ...
  chumps_export_dimers(job_dir,focal_mate, target_mode, ...
            chumps_round,find_bin_factor, smooth_dir, ...
            phis_align, thetas_align, psis_align, coords_x_align, coords_y_align,
            prerotate, bin_factor,...
            fit_order, data_redundancy, max_outliers_per_window, ...
            twist_tolerance, coord_error_tolerance,...
            phi_error_tolerance, theta_error_tolerance, ...
            psi_error_tolerance, min_seg_length)
% read in mt repeat infos and output kinesin infos
% info is coordinates, angles. Shifts should keep the same as mt repeats
stdout = 1;

job_dir = trim_dir(job_dir);

%%%%%%%%%%%%%%%%%%
% Set other parameters
%%%%%%%%%%%%%%%%%%

ref_dir = sprintf('chumps_round%d/ref_tub_subunit_bin%d',chumps_round,find_bin_factor);
%tub_ref_file = sprintf('%s/ref_tot.spi',ref_dir);
tub_ref_file = sprintf('%s/tub_centered_vol.spi',ref_dir);
ref_params_name = sprintf('%s/ref_params.spi',ref_dir);
com_doc_name = sprintf('%s/tub_cgr_doc.spi',ref_dir);

ref_dir = sprintf('chumps_round%d/ref_kin_subunit_bin%d',chumps_round,find_bin_factor);
%kin_ref_file = sprintf('%s/ref_tot.spi',ref_dir);
kin_ref_file = sprintf('%s/kin_centered_vol.spi',ref_dir);

elastic_params = [0 0 0 0];
radius_scale_factor = 1;

%%%%%%%%%%%%%%%%%%
% Get user parameters
%%%%%%%%%%%%%%%%%%

if(isoctave)
  run 'cf-parameters.m'
else
  run 'cf-parameters'
end

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;

%%%%%%%%%%%%%%%%%%%%%
% Read CTF info
%%%%%%%%%%%%%%%%%%%%%
job_dir
[start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');
start
ctf_doc = dlmread(sprintf('scans/%s_ctf_doc.spi',job_dir(1:start-1)));
index = find(ctf_doc(:,1) == 1);
if(prod(size(index)) == 0)
  fprintf(stdout,'ERROR: no CTF information found... (please run chumps_ctf first)\n');
  return
end
index = index(prod(size(index)));
defocus1 = ctf_doc(index,3);
defocus2 = ctf_doc(index,4);
astig_angle = ctf_doc(index,5);

%%%%%%%%%%%%%%%%%%
% Get alignment and micrograph info
%%%%%%%%%%%%%%%%%%
[micrograph,defocus,astig_mag,astig_angle] = ...
  chumps_micrograph_info(job_dir,focal_mate,chumps_round);
[num_pfs,num_starts] = ... %,coords, phi, theta, psi, directional_psi] = ...
  read_chumps_alignment(job_dir,helical_repeat_distance,chumps_round,micrograph_pixel_size);

sites_per_repeat = 2*num_pfs;
max_overlap = 2*num_pfs;

kin_num_repeats = ceil( (sites_per_repeat+2*max_overlap)/num_pfs );
tub_num_repeats = 2*kin_num_repeats; % make the underlying MT extra long

kin_lib_size = 2*kin_num_repeats*num_pfs;

%%%%%%%%%%%%%%%%%%
% Read in parameter files and extract relevant data
%%%%%%%%%%%%%%%%%%
com_info = readSPIDERdoc_dlmlike(com_doc_name);

index = find(com_info(:,1) == 1);
ref_com = com_info(index(1),3:5);

%  [coords,phi,theta,psi,...
%   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
%   est_repeat_distance_smooth, psi_ref, psi_ref_smooth, discontinuities, outliers] = ...
%    fixup_chuff_alignment(coords,phi,theta,psi,...
%                        directional_psi,micrograph_pixel_size,helical_repeat_distance,...
%                        0, 1, ...
%                        'smooth_all', fit_order, data_redundancy, max_outliers_per_window, ...
%                        twist_tolerance, coord_error_tolerance,...
%                        phi_error_tolerance, theta_error_tolerance, psi_error_tolerance);
est_repeat_distance = helical_repeat_distance;
est_pix_per_repeat = est_repeat_distance/micrograph_pixel_size;

load(sprintf('%s/%s_fil_info.mat', smooth_dir, job_dir));
coords = coords_est_smooth;
phi = phi_est_smooth;
theta = theta_est_smooth;
psi = psi_est_smooth;
[coords_dummy,phi_dummy,theta_dummy,psi_dummy,...
 d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat] = ...
 fixup_chumps_alignment(coords,phi,theta,psi,90,...
                         micrograph_pixel_size,helical_repeat_distance,...
                         est_pix_per_repeat, num_pfs, num_starts, ref_com);
n_good_boxes = prod(size(phi));

%%%%%%%%%%%%%%%%%%%%
% ID the good segments of the filament
%%%%%%%%%%%%%%%%%%%%

goodcount = 0;
segids = zeros([n_good_boxes 1]);
segid = 0;
% keep all
for ind=1:n_good_boxes
%  if(!discontinuities(ind))
    if(goodcount <= 0)
      segid = segid + 1;
    end
    goodcount = goodcount + 1;
    segids(ind) = segid;
%  else
%    segids(ind) = 0;
%    goodcount = 0;
%  end
end

n_segs = segid;

%%%%%%%%%
% Discard segments that are too short
%%%%%%%%%
for ind = 1:segid
  if(prod(size(find(segids == ind))) < min_seg_length)
    segids(find(segids == ind)) = 0;
  end
end

%%%%%%%%%
% Renumber the remaining segments
%%%%%%%%%
segid = 0;
for ind=1:n_good_boxes
  if(segids(ind) && segids(ind) != segid)
    segid = segid + 1;
    segids(find(segids == segids(ind))) = segid;
  end
end

n_segs = segid;

%%%%%%%%%%%%%%%%%%
% Derive related filenames
%%%%%%%%%%%%%%%%%%

[start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');

if(focal_mate == 0)
  find_dir = sprintf('chumps_round%d/%s/find_kinesin',chumps_round,job_dir);
  micrograph = sprintf('scans/%s.mrc',job_dir(1:start-1));
else
  find_dir = sprintf('chumps_round%d/%s/fmate_find',chumps_round,job_dir);
  micrograph = sprintf('scans/%s_focal_mate_align.mrc',job_dir(1:start-1));
end

old_frealign_dir = sprintf('%s_pf%d_start%d', smooth_dir, num_pfs, 2*num_starts);

%%%%%%%%%%%%%%%%%%
% Initialize file-related parameters
%%%%%%%%%%%%%%%%%%

%if(frealign_round > 0)
%  frealign_par_data = dlmread(sprintf('%s/filament_data/%s_%d_strip.par',...
%                              old_frealign_dir,job_dir, frealign_round));

 % n_boxes_from_frealign = size(frealign_par_data);
 % n_boxes_from_frealign = n_boxes_from_frealign(1);
%
%  if(n_boxes_from_frealign != prod(size(find(segids))))
%    printf('Gah!\n');
%    exit(2);
%  end
%
%  fprintf(stdout,'%s %4d / %4d\n', job_dir, n_boxes_from_frealign, n_good_boxes);
%end

%%%%%%%%%%%%%%%%%%%%%%%
% Loop through the repeats
%%%%%%%%%%%%%%%%%%%%%%%

%tot_decorate_map = readSPIDERfile(sprintf('%s/tot_decorate_map.spi',find_dir));
%
%sorted_dimer_map = readSPIDERfile(sprintf('%s/sorted_map_full_%s.spi',find_dir, target_mode));
%
%[box_repeat_origin, kin_repeat_index, kin_pf_index,...
% x_tub, y_tub, phi_tub, theta_tub, psi_tub] = ...
% get_findkin_refs(1, 0, [n_good_boxes n_good_boxes 1], num_pfs,num_starts,...
%                  phi, theta, psi, elastic_params,...
%                  d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
%                  est_repeat_distance,radius_scale_factor,ref_com);
%
%[box_bounds, tot_bounds, sorted_indices] = ...
%  map_to_helimap(tot_decorate_map, tot_decorate_map, 1, [1 n_good_boxes], 1, ...
%                 num_pfs, sites_per_repeat, kin_repeat_index, kin_pf_index);
%
%tot_decorate_map_dimer = zeros(size(tot_decorate_map));
%tot_decorate_map_dimer(box_bounds(1):box_bounds(2)) = sorted_dimer_map(sorted_indices);

if (any(strcmp(target_mode, {'dimer', 'composite_dimer', 'monomer', '0110', '010'})))
    tot_decorate_map_dimer = readSPIDERfile(sprintf('%s/tot_decorate_map_%s.spi',find_dir, target_mode));
else
    % target_mode is a map file. Make sure the position need to be exported is 1
    tot_decorate_map_dimer = readSPIDERfile(sprintf('%s/%s', find_dir, target_mode));
end

target_monomer_id = 1;

%for pfid=1:num_pfs
%  frealign_output_par = sprintf('%s_pf%d_0.par', frealign_output_header, pfid);
%  fid = fopen(frealign_output_par, 'w');
%  fprintf(fid, "");
%  fclose(fid);
%end


tot_n_particles = 0;
result_psis = [];
result_thetas = [];
result_phis = [];
result_xs = [];
result_ys = [];
box_nums = [];
for box_num=1:n_good_boxes
  if(!segids(box_num))
    continue
  end

%  if(frealign_round > 0)
    frealign_psi = psis_align(box_num); %frealign_par_data(box_num, 2);
    frealign_theta = thetas_align(box_num); %frealign_par_data(box_num, 3);
    frealign_phi = phis_align(box_num); %frealign_par_data(box_num, 4);
    coord_x = coords_x_align(box_num);
    coord_y = coords_y_align(box_num);
%    frealign_shx = frealign_par_data(box_num, 5);
%    frealign_shy = frealign_par_data(box_num, 6);
%    frealign_mag = frealign_par_data(box_num, 7);
%    frealign_film = frealign_par_data(box_num, 8);
%    frealign_df1 = frealign_par_data(box_num, 9);
%    frealign_df2 = frealign_par_data(box_num, 10);
%    frealign_ang_astig = frealign_par_data(box_num, 11);
%    frealign_occ = frealign_par_data(box_num, 12);
%    frealign_logp = frealign_par_data(box_num, 13);
%    frealign_sigma = frealign_par_data(box_num, 14);
%    frealign_score = frealign_par_data(box_num, 15);
%    frealign_change = frealign_par_data(box_num, 16);
%  else
%    'Need to complete the code here...'
%    exit(2);
%  end

  fprintf(stdout,'\nBox %4d/%4d : \n', box_num, n_good_boxes);

  lib_origin_offset = 0;

  if(prerotate)
    psi_adjust = zeros([prod(size(psi)) 1]);
  else
    psi_adjust = psi;
  end
  [box_repeat_origin, kin_repeat_index, kin_pf_index,...
     x_tub, y_tub, phi_tub, theta_tub, psi_tub, phi_mt, theta_mt, psi_mt, ...
     ref_tub, ref_kin, x_mt, y_mt, z_mt] = ...
      get_findkin_refs(box_num, 0,1,...
                   num_pfs,num_starts,...
                   phi, theta, psi_adjust, elastic_params,...
                   d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
                   est_repeat_distance,radius_scale_factor,ref_com);
  decorate = zeros([kin_lib_size 1]);

  [box_bounds, tot_bounds] = ...
     box_to_tot_findmap(decorate, box_repeat_origin, ...
                   box_repeat_origin(1), ...
                   tot_decorate_map_dimer, box_num, sites_per_repeat);
  decorate = tot_decorate_map_dimer(tot_bounds(1):tot_bounds(2));

decorate(1:2:sites_per_repeat)

%save('debug.binmat', 'num_pfs','num_starts',...
%                   'phi', 'theta', 'psi_adjust', 'elastic_params',...
%                   'd_phi_d_repeat','d_theta_d_repeat','d_psi_d_repeat',...
%                   'est_repeat_distance','radius_scale_factor','ref_com');

  for subunit_num=box_bounds(1):box_bounds(2)
    pfid = ceil(subunit_num/2);

    if(decorate(subunit_num) == target_monomer_id)
      occ = 1;
    else
      occ = 0;
    end

%format bank;
%if(subunit_num == 25 && occ)
%  [(pfid-1)*n_good_boxes + box_num; ...
%   coords_est_smooth(box_num,:)(:)*micrograph_pixel_size; ...
%   frealign_shx + x_mt(subunit_num);  ...
%   frealign_shy + y_mt(subunit_num) ; ...
%   frealign_psi +   180/pi*(psi_mt(subunit_num) -   psi_mt(1)); ...
%   frealign_theta + 180/pi*(theta_mt(subunit_num) - theta_mt(1)); ...
%   frealign_phi +   180/pi*(phi_tub(subunit_num) -  phi_tub(1)); ...
%   box_num]'
%end

if(occ)
%if (1)
  tot_n_particles = tot_n_particles + 1;
%  [header, dim] = readSPIDERheader(sprintf('%s/%s_bin%d.spi', smooth_dir, job_dir, bin_factor));
  
%  temp_im = readSPIDERwin(sprintf('%s/%s_bin%d.spi', smooth_dir, job_dir, bin_factor),...
%                          [dim(1) dim(2) 1], [1 1 box_num]);

%  appendSPIDERvol(sprintf('%s_bin%d.spi', frealign_output_header, bin_factor), ...
%                  real(ifftn(fftn(temp_im).*...
%                             FourierShift(size(temp_im,1), ...
%                               [-x_mt(subunit_num)/(bin_factor*micrograph_pixel_size) ...
%                                -y_mt(subunit_num)/(bin_factor*micrograph_pixel_size)]))));
%
%  frealign_output_par = sprintf('%s_0.par', frealign_output_header);
%  fid = fopen(frealign_output_par, 'a');

    new_psi = frealign_psi + 180/pi*(psi_mt(subunit_num) -   psi_mt(1));
    new_theta = frealign_theta + 180/pi*(theta_mt(subunit_num) - theta_mt(1));
    new_phi = frealign_phi + 180/pi*(phi_tub(subunit_num) -  phi_tub(1));
    x_new = coord_x + x_mt(subunit_num)/micrograph_pixel_size;
    y_new = coord_y + y_mt(subunit_num)/micrograph_pixel_size;
    %new_psi = frealign_psi;
    %new_theta = frealign_theta;
    %new_phi = frealign_phi;
    %x_new = coord_x;
    %y_new = coord_y;
    result_psis = [result_psis, new_psi];
    result_thetas = [result_thetas, new_theta];
    result_phis = [result_phis, new_phi];
    result_xs = [result_xs, x_new];
    result_ys = [result_ys, y_new];
    box_nums = [box_nums, box_num];
%  fprintf(fid, ...
%         '%7d%8.2f%8.2f%8.2f%10.2f%10.2f%8d%6d%9.1f%9.1f%8.2f%8.2f%10d%11.4f%8.2f%8.2f\n', ...
%         tot_n_particles, ...
%         frealign_psi +   180/pi*(psi_mt(subunit_num) -   psi_mt(1)), ...
%         frealign_theta + 180/pi*(theta_mt(subunit_num) - theta_mt(1)), ...
%         frealign_phi +   180/pi*(phi_tub(subunit_num) -  phi_tub(1)), ...
%         frealign_shx,  frealign_shy, ...
%         frealign_mag,  frealign_film, frealign_df1,  frealign_df2,  frealign_ang_astig,  ...
%         occ,  frealign_logp,  frealign_sigma, frealign_score, frealign_change);
%
%  fclose(fid);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% A note on the shifts here:
%  To shift an image in the 'SPIDER' sense, positive x, y would slide the image
%   in the positive x and y direction.
%  If on the other hand, one treats x, y as deltas that are added to the absolute coordinates,
%   then one would reverse the sign of x and y. Thus, to slide an image in the positive x and y
%   direction using this latter convention, one would *subtract* x and y from the 
%   absolute coordinates.
% 
% FREALIGN, unlike SPIDER, uses the latter convention- a never-ending source of confusion!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%    frealign_output_par = sprintf('%s_pf%d_0.par', frealign_output_header, pfid);
%    fid = fopen(frealign_output_par, 'a');
%    fprintf(fid, ...
%         '%7d%8.2f%8.2f%8.2f%10.2f%10.2f%8d%6d%9.1f%9.1f%8.2f%8.2f%10d%11.4f%8.2f%8.2f\n', ...
%         box_num, ...
%         frealign_psi +   180/pi*(psi_mt(subunit_num) -   psi_mt(1)), ...
%         frealign_theta + 180/pi*(theta_mt(subunit_num) - theta_mt(1)), ...
%         frealign_phi +   180/pi*(phi_tub(subunit_num) -  phi_tub(1)), ...
%         frealign_shx + x_mt(subunit_num),  frealign_shy + y_mt(subunit_num), ...
%         frealign_mag,  frealign_film, frealign_df1,  frealign_df2,  frealign_ang_astig,  ...
%         occ,  frealign_logp,  frealign_sigma, frealign_score, frealign_change);
%
%         box_num, psi_mt(subunit_num),theta_mt(subunit_num),phi_mt(subunit_num),...
%         x_mt(subunit_num),y_mt(subunit_num), ...
%         magnification/bin_factor,filament_number, ...
%         defocus1,defocus2,adjusted_astig_angle(i), ...
%         occ,lgp,s,pres,dpres);

%    fclose(fid);
  end
end

return
