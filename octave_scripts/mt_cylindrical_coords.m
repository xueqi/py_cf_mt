function [z, phi, mt_radius, axial_repeat_dist, twist_per_repeat, repeat_index, pf_index] = mt_cylindrical_coords(num_pfs,num_starts, dimer_repeat_dist13pf, radius13pf)
% function [z, phi, mt_radius, axial_repeat_dist, twist_per_repeat] = mt_cylindrical_coords(num_pfs,num_starts, dimer_repeat_dist13pf, radius13pf)

[twist_per_subunit, rise_per_subunit, mt_radius, axial_repeat_dist, twist_per_repeat] = mt_lattice_params(num_pfs, num_starts, dimer_repeat_dist13pf, radius13pf);

index = 1;

for i=-4*num_starts:4*num_starts
  for j=1:num_pfs
    z = i*num_pfs*rise_per_subunit/num_starts + (j-1)*rise_per_subunit;
    output_matrix(index,1) = i;
    output_matrix(index,2) = j;
    output_matrix(index,3) = z;
    phi = i*twist_per_repeat + 2*pi + (j-1)*twist_per_subunit;
    output_matrix(index,4) = phi;
    index = index + 1;
  end
end

[ordered,ord_idx]=sort(output_matrix(:,3));
% Last we order the output parameters by increasing z value

repeat_index = output_matrix(ord_idx,1);
pf_index = output_matrix(ord_idx,2);
z = output_matrix(ord_idx,3);
phi = output_matrix(ord_idx,4);
phi = mod(phi,2*pi);

%lowerb = find(z == 0) - floor(num_pfs/2);
lowerb = find(z == 0);
upperb = lowerb+num_pfs-1;

repeat_index = repeat_index(lowerb:upperb);
pf_index = pf_index(lowerb:upperb);
z = z(lowerb:upperb);
phi = phi(lowerb:upperb);
