######################################################
# Determine what binary executables are available
######################################################

if($?cfdebug) then
    echo 'Entering accessory script: setup_executables.csh ...'
endif

if(! $?operating_system_type) then
    echo 'ERROR: cannot determine operating system type, to run binary executables'
    echo '   Please specify "operating_system_type" in "'$chuff_dir'/cf-machine-parameters.m"'
    printf "\n"
    exit(2)
endif

set operating_system_type = $operating_system_type[1]

if(! $?chuff_required_executables) exit(0)

set i = 1
while ($i <= $#chuff_required_executables)

##############################
# if the user did not set "xxxx_prog" in the cf-machine-parameters.txt file
##############################
    if(! `eval 'echo $?'$chuff_required_executables[$i]'_prog'`) then

#####################################################
# DISABLED: try to actually run the program
#	eval 'which '$chuff_required_executables[$i] >& /dev/null
#	if($status != 0) then                  # if the user does not have this program in their own system
#####################################################

#####################################################
# DISABLED: used chuff-specific dynamic libraries
#		if(! $?adjusted_ld_library_path) then
#		    if(! $?LD_LIBRARY_PATH) then
#			setenv LD_LIBRARY_PATH $chuff_dir/$operating_system_type/lib
#		    else
#			setenv LD_LIBRARY_PATH $chuff_dir/$operating_system_type/lib:$LD_LIBRARY_PATH
#		    endif
#
#		    set unset adjusted_ld_library_path
#		endif
#####################################################

                set set_error_len = `echo $chuff_required_executables[$i]'_prog' | awk '{print length($1)}'`
                if($set_error_len > 30) then
                    echo 'ERROR: please keep chuff variables names <= 25 characters long...'
                    echo '  file: "cf-machine-parameters.txt"'
                    echo '  offending variable: ' $chuff_required_executables[$i]
                    exit(2)
                endif

		eval set $chuff_required_executables[$i]'_prog = ' $chuff_dir/$operating_system_type/$chuff_required_executables[$i]

		eval 'which $'$chuff_required_executables[$i]'_prog' >& /dev/null

		if($status != 0) then 
		    set chuff_warn_msg = 0
		    foreach required_exec($chuff_required_executables)
			if($required_exec == $chuff_required_executables[$i]) then
			    set chuff_warn_msg = 1
			endif
		    end
		    if($chuff_warn_msg == 1) then
			echo 'ERROR: cannot locate the required executable: ' 
			eval 'echo which $'$chuff_required_executables[$i]'_prog'
			echo 'Please adjust "'$chuff_required_executables[$i]'_prog" in "chuff-machine-parameters.m" to a useable version...'
			printf "\n"
                        exit(2)
		    endif
	    endif

#	else
#	    eval 'set '$chuff_required_executables[$i]'_prog = '$chuff_required_executables[$i]    # Use the user's version
#	endif

    endif

    eval alias $chuff_required_executables[$i]'_prog' '$'$chuff_required_executables[$i]'_prog[1]'

    @ i = $i + 1
end

if($?spider_prog) then
    set spif_prog = "$chuff_dir/commands/spif -spider_exec $spider_prog"
    setenv SPPROC_DIR ../spider/
    setenv SPMAN_DIR $chuff_dir/OSX/
    eval alias spif_prog $spif_prog
else
    set spif_prog = "$chuff_dir/commands/spif"
    setenv SPPROC_DIR ../spider/
    eval alias spif_prog $spif_prog
endif

if($?octave_prog) then
    set octave_prog = "$octave_prog --path $chuff_dir/octave_scripts --path $chuff_dir/octave_mex_${operating_system_type} --path $chuff_dir/octave_mex_${operating_system_type}'b' --no-window-system"
    eval alias octave_prog $octave_prog
endif

setenv GDFONTPATH $chuff_dir/fonts
setenv GNUPLOT_DEFAULT_GDFONT LiberationSans-Regular

if($?cfdebug) then
    echo 'Successfully completed accessory script: setup_executables.csh .'
    echo 'Program definitions:'
    foreach blah($chuff_required_executables)
	echo $blah'_prog:'
	echo '   ' `eval alias $blah'_prog'`
    end
endif

